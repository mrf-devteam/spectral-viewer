﻿/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QObject>
#include <QPoint>
#include <QImage>
#include <QFileInfo>
#include <QMap>
#include <QFileSystemWatcher>

#include <memory>
#include <cstddef>

#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/data_struct/array2d.hpp>
#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/radiometry/illuminants.hpp>
#include <mrf_core/radiometry/spectrum_type.hpp>

#ifdef MRF_WITH_EIGEN_SUPPORT
#include "Eigen/Core"
#endif

class SpectralViewerModel : public QObject
{
    Q_OBJECT
public:
    enum OpenMode {
        SINGLE_FILE,
        MULTIPLE_FILES
    };

    enum ExposureAdaptation {
        Standard, Reinhard
    };

    enum Model {
        NoFile,
        sRGBImage,
        HDRImage,
        SpectralImage,
        Spectrum
    };

    SpectralViewerModel(QObject *parent = nullptr);


    //-------------------------------------------------------------------------
    // Getting supported formats
    //-------------------------------------------------------------------------

    const std::vector<std::string>& getSupportedLoadingRGBFormats() const {
        return _supportedLoadingRGBFormats;
    }

    const std::vector<std::string>& getSupportedSavingRGBFormats() const {
        return _supportedSavingRGBFormats;
    }

    const std::vector<std::string>& getSupportedLoadingSpectralFormats() const {
        return _supportedLoadingSpectralFormats;
    }

    const std::vector<std::string>& getSupportedSavingSpectralFormats() const {
        return _supportedSavingSpectralFormats;
    }

    mrf::image::IMAGE_LOAD_SAVE_FLAGS addValuesFromGrayImage(
            QString   const & filepath,
            mrf::uint         wavelength
            );

    //-------------------------------------------------------------------------
    // Loading functions
    //-------------------------------------------------------------------------
    mrf::image::IMAGE_LOAD_SAVE_FLAGS openFile(QString const & filepath);

    mrf::image::IMAGE_LOAD_SAVE_FLAGS openFilesWavelength(
            QStringList const & filepaths,
            mrf::uint startWavelength, mrf::uint offsetWavelength
            );

private:
    mrf::image::IMAGE_LOAD_SAVE_FLAGS openHyperSpectral(QString const & filepath);
    mrf::image::IMAGE_LOAD_SAVE_FLAGS openARTRaw       (QString const & filepath);
    mrf::image::IMAGE_LOAD_SAVE_FLAGS openSpectralEXR  (QString const & filepath);
    mrf::image::IMAGE_LOAD_SAVE_FLAGS openSpd          (QString const & filepath);
    mrf::image::IMAGE_LOAD_SAVE_FLAGS openRgbImage     (QString const & filepath);

    //-------------------------------------------------------------------------
    // Export functions
    //-------------------------------------------------------------------------
public:
    void saveSpectralImage(QString const & filepath);

    // void saveSpectralValuesAsExr(
    //         QString   const & filepath,
    //         mrf::uint         wavelengthR,
    //         mrf::uint         wavelengthG,
    //         mrf::uint         wavelengthB
    //         );

    bool saveColorImage(
            QString const & filepath
            );

    void saveSpectrum(
            mrf::uint i, mrf::uint j,
            size_t            kernel_size,
            QString   const & filepath
            );

    void saveSpectrumAtSelectedPixel(
            QString const & filepath
            );

    void saveSpectrumAtSelectedPixel(
            QString   const & filepath,
            size_t            kernel_size
            );

    void convertSpectralImageToGrayRGBImage(
            mrf::uint start_wavelength, mrf::uint end_wavelength,
            bool interpolated
            );

    void setTonemappingValues(
            double const & chromatic_adaptation,
            double const & light_adaptation,
            double const & contrast,
            double const & intensity
            );

    void setTonemappingValues(
            double const & exposure,
            double const & gray_point);

    void setDefaultTonemappingValues();

    void getTonemappingValues(
            double & chromatic_adaptation,
            double & light_adaptation,
            double & contrast,
            double & intensity
            ) const;

    void rebuildLuminanceHistogram();

    void getColorImageStatistics(float & min_luminance,
            float & max_luminance,
            float & average_luminance,
            float & median_value_above_zero
            ) const;

    void getGrayImageStatistics(
            float & min_luminance,
            float & max_luminance,
            float & average_luminance,
            float & median_value
            ) const;

    void setSpectrumWavelengths(std::vector<mrf::uint> const & wavelengths);
    void setSpectrumWavelengths(std::vector<mrf::uint>&& wavelengths);

    void setDefaultOpenFolder(QString const& f);

    //=========================================================================
    // Simple Getters
    //=========================================================================

    //-------------------------------------------------------------------------
    // File meta
    //-------------------------------------------------------------------------
    QString openedFilename()     const { return _openedFilename; }
    QMap<QString, QString>
            openedFileMeta()     const { return _openedFileMetadata; }
    QString openedFileFolder()   const { return _openedFileFolder; }
    QString defaultOpenFolder()  const { return _defaultOpenFolder; }
    size_t  width()              const { return _width; }
    size_t  height()             const { return _height; }
    size_t  bands()              const { return _bands; }
    double  minWavelength()      const { return _minWavelength; }
    double  maxWavelength()      const { return _maxWavelength; }
    Model   model()              const { return _model; }
    bool    isLoaded()           const { return _isLoaded; }
    bool    isEditableEmissive() const { return _editableEmissive; }

    mrf::radiometry::SpectrumType spectrumType() const { return _spectrumType; }
    bool isEmissive()   const { return mrf::radiometry::isEmissive(_spectrumType); }
    bool isReflective() const { return mrf::radiometry::isReflective(_spectrumType); }
    bool isPolarised()  const { return mrf::radiometry::isPolarised(_spectrumType); }
    bool isBispectral() const { return mrf::radiometry::isBispectral(_spectrumType); }

    const std::vector<uint> & getSpectrumWavelengths() const
    {
        return _spectralImage->wavelengths();
    }

    const std::vector<float> & getSpectrumValuesGraphMode() const
    {
        return _spectrumGraphMode.values();
    }

    const std::vector<uint> & getSpectrumWavelengthsGraphMode() const
    {
        return _spectrumGraphMode.wavelengths();
    }

    //-------------------------------------------------------------------------
    // Colour stats
    //-------------------------------------------------------------------------
    float grayMin() const { return _grayMin; }
    float grayMax() const { return _grayMax; }
    float grayAvg() const { return _grayAvg; }
    float grayMed() const { return _grayMed; }

    float yMin() const { return _yMin; }
    float yMax() const { return _yMax; }
    float yAvg() const { return _yAvg; }
    float yMed() const { return _yMed; }

    //-------------------------------------------------------------------------
    // Colour space information
    //-------------------------------------------------------------------------
    mrf::color::SensitivityCurveName
                                curveName()  const { return _sensitivityCurve; }
    mrf::radiometry::MRF_ILLUMINANTS
                      reflectiveIlluminant() const { return _reflectiveIlluminant; }
    mrf::color::MRF_WHITE_POINT whitePoint() const { return _whitePoint; }
    mrf::color::MRF_COLOR_SPACE colorSpace() const { return _colorSpace; }

    //-------------------------------------------------------------------------
    // Images
    //-------------------------------------------------------------------------
    const QImage& getTonemappedImage() const { return _tonemappedImage; }
    const QImage& getFalseColorImage() const { return _falseColorImage; }

    /**
     * @brief maxLuminance
     * @return The maximum luminance in the image
     */
    float         maxLuminance()       const { return _maxLuminance; }
    // const int *   luminanceHistogram() const { return _luminance_histogram.data(); }

    //-------------------------------------------------------------------------
    // Tonemapping parameters
    //-------------------------------------------------------------------------
    ExposureAdaptation exposureAdaptation() const { return _exposureAdaptation; }

    // Standard
    double exposure()  const { return _exposure; }
    double grayPoint() const { return _grayPoint; }

    // Reinhard
    double chromaticAdaptation() const { return _chromaticAdaptation; }
    double lightAdaptation()     const { return _lightAdaptation; }
    double contrast()            const { return _contrast; }
    double intensity()           const { return _intensity; }

    bool s0Enabled() const { return _stokesStates[0]; }
    bool s1Enabled() const { return _stokesStates[1]; }
    bool s2Enabled() const { return _stokesStates[2]; }
    bool s3Enabled() const { return _stokesStates[3]; }

    /**
     * @brief luminance
     * @return The luminance multiplier used for the gray image
     */
    double luminance()           const { return _luminance; }

    //-------------------------------------------------------------------------
    // Selection parameters
    //-------------------------------------------------------------------------
    /**
     * @brief selectedPixel
     * @return The coordinates of the selected pixel in the image
     */
    QPoint selectedPixel() const { return _selectedPixel; }

    /**
     * @brief kernelSize
     * @return The size of kernel used for averaging values arround the
     * selected pixel
     */
    size_t kernelSize()    const { return _kernelSize; }

    /**
     * @brief minSelectedWavelength
     * @return The lower bound used for energy accumulation in the gray image
     */
    double minSelectedWavelength() const { return _minSelectedWavelength; }

    /**
     * @brief minSelectedWavelength
     * @return The upper bound used for energy accumulation in the gray image
     */
    double maxSelectedWavelength() const { return _maxSelectedWavelength; }


    //-------------------------------------------------------------------------
    // Transform parameters
    //-------------------------------------------------------------------------
    double zoomLevel() const { return _zoomLevel; }
    double rotation()  const { return _rotation; }


    //=========================================================================
    // Advanced Getters
    //=========================================================================

    //Return memory occupancy in MBytes
    double memoryOccupancy() const;

    //Compute the expected memory occupancy of loading an image in MBytes
    // TODO: shall be static
    double expectedMemoryOccupancy(int image_width, int image_height, int image_bands) const;

    void getSelectedPixelInfo(
            mrf::color::Color       & xyz,
            mrf::color::Color       & rgb,
            mrf::color::Color       & tone_mapped_rgb,
            std::vector<mrf::uint>  & wavelengths,
            std::vector<float>      & avg_spectrum,
            float                   & avg_spectrum_single_value
            ) const;

    void downsampleSpectralImage(mrf::uint downsampling_factor);

public slots:
    mrf::image::IMAGE_LOAD_SAVE_FLAGS refresh();

    //-------------------------------------------------------------------------
    // Colour mode
    //-------------------------------------------------------------------------
    void setEmissive(bool isEmissive);
    void setReflectiveIlluminant(mrf::radiometry::MRF_ILLUMINANTS illuminant);
    void setSensitivityCurve(mrf::color::SensitivityCurveName curve);
    void setWhitePoint(mrf::color::MRF_WHITE_POINT whitepoint);
    void setColorSpace(mrf::color::MRF_COLOR_SPACE color_space);

    //-------------------------------------------------------------------------
    // Exposure adaptation
    //-------------------------------------------------------------------------

    // Mode
    void setExposureAdaptationMode(SpectralViewerModel::ExposureAdaptation mode);

    // Auto exposure
    void setAutoExposure();

    // Standard
    void setExposure (double value);
    void setGrayPoint(double value);

    // Reinhard
    void setReinhardChromaticAdaptation(double value);
    void setReinhardLightAdaptation    (double value);
    void setReinhardContrast           (double value);
    void setReinhardIntensity          (double value);

    // False colour image
    void setGrayLuminance(double value);
    void setS0Enabled    (bool enable);
    void setS1Enabled    (bool enable);
    void setS2Enabled    (bool enable);
    void setS3Enabled    (bool enable);

    void setSelectedPixel(QPointF const & p);

    //-------------------------------------------------------------------------
    // Transformation
    //-------------------------------------------------------------------------

    void setZoomLevel(double value);
    void setRotation (double value);

    void rotateLeft();
    void rotateRight();

    void setMinSelectedWavelength  (double wl);
    void setMaxSelectedWavelength  (double wl);
    void setSelectedWavelengthRange(double wl_min, double wl_max);

    void setKernelSize(int size);

signals:
    void loadingProgress(int value);
    void loadingMessage(QString const & message);

    void startLoading();
    void loadSuccess();
    void loadFailure();
    void loadSaveStatus(mrf::image::IMAGE_LOAD_SAVE_FLAGS status);
    void processingStarted();
    void processingEnded();

    void xyzUpdated();
    void linearRGBUpdated();
    void tonemappedUpdated();
    void falseColorUpdated();

    //-------------------------------------------------------------------------
    // File meta
    //-------------------------------------------------------------------------
    void widthChanged (int width);
    void heightChanged(int height);
    void bandsChanged (int bands);
    void openedFilenameChanged(QString const & openedFilename);
    void openedFileMetaChanged(QMap<QString, QString> const & openedFileComments);
    void spectralModeChanged  (SpectralViewerModel::Model mode, mrf::radiometry::SpectrumType);
    void spectrumTypeChanged  (mrf::radiometry::SpectrumType spectrumType);

    void minWavelengthChanged(double);
    void maxWavelengthChanged(double);

    //-------------------------------------------------------------------------
    // Colour space information
    //-------------------------------------------------------------------------
    void emissiveModeChanged(bool isEmissive);
    void sensitivityCurveChanged(mrf::color::SensitivityCurveName curve);
    void reflectiveIlluminantChanged(mrf::radiometry::MRF_ILLUMINANTS illuminant);
    void whitePointChanged(mrf::color::MRF_WHITE_POINT illuminant);
    void colorSpaceChanged(mrf::color::MRF_COLOR_SPACE colorSpace);

    //-------------------------------------------------------------------------
    // Tonemapping parameters
    //-------------------------------------------------------------------------
    // Mode
    void exposureAdaptationModeChanged(int mode);

    // Standard
    void sRGBChanged     (bool issRGB);
    void gammaChanged    (double value);
    void exposureChanged (double value);
    void grayPointChanged(double value);

    // Reinhard
    void reinhardChromaticAdaptationChanged(double value);
    void reinhardLightAdaptationChanged    (double value);
    void reinhardContrastChanged           (double value);
    void reinhardIntensityChanged          (double value);

    // Gray image
    void grayLuminanceChanged(double value);
    void s0StateChanged(bool active);
    void s1StateChanged(bool active);
    void s2StateChanged(bool active);
    void s3StateChanged(bool active);

    //-------------------------------------------------------------------------
    // Selection parameters
    //-------------------------------------------------------------------------
    void selectedPixelChanged(QPointF const & p);
    void kernelSizeChanged(int);
    void minSelectedWavelengthChanged(double);
    void maxSelectedWavelengthChanged(double);

    //-------------------------------------------------------------------------
    // Transform parameters
    //-------------------------------------------------------------------------
    void zoomLevelChanged    (double value);
    void rotationChanged     (double value);
    void rotationDeltaChanged(double value);



private:
    void prepareSpectralImage();
    void prepareHDRImage();

    /**
     * @brief updateXYZImage
     * Converts spectral image to XYZ image
     */
    void updateXYZImage(bool updateRGB = true, bool updateTonemapped = true);

    mrf::color::Color spectrumToXYZ(
            std::vector<uint>  const & spectrumWavelengths,
            std::vector<float> const & spectrumValues
            ) const;


    /**
     * @brief updateLinearRGBImage
     * Converts XYZ image to linear RGB image depending on white point
     * and colour space selected
     */
    void updateLinearRGBImage(bool updateTonemapped = true);

    void recalcGrayImage(bool updateGrayImageDisplay = true);
    void updateGrayImage();

    /**
     * @brief updateTonemappedRGBImage
     * Converts linear RGB image to tonemapped RGB image depending on
     * exposure adaptation mode and parameters
     */
    void updateTonemappedRGBImage();


    void getColorImageByLineBasicTonemapping(
            uchar * buffer,
            size_t  num_line
            );

    void getColorImageByLineReinhardTonemapping(
            uchar * buffer,
            size_t  num_line
            );

    //-------------------------------------------------------------------------
    // File meta
    //-------------------------------------------------------------------------
    void setOpenedFilename(QString const & filename);
    void setOpenedFileMeta(QMap<QString, QString> const & meta);
    void setOpenedFileMeta(std::map<std::string, std::string> const & meta);

    void setWidth(size_t width);
    void setHeight(size_t height);
    void setBands(size_t bands);
    void setSpectrumType(mrf::radiometry::SpectrumType type);

    void setMinMaxWavelength(
            double wl_min,
            double wl_max
            );

    void setFileModel(Model mode, mrf::radiometry::SpectrumType);

    /**
     * @brief resetFileinfo
     * When no file is loaded, sets default information
     */
    void resetFileinfo();

    //-------------------------------------------------------------------------
    // File meta
    //-------------------------------------------------------------------------
    OpenMode _openedMode;
    QStringList _openedFileWavelengths;
    mrf::uint _startWavelength = 0;
    mrf::uint _offsetWavelength = 0;
    QString _openedFilename;
    QMap<QString, QString>
            _openedFileMetadata;
    QString _openedFileFolder;
    QString _defaultOpenFolder;
    size_t  _width, _height;
    size_t  _bands;
    double  _minWavelength, _maxWavelength;
    Model   _model;
    bool    _isLoaded;
    bool    _editableEmissive;

    //-------------------------------------------------------------------------
    // Image stats
    //-------------------------------------------------------------------------
    float _grayMin;
    float _grayMax;
    float _grayAvg;
    float _grayMed;

    float _yMin;
    float _yMax;
    float _yAvg;
    float _yMed;

    //-------------------------------------------------------------------------
    // Colour space information
    //-------------------------------------------------------------------------
    mrf::radiometry::SpectrumType _spectrumType;

    mrf::color::SensitivityCurveName _sensitivityCurve;
    mrf::radiometry::MRF_ILLUMINANTS _reflectiveIlluminant;
    mrf::color::SpectrumConverter _converter;
    mrf::color::MRF_WHITE_POINT   _whitePoint;
    mrf::color::MRF_COLOR_SPACE   _colorSpace;

    //-------------------------------------------------------------------------
    // Images
    //-------------------------------------------------------------------------
    std::unique_ptr<mrf::image::UniformSpectralImage> _spectralImage;

    mrf::data_struct::Array2D _xyzImageX;
    mrf::data_struct::Array2D _xyzImageY;
    mrf::data_struct::Array2D _xyzImageZ;

    std::unique_ptr<mrf::image::ColorImage> _linearRGBImage;
    mrf::data_struct::Array2D _grayImage;
    mrf::data_struct::Array2D _s1Image;
    mrf::data_struct::Array2D _s2Image;
    mrf::data_struct::Array2D _s3Image;
    QImage                    _tonemappedImage;
    QImage                    _falseColorImage;

public: // TODO
    std::vector<QImage> _wavelengthImages;

private:
    double _maxLuminance;
    // std::vector<int> _luminance_histogram;

    // In graph mode store a spectrum
    mrf::radiometry::Spectrum _spectrumGraphMode;

    //-------------------------------------------------------------------------
    // Tonemapping parameters
    //-------------------------------------------------------------------------
    // Mode
    ExposureAdaptation _exposureAdaptation;

    // Standard
    double _exposure;
    double _grayPoint;

    // Reinhard
    double _chromaticAdaptation;
    double _lightAdaptation;
    double _contrast;
    double _intensity;

    // Gray image
    double _luminance;
    std::array<bool, 4> _stokesStates;

    //-------------------------------------------------------------------------
    // Selection parameters
    //-------------------------------------------------------------------------
    QPoint _selectedPixel;
    size_t _kernelSize;
    double _minSelectedWavelength, _maxSelectedWavelength;

    //-------------------------------------------------------------------------
    // Transform parameters
    //-------------------------------------------------------------------------
    double  _zoomLevel;
    double  _rotation;


    QFileSystemWatcher _watcher;

    std::vector<std::string> _supportedLoadingRGBFormats;
    std::vector<std::string> _supportedSavingRGBFormats;
    std::vector<std::string> _supportedLoadingSpectralFormats;
    std::vector<std::string> _supportedSavingSpectralFormats;

public:
    static constexpr double DEFAULT_EXPOSURE = 0;
    static constexpr double DEFAULT_GRAY_POINT = 0.5;

    static constexpr double DEFAULT_CHROMATIC_ADAPTATION = 0.5;
    static constexpr double DEFAULT_LIGHT_ADAPTATION = 0;
    static constexpr double DEFAULT_CONTRAST = 0.5;
    static constexpr double DEFAULT_INTENSITY = 0;
    static constexpr double DEFAULT_LUMINANCE = 1.0;

    static constexpr size_t DEFAULT_KERNEL_SIZE = 0;

    static constexpr double DEFAULT_ZOOM_LEVEL = 1.;
};


#ifdef MRF_WITH_EIGEN_SUPPORT

inline float luminance(Eigen::Vector3f const & color)
{
    return mrf::color::LUMINANCE_COEFF[0] * color[0] + mrf::color::LUMINANCE_COEFF[1] * color[1] + mrf::color::LUMINANCE_COEFF[2] * color[2];
}

inline float luminance(Eigen::Array4f const & color)
{
    return mrf::color::LUMINANCE_COEFF[0] * color[0] + mrf::color::LUMINANCE_COEFF[1] * color[1] + mrf::color::LUMINANCE_COEFF[2] * color[2];
}


inline Eigen::Array4f reinhard05Tonemapping(
        Eigen::Array4f const & color,
        float const & chromatic_adaptation,
        Eigen::Array4f const & means,
        float const & lum_mean,
        float const & light_adaptation,
        float const & contrast,
        float const & intensity);

inline Eigen::Array4f reinhard05Tonemapping(
        Eigen::Array4f const & color,
        float const & chromatic_adaptation,
        Eigen::Array4f const & means,
        float const & lum_mean,
        float const & light_adaptation,
        float const & contrast,
        float const & intensity)
{
    //EIGEN version
    //float lum = luminance(color);
    Eigen::Array4f LUM_COEFF(mrf::color::LUMINANCE_COEFF[0], mrf::color::LUMINANCE_COEFF[1], mrf::color::LUMINANCE_COEFF[2], 1.f);
    LUM_COEFF = LUM_COEFF * color;
    float lum = LUM_COEFF[0] + LUM_COEFF[1] + LUM_COEFF[2];

    Eigen::Array4f lc = color * chromatic_adaptation + (1.f - chromatic_adaptation)*lum;
    Eigen::Array4f gc = means * chromatic_adaptation + (1.f - chromatic_adaptation)*lum_mean;
    Eigen::Array4f ca = lc * light_adaptation + gc * (1.f - light_adaptation);

    ca = ca*intensity;

    //ca = ca.pow(contrast);
    ca = (ca.log()*contrast).exp();

    return color/(color+ca);
}

#endif
