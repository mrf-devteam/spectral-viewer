#include "console_logger.h"

#include <iostream>

ConsoleLogger::ConsoleLogger(QSharedPointer<SpectralViewerModel> model)
    : _model(model)
    , _width(60)
{
    connect(_model.data()  , SIGNAL(loadingMessage(QString const &)),
            this           , SLOT(onLoadingMessage(QString const&)));

    connect(_model.data()  , SIGNAL(loadingProgress(int)),
            this           , SLOT(onLoadingProgress(int)));

    connect(_model.data()  , SIGNAL(loadSuccess()),
            this           , SLOT(onLoadSuccess()));

    connect(_model.data()  , SIGNAL(loadFailure()),
            this           , SLOT(onLoadFailure()));
}


ConsoleLogger::~ConsoleLogger() {}


void ConsoleLogger::showMessage(QString const& msg)
{
    std::cout << msg.toStdString() << std::endl;
}


void ConsoleLogger::onLoadingMessage(QString const& msg)
{
    _message = msg;
}


void ConsoleLogger::onLoadingProgress(int percent) const
{
    const size_t barWidth = _width - _message.length();

    std::cout << _message.toStdString() << " [";

    const size_t pos = barWidth * float(percent)/float(100.f);

    for (size_t i = 0; i < barWidth; ++i) {
        if (i <= pos) std::cout << "+";
        else std::cout << " ";
    }

    std::cout << "] (" << int(percent) << " %)  \r";
    std::cout.flush();
}


void ConsoleLogger::onLoadSuccess() const
{
//    const size_t barWidth = _width - _message.length();

//    std::cout << _message.toStdString() << " [";

//    for (size_t i = 0; i < barWidth; ++i) {
//        std::cout << "+";
//    }

//    std::cout << "]    [OK]" << std::endl;
}


void ConsoleLogger::onLoadFailure() const
{
    std::cout << "[Failure]" << std::endl;
}
