/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#ifndef EPS
# define EPS (10. * std::numeric_limits<double>::epsilon())
#endif

#ifndef DBL_EQ
# define DBL_EQ(_a, _b) (std::abs((_a) - (_b)) < EPS)
#endif

#ifndef DBL_NEQ
# define DBL_NEQ(_a, _b) (std::abs((_a) - (_b)) > EPS)
#endif

#ifndef CLAMP
# define CLAMP(_v, _min, _max) \
    std::min(std::max(_v, _min), _max)
#endif

#ifndef M_PI
# define M_PI		3.14159265358979323846	/* pi */
#endif

#include <mrf_core/color/color.hpp>

inline float tosRGB(const float & value) {
    float ret;

    if (value <= 0.0031308f) {
        ret = 12.92f * value;
    } else {
        ret = (1.055f) * std::pow(value, 1.f/2.4f) - 0.055f;
    }

    return ret;
}

inline mrf::color::Color tosRGB(const mrf::color::Color &col) {
    mrf::color::Color ret;

    for (int c = 0; c < 3; c++) {
        ret[c] = tosRGB(col[c]);
    }

    return ret;
}

inline mrf::color::Color fromsRGB(const mrf::color::Color &col) {
    mrf::color::Color ret;

    for (int c = 0; c < 3; c++) {
        if (col[c] <= 0.04045f) {
            ret[c] = col[c]/12.92f;
        } else {
            ret[c] = std::pow((col[c] + 0.055f)/1.055f, 2.4f);
        }
    }

    return ret;
}
