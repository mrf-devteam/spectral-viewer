QT += core gui charts concurrent svg network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SpectralViewer
TEMPLATE = lib
CONFIG += c++14 warn_on

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
# DEFINES += SPECTRALVIEWER_BENCHMARK

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

!mac {
    QMAKE_CXXFLAGS += -fopenmp
    LIBS += -fopenmp
}

LIBS += -L../externals -lMRF


INCLUDEPATH += $$PWD $$PWD/../externals/MRF/ $$PWD/image

SOURCES +=                                                                     \
    console_logger.cpp                                                         \
    mainwindow.cpp                                                             \
    spectral_viewer_model.cpp                                                  \
                                                                               \
    widget/scientific_spin_box.cpp                                             \
                                                                               \
    tools/luminance_histogram_chart.cpp                                        \
    tools/image_info_widget.cpp                                                \
    tools/image_transform_widget.cpp                                           \
    tools/selection_info_widget.cpp                                            \
    tools/spectrum_parameters_widget.cpp                                       \
    tools/tonemapping_widget.cpp                                               \
    tools/spectrum_chart.cpp                                                   \
    tools/graphics_scene.cpp                                                   \
    tools/graphics_view.cpp                                                    \
    tools/polarisation_widget.cpp                                              \
                                                                               \
    dialog/about.cpp                                                           \
    dialog/help.cpp                                                            \
    dialog/export_spectrum_dialog.cpp                                          \
    dialog/open_spectral_image_png_dialog.cpp                                  \
    dialog/save_as_exr_dialog.cpp                                              \
    dialog/welcome_dialog.cpp                                                  \
    tools/wavelengths_info_widget.cpp                                          \
                                                                               \
    image/tiff_color_image.cpp                                                 \
    image/polarised_spectral_image.cpp                                         \
    image/exr_polarised_image.cpp                                              \

HEADERS  +=                                                                    \
    console_logger.h                                                           \
    mainwindow.h                                                               \
    math_macros.h                                                              \
    spectral_viewer_model.h                                                    \
                                                                               \
    widget/scientific_spin_box.h                                               \
                                                                               \
    tools/image_info_widget.h                                                  \
    tools/image_transform_widget.h                                             \
    tools/selection_info_widget.h                                              \
    tools/spectrum_parameters_widget.h                                         \
    tools/tonemapping_widget.h                                                 \
    tools/luminance_histogram_chart.h                                          \
    tools/spectrum_chart.h                                                     \
    tools/abstract_controller.h                                                \
    tools/graphics_scene.h                                                     \
    tools/graphics_view.h                                                      \
    tools/polarisation_widget.h                                                \
                                                                               \
    dialog/about.h                                                             \
    dialog/help.h                                                              \
    dialog/export_spectrum_dialog.h                                            \
    dialog/open_spectral_image_png_dialog.h                                    \
    dialog/save_as_exr_dialog.h                                                \
    dialog/welcome_dialog.h                                                    \
    tools/wavelengths_info_widget.h                                            \
                                                                               \
    image/tiff_color_image.hpp                                                 \
    image/polarised_spectral_image.hpp                                         \
    image/exr_polarised_image.hpp                                              \

FORMS    +=                                                                    \
    mainwindow.ui                                                              \
                                                                               \
    tools/image_info_widget.ui                                                 \
    tools/image_transform_widget.ui                                            \
    tools/selection_info_widget.ui                                             \
    tools/spectrum_parameters_widget.ui                                        \
    tools/tonemapping_widget.ui                                                \
    tools/polarisation_widget.ui                                               \
                                                                               \
    dialog/about.ui                                                            \
    dialog/help.ui                                                             \
    dialog/export_spectrum_dialog.ui                                           \
    dialog/open_spectral_image_png_dialog.ui                                   \
    dialog/save_as_exr_dialog.ui                                               \
    dialog/welcome_dialog.ui                                                   \
    tools/wavelengths_info_widget.ui

RESOURCES +=                                                                   \
    ../resource/resource.qrc                                                   \
    ../resource/themes/theme.qrc                                               \

RC_FILE += ../SpectralViewer.rc
