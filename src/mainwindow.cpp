/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QFile>
#include <QStringList>
#include <QMessageBox>
#include <QFuture>
#include <QtConcurrent>
#include <QProgressBar>
#include <QScrollBar>
#include <QSizePolicy>

#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/util/cpu_memory.hpp>
#include <mrf_core/image/image_formats.hpp>

#include "dialog/about.h"
#include "dialog/help.h"
#include "dialog/open_spectral_image_png_dialog.h"
#include "dialog/save_as_exr_dialog.h"
#include "dialog/welcome_dialog.h"

#include "tools/graphics_scene.h"
#include "tools/spectrum_chart.h"
#include "tools/luminance_histogram_chart.h"
#include "tools/polarisation_widget.h"

#include <vector>

using namespace QtCharts;

MainWindow::MainWindow(const QSharedPointer<SpectralViewerModel>& model,
        QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , _progressDialog  (new QProgressDialog(tr("Loading"), QString(), 0, 100, this))
    , _model(model)
    , _status_bar_memory_consumption(new QLabel(this))
    , _status_bar_progress(new QProgressBar(this))
    , _imageProcessWatcher(new QFutureWatcher<void>(this))
    , _baseWindowTitle("Spectral Viewer " + QApplication::applicationVersion())
    , _checkForUpdates(true)
{
    ui->setupUi(this);
    setAcceptDrops(true);
    setWindowTitle(_baseWindowTitle);

    qRegisterMetaType<mrf::image::IMAGE_LOAD_SAVE_FLAGS>("mrf::image::IMAGE_LOAD_SAVE_FLAGS");
    qRegisterMetaType<SpectralViewerModel::Model>("SpectralViewerModel::Model");
    qRegisterMetaType<mrf::radiometry::SpectrumType>("mrf::radiometry::SpectrumType");

    _status_bar_memory_consumption->setText(tr("Memory consumption: 0 MB"));
    statusBar()->insertPermanentWidget(0, _status_bar_memory_consumption);

    QSizePolicy p = QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored);
    _status_bar_progress->setMaximumWidth(150);
    _status_bar_progress->setSizePolicy(p);
    statusBar()->insertPermanentWidget(1, _status_bar_progress);

    ui->graphicsViewColor->setIsColor  (true);
    ui->graphicsViewColor->setAutoscale(true);
    ui->graphicsViewGray ->setIsColor  (false);

    _toolboxes.push_back({ui->dockImageInfo     , ui->imageInfoWidget});
    _toolboxes.push_back({ui->dockSelection     , ui->selectionInfoWidget});
    _toolboxes.push_back({ui->dockTransform     , ui->imageTransformWidget});
    _toolboxes.push_back({ui->dockTonemapping   , ui->tonemappingWidget});
    _toolboxes.push_back({ui->dockSpectrumPlot  , ui->spectrumChart});
    _toolboxes.push_back({ui->dockSpectrumParams, ui->spectrumParametersWidget});
    _toolboxes.push_back({ui->dockPolarisation  , ui->polarisationWidget});

    ui->spectrumParametersWidget->setSpectrumChart(ui->spectrumChart);

    for (const Toolbox& tb: _toolboxes) {
        ui->menuTools->addAction(tb.dockWidget->toggleViewAction());
    }

    setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);

    // Read user setting -> rest UI and default folder
    readSettings();

    setModel(model);

    _progressDialog->close();

    // We check if a newer version is available
    if (_checkForUpdates) {
        // Not automatic yet:
        // This have to be confirmed at fist launch for privacy concerns
        // Manual check is available in the about dialog.
    }
}



MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::openFile(const QString &fileName)
{
    QFuture<void> imageLoading = QtConcurrent::run([=]() {
        _model->openFile(fileName);
    });

    _imageProcessWatcher->setFuture(imageLoading);
}


void MainWindow::openFolder(
        QStringList const & imageFiles,
        uint start_wavelength,
        uint offset_wavelength
        )
{
    QFuture<void> imageLoading = QtConcurrent::run([=]() {
        _model->openFilesWavelength(imageFiles, start_wavelength, offset_wavelength);
    });

    _imageProcessWatcher->setFuture(imageLoading);
}


void MainWindow::setModel(const QSharedPointer<SpectralViewerModel>& model)
{
    _model = model;

    //-------------------------------------------------------------------------
    // Load save
    //-------------------------------------------------------------------------
    connect(_model.data(), SIGNAL(loadSuccess()),
            this         , SLOT(onLoadSuccess()));

    connect(_model.data(), SIGNAL(loadSaveStatus(mrf::image::IMAGE_LOAD_SAVE_FLAGS)),
            this         , SLOT(onLoadSaveStatus(mrf::image::IMAGE_LOAD_SAVE_FLAGS)));

    connect(_model.data(), SIGNAL(openedFilenameChanged(const QString&)),
            this         , SLOT(onOpenedFilenameChanged(const QString&)));

    connect(_model.data(), SIGNAL(spectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType)),
            this         , SLOT(onSpectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType)));

    //-------------------------------------------------------------------------
    // Progress indicator
    //-------------------------------------------------------------------------

    connect(_model.data()  , SIGNAL(startLoading()),
            _progressDialog, SLOT(show()));

    connect(_model.data()  , SIGNAL(loadingMessage(QString const &)),
            _progressDialog, SLOT(setLabelText(QString const&)));

    connect(_model.data()  , SIGNAL(loadingProgress(int)),
            _progressDialog, SLOT(setValue(int)));

    connect(_model.data()  , SIGNAL(loadSuccess()),
            _progressDialog, SLOT(hide()));

    connect(_model.data()  , SIGNAL(loadFailure()),
            _progressDialog, SLOT(hide()));

    connect(_model.data()  , SIGNAL(processingEnded()),
            _progressDialog, SLOT(hide()));


    connect(_model.data()       , SIGNAL(loadingProgress(int)),
            _status_bar_progress, SLOT(setValue(int)));

    connect(_model.data()  , SIGNAL(loadingMessage(QString const &)),
            statusBar()    , SLOT(showMessage(QString const&)));

    connect(_model.data()  , SIGNAL(startLoading()),
            this           , SLOT(onStartLoading()));

    connect(_model.data()  , SIGNAL(loadSuccess()),
            this           , SLOT(onAfterLoad()));

    connect(_model.data()  , SIGNAL(loadFailure()),
            this           , SLOT(onAfterLoad()));


    connect(_model.data()  , SIGNAL(processingStarted()),
            this           , SLOT(onProcessingStarted()));


    connect(_model.data()  , SIGNAL(processingEnded()),
            this           , SLOT(onProcessingEnded()));

    //-------------------------------------------------------------------------
    // Image views
    //-------------------------------------------------------------------------
    ui->graphicsViewColor->setModel(_model);
    ui->graphicsViewGray ->setModel(_model);

    // Keep the view for colour and gray image at the same position
    connect(ui->graphicsViewGray  , SIGNAL(dragImage(std::pair<int,int>)),
            ui->graphicsViewColor , SLOT(updateHandleBarsImage(std::pair<int, int>)));

    connect(ui->graphicsViewColor , SIGNAL(dragImage(std::pair<int, int>)),
            ui->graphicsViewGray  , SLOT(updateHandleBarsImage(std::pair<int, int>)));

    connect(ui->graphicsViewColor , SIGNAL(autoscaled()),
            ui->graphicsViewGray  , SLOT(autoscale()));


    //-------------------------------------------------------------------------
    // Toolboxes
    //-------------------------------------------------------------------------
    for (const Toolbox& tb : _toolboxes) {
        tb.controler->setModel(_model);
    }

    onSpectralModeChanged(model->model(), model->spectrumType());
}


void MainWindow::resetViewingParameters()
{
    _model->setDefaultTonemappingValues();
    _model->setZoomLevel(1.0);
    _model->setRotation(0.);
}


void MainWindow::showEvent(QShowEvent* event) {
    QMainWindow::showEvent(event);

    QTimer::singleShot(0, this, SLOT(openWelcome()));
}


void MainWindow::openWelcome() {
    // TODO: show a welcome dialog to prompt for opening file / folder
//    if (!_model->isImageLoaded()) {
//        // Prompt for opening a file
//        WelcomeDialog d(this);

//        if (d.exec()) {
//            switch(d.mode()) {
//            case WelcomeDialog::FOLDER:
//                break;
//            case WelcomeDialog::SINGLE_FILE:
//                openFile(d.filepath());
//            }
//        } else {
//            close();
//        }
//    }
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    writeSettings();
    event->accept();
}


void MainWindow::onLoadSuccess()
{
    QString tempnumString;
    tempnumString.setNum(_model->memoryOccupancy(),'f',2);
    tempnumString = "Memory consumption: " + tempnumString + " MB";
    _status_bar_memory_consumption->setText(tempnumString);
}


void MainWindow::errorOpenImage()
{
    setWindowTitle(_baseWindowTitle + " - No Image");
    _status_bar_memory_consumption->setText("Memory consumption: 0 MB");
}


void MainWindow::dropEvent(QDropEvent *ev)
{
    QList<QUrl> urls = ev->mimeData()->urls();

    if (!urls.empty())
    {
        QString fileName = urls[0].toString();
        QString startFileTypeString =
            #ifdef _WIN32
                "file:///";
            #else
                "file://";
            #endif

        if (fileName.startsWith(startFileTypeString))
        {
            fileName = fileName.remove(0, startFileTypeString.length());
            openFile(fileName);
        }
    }
}


void MainWindow::dragEnterEvent(QDragEnterEvent *ev)
{
    ev->acceptProposedAction();
}


void MainWindow::writeSettings()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "manao", "Spectral Viewer");

    settings.beginGroup("MainWindow");
    settings.setValue("geometry"      , saveGeometry());
    settings.setValue("state"         , saveState());
    settings.setValue("check_update"  , _checkForUpdates);
    settings.setValue("gv_splitter"   , ui->graphicsViewSplitter->saveState());
    settings.setValue("current_folder", _model->defaultOpenFolder());
    settings.endGroup();

    for (const Toolbox& tb: _toolboxes) {
        settings.beginGroup(tb.dockWidget->windowTitle());

        settings.setValue("geometry", tb.dockWidget->saveGeometry());
        settings.setValue("active"  , tb.dockWidget->toggleViewAction()->isChecked());
        tb.controler->writeSettings(settings);

        settings.endGroup();
    }
}


void MainWindow::readSettings()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "manao", "Spectral Viewer");

    settings.beginGroup("MainWindow");
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState   (settings.value("state").toByteArray());
    _checkForUpdates = settings.value("check_update", true).toBool();
    ui->graphicsViewSplitter->restoreState(
                settings.value("gv_splitter").toByteArray());
    _model->setDefaultOpenFolder(
                settings.value("current_folder").toString());
    settings.endGroup();

    for (const Toolbox& tb: _toolboxes) {
        settings.beginGroup(tb.dockWidget->windowTitle());

        tb.dockWidget->restoreGeometry(settings.value("geometry").toByteArray());
        tb.dockWidget->toggleViewAction()->setChecked(settings.value("active").toBool());
        tb.controler->readSettings(settings);

        settings.endGroup();
    }
}


void MainWindow::onLoadSaveStatus(mrf::image::IMAGE_LOAD_SAVE_FLAGS status) {
    QString reason;

    switch(status) {
    case mrf::image::MRF_NO_ERROR:
        return;
    case mrf::image::MRF_ERROR_WRONG_EXTENSION:
        reason = tr("wrong extension");
        break;
    case mrf::image::MRF_ERROR_WRONG_FILE_PATH:
        reason = tr("wrong file path.");
        break;
    case mrf::image::MRF_ERROR_LOADING_FILE:
        reason = tr("error loading file.");
        break;
    case mrf::image::MRF_ERROR_DECODING_FILE:
        reason = tr("error decoding file.");
        break;
    case mrf::image::MRF_ERROR_SAVING_FILE:
        reason = tr("error saving file.");
        break;
    case mrf::image::MRF_ERROR_WRONG_PIXEL_FORMAT:
        reason = tr("wrong pixel format.");
        break;
    case mrf::image::MRF_ERROR_WRONG_IMAGE_SIZE:
        reason = tr("wrong image size.");
        break;
    }

    QMessageBox::critical(
                this,
                tr("Error with file: ") + _model->openedFilename(),
                tr("\nError while processing the file: ")
                + reason);
}


void MainWindow::onOpenedFilenameChanged(const QString& filename)
{
    setWindowTitle(_baseWindowTitle + " - " + filename);
}


void MainWindow::onSpectralModeChanged(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType type)
{
    // Activate or De activate toolboxes depending on the model displayed
    for (const Toolbox& tb : _toolboxes) {
        if (tb.controler->isActive(mode, type)) {
//            if (tb.dockWidget->toggleViewAction()->isChecked()) {
                tb.dockWidget->show();
//            }
            tb.dockWidget->toggleViewAction()->setEnabled(true);
        } else {
            tb.dockWidget->hide();
            tb.dockWidget->toggleViewAction()->setEnabled(false);
        }
    }

    ui->graphicsViewGray->setVisible(ui->graphicsViewGray->isActive(mode, type));
    ui->graphicsViewColor->setVisible(ui->graphicsViewColor->isActive(mode, type));

    switch (mode) {
    case SpectralViewerModel::Model::NoFile:
        ui->actionRefresh->setEnabled(false);
        ui->actionRotate_Left->setEnabled(false);
        ui->actionZoomIn->setEnabled(false);
        ui->actionZoomOut->setEnabled(false);
        ui->actionSplitView->setEnabled(false);
        ui->actionGrowView->setEnabled(false);
        ui->actionRotate_Right->setEnabled(false);
        ui->actionSave_Spectral_Image->setEnabled(false);
        //ui->actionSave_as_exr->setEnabled(false);
        ui->actionSave_RGB_image->setEnabled(false);
        break;
    default:
        ui->actionRefresh->setEnabled(true);
        ui->actionRotate_Left->setEnabled(true);
        ui->actionRotate_Right->setEnabled(true);
        ui->actionZoomIn->setEnabled(true);
        ui->actionZoomOut->setEnabled(true);
        ui->actionSplitView->setEnabled(false);
        ui->actionGrowView->setEnabled(false);
        //ui->actionSave_as_exr->setEnabled(mode != SpectralViewerModel::Model::sRGBImage);
        ui->actionSave_RGB_image->setEnabled(true);
        ui->actionSave_Spectral_Image->setEnabled(false);
        break;
    }

    if (mode == SpectralViewerModel::Model::SpectralImage) {
        ui->actionSave_Spectral_Image->setEnabled(true);
        ui->actionSplitView->setEnabled(true);
        ui->actionGrowView->setEnabled(true);
    }
}


void MainWindow::onStartLoading()
{
    statusBar()->showMessage("Loading...");
}


void MainWindow::onProcessingStarted()
{
    statusBar()->showMessage("Busy...");
}


void MainWindow::onProcessingEnded()
{
    statusBar()->clearMessage();
    _status_bar_progress->setValue(0);
}


void MainWindow::onAfterLoad()
{
    statusBar()->clearMessage();
}


void MainWindow::on_actionOpenFolder_triggered()
{
    OpenSpectralImagePngDialog widget(this, _model->defaultOpenFolder());
    if (widget.exec())
    {
        QString dirPath = widget.folderPath();
        QDir dir(dirPath);

        //change current directory
        QDir::setCurrent(dirPath);

        QStringList filters;
        filters << "*.png";
        filters << "*.exr";
        filters << "*.jpeg";
        filters << "*.jpg";
#ifdef HAS_TIFF
        filters << "*.tiff";
        filters << "*.tif";
#endif
        dir.setNameFilters(filters);

        QStringList imageFiles = dir.entryList();

        uint start_wavelength = widget.startWavelength();
        uint offset_wavelength = widget.offsetWavelength();

        openFolder(imageFiles, start_wavelength, offset_wavelength);
    }
}


void MainWindow::on_actionOpenFile_triggered()
{
    std::string formatFilter = "Image (";
    for (const std::string& supportedLoading: _model->getSupportedLoadingRGBFormats()) {
        formatFilter += "*" + supportedLoading + " ";
    }

    for (const std::string& supportedLoading: _model->getSupportedLoadingSpectralFormats()) {
        formatFilter += "*" + supportedLoading + " ";
    }

    formatFilter += ")";

    QString fileName = QFileDialog::getOpenFileName(
                this,
                tr("Open ENVI / ARTRAW / EXR / Image"),
                _model->defaultOpenFolder(),
                tr(formatFilter.c_str()));

    if (fileName.size() != 0) {
        openFile(fileName);
    }
}


void MainWindow::on_actionSave_Spectral_Image_triggered()
{
    if (_model->model() != SpectralViewerModel::SpectralImage) {
        return;
    }

    std::string formatFilter = "Spectral Image (";

    for (const std::string& supportedSaving: _model->getSupportedSavingSpectralFormats()) {
        formatFilter += "*" + supportedSaving + " ";
    }

    formatFilter += ")";

    QString fileName = QFileDialog::getSaveFileName(
                this,
                tr("Save Tonemapped Image"),
                _model->defaultOpenFolder(),
                tr(formatFilter.c_str()));

    if (fileName.size() > 0) {
        QFuture<void> imageSaving = QtConcurrent::run([=]() {
            _model->saveSpectralImage(fileName);
        });

        _imageProcessWatcher->setFuture(imageSaving);
    }
}


void MainWindow::on_actionSave_RGB_image_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(
                this,
                tr("Save Tonemapped Image"),
                _model->defaultOpenFolder(),
                tr("Image Files (*.png *.jpg *.bmp *.exr *.tiff *.tif)"));

    if (fileName.size() > 0) {
        QFuture<void> imageSaving = QtConcurrent::run([=]() {
            _model->saveColorImage(fileName);
        });

        _imageProcessWatcher->setFuture(imageSaving);
    }
}


void MainWindow::on_actionExit_triggered()
{
    close();
}


//-------------------------------------------------------------------------
// View menu actions
//-------------------------------------------------------------------------

void MainWindow::on_actionMemory_Occupancy_triggered()
{
    QMessageBox::information(
                this,
                tr("Memory info"),
                tr("Memory occupancy: ") +
                QString::number(_model->memoryOccupancy()) +
                tr(" MB"));
}


//-------------------------------------------------------------------------
// Help menu actions
//-------------------------------------------------------------------------

void MainWindow::on_actionHelp_triggered()
{
    Help help_window(this);
    help_window.exec();
}


void MainWindow::on_actionAbout_triggered()
{
    About about_window(this);
    about_window.exec();
}


void MainWindow::on_actionRotate_Right_triggered()
{
    _model->rotateRight();
}


void MainWindow::on_actionRotate_Left_triggered()
{
    _model->rotateLeft();
}


void MainWindow::on_actionRefresh_triggered()
{
    _model->refresh();
}


void MainWindow::on_actionZoomIn_triggered()
{
    _model->zoomLevelChanged(_model->zoomLevel() * 1.1);
}


void MainWindow::on_actionZoomOut_triggered()
{
    _model->zoomLevelChanged(_model->zoomLevel() / 1.1);
}


void MainWindow::on_actionSplitView_triggered()
{
    const int w = ui->graphicsViewSplitter->width();
    ui->graphicsViewSplitter->setSizes(QList<int>({w/2, w/2}));
    ui->graphicsViewColor->autoscale();
}

void MainWindow::on_actionGrowView_triggered()
{
    const int w = ui->graphicsViewSplitter->width();
    ui->graphicsViewSplitter->setSizes(QList<int>({w/2, 0}));
    ui->graphicsViewColor->autoscale();
}
