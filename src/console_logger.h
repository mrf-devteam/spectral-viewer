/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QObject>
#include <QSharedPointer>
#include "spectral_viewer_model.h"

class ConsoleLogger : public QObject
{
    Q_OBJECT
public:
    ConsoleLogger(QSharedPointer<SpectralViewerModel> model);
    ~ConsoleLogger();

    void showMessage(QString const& msg);

private slots:
    void onLoadingMessage(QString const& msg);
    void onLoadingProgress(int percent) const;

    void onLoadSuccess() const;
    void onLoadFailure() const;

private:
    QSharedPointer<SpectralViewerModel> _model;
    QString _message;
    size_t _width;
};

