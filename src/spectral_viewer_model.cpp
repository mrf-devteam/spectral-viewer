﻿/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <QImage>
#include <QtGlobal>
#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
#include <QColorSpace>
#endif
#include <QDebug>
#include <QDir>
#include <QUrl>
#include <QFileInfo>

#include <limits>

#include <mrf_core/radiometry/d65.hpp>
#include <mrf_core/radiometry/d50.hpp>
#include "radiometry/e.hpp"
#include <mrf_core/util/precision_timer.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/polarised_spectral_image.hpp>

// Additional supported formats
#include "image/tiff_color_image.hpp"
#include "image/openexr_color_image.hpp"
#include "image/openexr_spectral_image.hpp"

#include "spectral_viewer_model.h"

#include "math_macros.h"

#ifdef SPECTRALVIEWER_BENCHMARK
#define BENCHMARK_START                 \
    mrf::util::PrecisionTimer a_timer;  \
    a_timer.start();

#define BENCHMARK_END(_msg)                                         \
    std::cout <<"[BENCHMARKING] In " << __FILE__ << ":" << __LINE__ \
      << "\t" << _msg << "\t"                                    \
      << a_timer;

#else
#define BENCHMARK_START
#define BENCHMARK_END(_msg)
#endif

using mrf::color::MRF_COLOR_SPACE;
using mrf::radiometry::MRF_ILLUMINANTS;
using mrf::color::Color;
using mrf::radiometry::Spectrum;
using mrf::uint;
using mrf::uchar;


SpectralViewerModel::SpectralViewerModel(QObject *parent)
    : QObject(parent)
    //-------------------------------------------------------------------------
    , _openedFilename      ("")
    , _openedFileFolder    ("")
    , _defaultOpenFolder   ("")
    , _width(0), _height(0)
    , _bands(0)
    , _minWavelength(0.), _maxWavelength(0.)
    , _model(NoFile)
    , _isLoaded(false)
    , _editableEmissive(false)
    //-------------------------------------------------------------------------
    , _grayMin(NAN)
    , _grayMax(NAN)
    , _grayAvg(NAN)
    , _grayMed(NAN)
    , _yMin(NAN)
    , _yMax(NAN)
    , _yAvg(NAN)
    , _yMed(NAN)
    //-------------------------------------------------------------------------
    , _spectrumType(mrf::radiometry::SPECTRUM_NONE)
    , _sensitivityCurve(mrf::color::SensitivityCurveName::CIE_1931_2DEG)
    , _reflectiveIlluminant(mrf::radiometry::MRF_ILLUMINANTS::D65)
    , _converter (_sensitivityCurve)
    , _whitePoint(mrf::color::D65)
    , _colorSpace(mrf::color::SRGB)
    //-------------------------------------------------------------------------
    , _spectralImage (new mrf::image::EnviSpectralImage)
    , _xyzImageX     (size_t(0), size_t(0))
    , _xyzImageY     (size_t(0), size_t(0))
    , _xyzImageZ     (size_t(0), size_t(0))
    , _linearRGBImage(new mrf::image::EXRColorImage)
    , _grayImage     (size_t(0), size_t(0))
    , _s1Image       (size_t(0), size_t(0))
    , _s2Image       (size_t(0), size_t(0))
    , _s3Image       (size_t(0), size_t(0))
    //-------------------------------------------------------------------------
     , _maxLuminance(0)
    // , _luminance_histogram(50, 0)
    //, _spectrumGraphMode()
    //-------------------------------------------------------------------------
    , _exposureAdaptation(Standard)
    , _exposure           (DEFAULT_EXPOSURE)                // Standard
    , _grayPoint          (DEFAULT_GRAY_POINT)              // Standard
    , _chromaticAdaptation(DEFAULT_CHROMATIC_ADAPTATION)    // Reinhard
    , _lightAdaptation    (DEFAULT_LIGHT_ADAPTATION)        // Reinhard
    , _contrast           (DEFAULT_CONTRAST)                // Reinhard
    , _intensity          (DEFAULT_INTENSITY)               // Reinhard
    , _luminance          (DEFAULT_LUMINANCE)
    , _stokesStates       ({true, false, false, false})
    //-------------------------------------------------------------------------
    , _selectedPixel(0, 0)
    , _kernelSize   (DEFAULT_KERNEL_SIZE)
    , _minSelectedWavelength(0), _maxSelectedWavelength(0)
    , _zoomLevel(DEFAULT_ZOOM_LEVEL)
    , _rotation (0)
    //-------------------------------------------------------------------------
    , _supportedLoadingRGBFormats(
        {
          ".bmp",
          ".exr",
          ".gif",
          ".hdr",
          ".jpg", ".jpeg",
          ".png",
          ".pfm",
          ".pgm",
          ".ppm",
          #ifdef HAS_TIFF
          ".tif", ".tiff",
          #endif // HAS_TIFF
          ".xbm",
          ".xpm"
          })
    , _supportedSavingRGBFormats(
        {
          ".bmp",
          ".exr",
          ".gif",
          ".hdr",
          ".jpg", ".jpeg",
          ".png",
          ".pfm",
          ".pgm",
          ".ppm",
          //#ifdef HAS_TIFF // Although possible, not tested
          //".tif", ".tiff",
          //#endif // HAS_TIFF
          ".xbm",
          ".xpm"
        })
    , _supportedLoadingSpectralFormats(
        {
          ".exr",
          ".artraw",
          ".hdr", // ".raw", ".dat", ".bil", // We do not want to display those
          ".spd"
        })
    , _supportedSavingSpectralFormats(
        {
          ".exr",
          ".artraw",
          ".hdr", ".raw", ".dat", ".bil",
          ".spd"
        })
{
//    connect(&watcher, SIGNAL(fileChanged(const QString&)),
//            this    , SLOT(refresh()));
}


mrf::image::IMAGE_LOAD_SAVE_FLAGS SpectralViewerModel::openFile(
        const QString &filepath
        )
{
    BENCHMARK_START;

    if (!_watcher.files().empty()) {
        _watcher.removePaths(_watcher.files());
    }

    resetFileinfo();
    emit startLoading();
    emit loadingProgress(0);

    mrf::image::IMAGE_LOAD_SAVE_FLAGS status = mrf::image::MRF_NO_ERROR;
    SpectralViewerModel::Model fileModel = NoFile;

    // TODO: use Imageformat way for determining if we have a spectral or RGB image
    if (filepath.endsWith(".hdr", Qt::CaseInsensitive)) {
        if (mrf::image::isRadianceHDR(filepath.toStdString())) {
            emit loadingMessage(tr("Loading HDR image file..."));
            status = openRgbImage(filepath);
            fileModel = HDRImage;
        } else {
            emit loadingMessage(tr("Loading ENVI Spectral file..."));
            status = openHyperSpectral(filepath);
            fileModel = SpectralImage;
        }
    } else if (filepath.endsWith(".exr", Qt::CaseInsensitive)) {
        // TODO: replace with OpenEXR when available
#ifdef HAS_OPENEXR
        if (mrf::image::OpenEXRSpectralImage::isSpectral(filepath.toStdString())) {
#else
        if (mrf::image::EXRSpectralImage::isSpectral(filepath.toStdString())) {
#endif // HAS_OPENEXR
            emit loadingMessage(tr("Loading Spectral EXR image file..."));
            status = openSpectralEXR(filepath);
            fileModel = SpectralImage;
        } else {
            emit loadingMessage(tr("Loading RGB EXR image file..."));
            status = openRgbImage(filepath);
            fileModel = HDRImage;
        }
    } else if (filepath.endsWith(".raw", Qt::CaseInsensitive)
               || filepath.endsWith(".dat", Qt::CaseInsensitive)
               || filepath.endsWith(".bil", Qt::CaseInsensitive)) {
        emit loadingMessage(tr("Loading ENVI Spectral file..."));
        status = openHyperSpectral(filepath);
        fileModel = SpectralImage;
    } else if (filepath.endsWith(".artraw", Qt::CaseInsensitive)) {
        emit loadingMessage(tr("Loading ART Raw file..."));
        status = openARTRaw(filepath);
        fileModel = SpectralImage;
    } else if (filepath.endsWith(".pfm" , Qt::CaseInsensitive)
           #ifdef HAS_TIFF
               || filepath.endsWith(".tif" , Qt::CaseInsensitive)
               || filepath.endsWith(".tiff" , Qt::CaseInsensitive)
           #endif // HAS_TIFF
    ) {
        emit loadingMessage(tr("Loading HDR image file..."));
        status = openRgbImage(filepath);
        fileModel = HDRImage;
    } else if (filepath.endsWith(".png" , Qt::CaseInsensitive)
               || filepath.endsWith(".bmp" , Qt::CaseInsensitive)
               || filepath.endsWith(".gif" , Qt::CaseInsensitive)
               || filepath.endsWith(".jpg" , Qt::CaseInsensitive)
               || filepath.endsWith(".jpeg", Qt::CaseInsensitive)
               || filepath.endsWith(".pbm" , Qt::CaseInsensitive)
               || filepath.endsWith(".ppm" , Qt::CaseInsensitive)
               || filepath.endsWith(".xbm" , Qt::CaseInsensitive)
               || filepath.endsWith(".xpm" , Qt::CaseInsensitive)
    ) {
        emit loadingMessage(tr("Loading RGB file..."));
        status = openRgbImage(filepath);
        fileModel = sRGBImage;
    } else if (filepath.endsWith(".spd", Qt::CaseInsensitive)) {
        emit loadingMessage(tr("Loading spectrum file..."));
        status = openSpd(filepath);
        fileModel = Spectrum;
    } else {
        status = mrf::image::MRF_ERROR_WRONG_EXTENSION;
        fileModel = NoFile;
    }

    if (   fileModel == SpectralImage
        && _spectralImage->width() == 1
        && _spectralImage->height() == 1)
    {
        fileModel = Spectrum;
    }

    // Control loading status
    if (status == mrf::image::MRF_NO_ERROR) {
        setFileModel(fileModel, spectrumType());

        switch (fileModel) {
        case SpectralImage:
        case Spectrum:
            prepareSpectralImage();
            break;
        case HDRImage:
            prepareHDRImage();
            setBands(3);
            break;
        case sRGBImage:
            setBands(3);
            break;
        case NoFile:
            // This shall not happen
            assert(0);
            break;
        }

        setOpenedFilename(filepath);
        _isLoaded = true;
        _openedMode = SINGLE_FILE;
        _watcher.addPath(filepath);

        emit loadSuccess();
        //setSelectedPixel(QPointF(0., 0.));
        _selectedPixel = QPoint(0, 0);
        emit selectedPixelChanged(_selectedPixel);
    } else {
        resetFileinfo();
        setOpenedFilename("No Image");

        emit loadFailure();
    }

    emit loadingProgress(100);
    emit loadSaveStatus(status);

    BENCHMARK_END("openFile");

    return status;
}


mrf::image::IMAGE_LOAD_SAVE_FLAGS SpectralViewerModel::openFilesWavelength(
        QStringList const & filepaths,
        uint startWavelength, uint offsetWavelength
        )
{
    BENCHMARK_START;

    if (!_watcher.files().empty()) {
        _watcher.removePaths(_watcher.files());
    }

    resetFileinfo();
    emit startLoading();
    emit loadingProgress(0);

    mrf::image::IMAGE_LOAD_SAVE_FLAGS status = mrf::image::MRF_NO_ERROR;

    const float progressStep = 1.0F/float(filepaths.size());

    QStringList sortedFiles(filepaths);
    std::sort(sortedFiles.begin(), sortedFiles.end());

    std::vector<float> pixelValues;

    if (filepaths.empty()) {
        resetFileinfo();
        setOpenedFilename("No Image");
        emit loadFailure();
        return mrf::image::MRF_ERROR_WRONG_FILE_PATH;
    }

    _spectralImage->clear();

    for (int i = 0; i < sortedFiles.size(); i++) {
        emit loadingProgress(100.0F * i *progressStep );

        const QString& filepath = sortedFiles[i];
        const uint wavelength = startWavelength + i * offsetWavelength;

        // Load image using Qt
        if (   filepath.endsWith(".png", Qt::CaseInsensitive)
            || filepath.endsWith(".jpg", Qt::CaseInsensitive)
            || filepath.endsWith(".jpeg", Qt::CaseInsensitive)) {
            QImage currImage(filepath);

            // Ensure proper data alignment
            if (currImage.format() != QImage::Format_RGB32) {
                currImage = currImage.convertToFormat(QImage::Format_RGB32);
            }

            pixelValues.resize(currImage.width() * currImage.height());

            for (int y = 0; y < currImage.height(); y++) {
                const QRgb* scanline = (QRgb*)(currImage.scanLine(y));

                for (int x = 0; x < currImage.width(); x++) {
                    // We don't apply inverse gamma: we assume we have linerarly encoded
                    // value maps.
                    QRgb pixel = scanline[x];
                    pixelValues[y * currImage.width() + x] =
                            float(
                                qRed(pixel)
                              + qGreen(pixel)
                              + qBlue(pixel)
                            ) / (3.F * 255.F);
                }
            }

            status = _spectralImage->addValuesFromBuffer(
                pixelValues,
                currImage.width(),
                currImage.height(),
                wavelength
                );
        }
        // Using MRF
        else if (filepath.endsWith(".exr", Qt::CaseInsensitive)) {
            status = _spectralImage->addValuesFromGrayImage(
                        filepath.toStdString(),
                        wavelength);
        }
#ifdef HAS_TIFF
        // TODO: later merge with previous when TIFF will be part of MRF
        else if (filepath.endsWith(".tiff", Qt::CaseInsensitive) ||
                 filepath.endsWith(".tif", Qt::CaseInsensitive)) {
            mrf::image::TIFFColorImage image(filepath.toStdString());
            _spectralImage->addValuesFromGrayImage(image, wavelength);
        }
#endif // HAS_TIFF

        if (status != mrf::image::MRF_NO_ERROR) {
            resetFileinfo();
            setOpenedFilename("No Image");
            emit loadFailure();
            return status;
        }
    }

    QString parentFolder = QFileInfo(filepaths[0]).absolutePath();

    setSpectrumType(mrf::radiometry::SPECTRUM_EMISSIVE);
    _editableEmissive = true;

    setFileModel(SpectralImage, _spectrumType);
    setOpenedFilename(parentFolder);


    prepareSpectralImage();

    setZoomLevel(1.);
    setRotation(0.);
//    setSelectedPixel(QPointF(0., 0.));
    _selectedPixel = QPoint(0, 0);
    emit selectedPixelChanged(_selectedPixel);
    _isLoaded = true;

    _watcher.addPath(parentFolder);

    _openedFileWavelengths = filepaths;
    _startWavelength = startWavelength;
    _offsetWavelength = offsetWavelength;
    _openedMode = MULTIPLE_FILES;
    emit loadSuccess();

    emit loadingProgress(100);
    emit loadSaveStatus(status);

    BENCHMARK_END("openFilesWavelength");

    return mrf::image::MRF_NO_ERROR;
}


mrf::image::IMAGE_LOAD_SAVE_FLAGS SpectralViewerModel::openHyperSpectral(
    const QString &filepath
    )
{
    BENCHMARK_START;
    QString header_filepath;
    QString data_filepath;
    const QString nameNoExt = filepath.left(filepath.lastIndexOf('.'));

    if (filepath.endsWith(".hdr", Qt::CaseInsensitive)) {
        // The data path is provided
        data_filepath = filepath;

        // Header file does not always have .raw extension, trying to guess
        const QString headerNameRaw = nameNoExt + ".raw";
        const QString headerNameDat = nameNoExt + ".dat";
        const QString headerNameBil = nameNoExt + ".bil";

        if (QFileInfo::exists(headerNameRaw) && QFileInfo(headerNameRaw).isFile()) {
            header_filepath = headerNameRaw;
        } else if (QFileInfo::exists(headerNameDat) && QFileInfo(headerNameDat).isFile()) {
            header_filepath = headerNameDat;
        } else if (QFileInfo::exists(headerNameBil) && QFileInfo(headerNameBil).isFile()) {
            header_filepath = headerNameBil;
        } else {
            return mrf::image::MRF_ERROR_WRONG_FILE_PATH;
        }
    } else if (filepath.endsWith(".raw", Qt::CaseInsensitive) ||
               filepath.endsWith(".dat", Qt::CaseInsensitive) ||
               filepath.endsWith(".bil", Qt::CaseInsensitive)) {
        // The header path is provided
        header_filepath = filepath;
        data_filepath = nameNoExt + ".hdr";
    } else {
        return mrf::image::MRF_ERROR_WRONG_EXTENSION;
    }

    try {
        _spectralImage = std::unique_ptr<mrf::image::EnviSpectralImage>(
                    new mrf::image::EnviSpectralImage(
                        data_filepath.toStdString(),
                        header_filepath.toStdString(),
                        [this](int p) {
            emit loadingProgress(p);
        }));
    } catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS const& status) {
        return status;
    }

    setOpenedFileMeta(_spectralImage->metadata());
    setSpectrumType(mrf::radiometry::SPECTRUM_EMISSIVE);
    _editableEmissive = true;

    BENCHMARK_END("openHyperSpectral");

    return mrf::image::MRF_NO_ERROR;
}


mrf::image::IMAGE_LOAD_SAVE_FLAGS SpectralViewerModel::openARTRaw(
        QString const & filepath
        )
{
    BENCHMARK_START;

    try {
        mrf::image::ArtRawSpectralImage* image = new mrf::image::ArtRawSpectralImage(
                        filepath.toStdString(),
                        [this](int p) {
            emit loadingProgress(p);
        });

        setSpectrumType(image->getSpectrumType());

        _spectralImage = std::unique_ptr<mrf::image::ArtRawSpectralImage>(image);
    } catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS const& status) {
        return status;
    }

    _editableEmissive = false;

    BENCHMARK_END("openARTRaw");

    return mrf::image::MRF_NO_ERROR;
}


mrf::image::IMAGE_LOAD_SAVE_FLAGS SpectralViewerModel::openSpectralEXR(
        const QString &filepath
        )
{
    BENCHMARK_START;

    try {
        // For now, the OpenEXR implementation is experimental in this software
#ifdef HAS_OPENEXR
        mrf::image::OpenEXRSpectralImage* image = new mrf::image::OpenEXRSpectralImage(
                        filepath.toStdString(),
                        [this](int p) {
            emit loadingProgress(p);
        });

        _spectralImage = std::unique_ptr<mrf::image::OpenEXRSpectralImage>(image);
#else
        mrf::image::EXRSpectralImage* image = new mrf::image::EXRSpectralImage(
                        filepath.toStdString(),
                        [this](int p) {
            emit loadingProgress(p);
        });

        _spectralImage = std::unique_ptr<mrf::image::EXRSpectralImage>(image);
#endif // HAS_OPENEXR

        setSpectrumType(image->getSpectrumType());
    } catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS const& status) {
        return status;
    }

    _editableEmissive = false;

    BENCHMARK_END("openSpectralEXR");

    return mrf::image::MRF_NO_ERROR;
}


mrf::image::IMAGE_LOAD_SAVE_FLAGS SpectralViewerModel::openSpd(
        QString const & filepath
        )
{
    BENCHMARK_START;

    _spectrumGraphMode.clear();
    _spectralImage->clear();

    mrf::radiometry::Spectrum loadedSpectrum;

    if (!loadedSpectrum.loadSPDFile(filepath.toStdString())) {
        return mrf::image::MRF_ERROR_LOADING_FILE;
    }

    const std::vector<uint>  & wavelengths = loadedSpectrum.wavelengths();
    const std::vector<float> & values      = loadedSpectrum.values();

    std::vector<std::pair<uint, float>> spectrum(loadedSpectrum.size());

    // TODO: this is a bit hacky now. This shall be fixed in MRF though...

    // 1 - populate the spectrum with wavelengths, values
    for (size_t i = 0; i < loadedSpectrum.size(); i++) {
        spectrum[i] = std::pair<uint, float>(wavelengths[i], values[i]);
    }

    // 2 - sort the vector
    std::sort(spectrum.begin(), spectrum.end(),
              [](const std::pair<uint, float>& a, const std::pair<uint, float>&b) -> bool {
                return a.first < b.first;
              });

    // 3 - compact the vector:
    // duplicated wavelengths are gathered and averaged
    std::vector<std::pair<uint, float>> compactedSpectrum;
    compactedSpectrum.push_back(spectrum[0]);

    for (size_t i = 1, j = 0, n = 1; i < spectrum.size(); i++) {
        if (compactedSpectrum[j].first == spectrum[i].first) {
            compactedSpectrum[j].second += spectrum[i].second;
            ++n;
        } else {
            compactedSpectrum[j].second /= float(n);
            compactedSpectrum.push_back(spectrum[i]);
            ++j;
            n = 1;
        }
    }

    // 4 - split the pair in two individual vectors for spectrum initialization
    std::vector<uint> compactedWavelengths(compactedSpectrum.size());
    std::vector<float> compactedValues(compactedSpectrum.size());

    for (size_t i = 0; i < compactedSpectrum.size(); i++) {
        compactedWavelengths[i] = compactedSpectrum[i].first;
        compactedValues     [i] = compactedSpectrum[i].second;
    }

    // 4 - Initialize the spectrum
    _spectrumGraphMode = mrf::radiometry::Spectrum(compactedWavelengths, compactedValues);

    for (size_t i = 0; i < _spectrumGraphMode.size(); i++) {
        _spectralImage->addValuesFromBuffer(
                    &_spectrumGraphMode.values()[i],
                    1, 1,
                    _spectrumGraphMode.wavelengths()[i]);
    }

    setSpectrumType(mrf::radiometry::SPECTRUM_EMISSIVE);
    _editableEmissive = true;

    BENCHMARK_END("openSpd");

    return mrf::image::MRF_NO_ERROR;
}


mrf::image::IMAGE_LOAD_SAVE_FLAGS SpectralViewerModel::openRgbImage(
        QString const & filepath
        )
{
    BENCHMARK_START;

    resetFileinfo();

    mrf::image::IMAGE_LOAD_SAVE_FLAGS status = mrf::image::MRF_NO_ERROR;

    if (filepath.endsWith(".hdr", Qt::CaseInsensitive)      // Ward hdr format
        || filepath.endsWith(".exr", Qt::CaseInsensitive)
        || filepath.endsWith(".pfm", Qt::CaseInsensitive)
    #ifdef HAS_TIFF
        || filepath.endsWith(".tif" , Qt::CaseInsensitive)
        || filepath.endsWith(".tiff" , Qt::CaseInsensitive)
    #endif // HAS_TIFF
    ) {
        // We support those file formats with MRF
        try {
            if (filepath.endsWith(".hdr", Qt::CaseInsensitive)) {
                _linearRGBImage = std::unique_ptr<mrf::image::ColorImage>(
                            new mrf::image::HDRColorImage(filepath.toStdString()));
            } else if (filepath.endsWith(".exr", Qt::CaseInsensitive)) {
                #ifdef HAS_OPENEXR
                _linearRGBImage = std::unique_ptr<mrf::image::ColorImage>(
                            new mrf::image::OpenEXRColorImage(filepath.toStdString()));
                #else // Use TinyEXR implementation
                _linearRGBImage = std::unique_ptr<mrf::image::ColorImage>(
                            new mrf::image::EXRColorImage(filepath.toStdString()));
                #endif // HAS_OPENEXR
            } else if (filepath.endsWith(".pfm", Qt::CaseInsensitive)) {
                _linearRGBImage = std::unique_ptr<mrf::image::ColorImage>(
                            new mrf::image::PFMColorImage(filepath.toStdString()));
            }
        #ifdef HAS_TIFF
            else if (filepath.endsWith(".tif", Qt::CaseInsensitive)
                     || filepath.endsWith(".tiff", Qt::CaseInsensitive)) {
                _linearRGBImage = std::unique_ptr<mrf::image::ColorImage>(
                            new mrf::image::TIFFColorImage(filepath.toStdString()));
            }
        #endif // HAS_TIFF
        }  catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS const& s) {
            status = s;
        }

        //status = _linearRGBImage->load(filepath.toStdString());

        if (status == mrf::image::MRF_NO_ERROR) {
            setWidth(_linearRGBImage->width());
            setHeight(_linearRGBImage->height());
            setOpenedFileMeta(_linearRGBImage->metadata());
        }
    } else {
        // We use Qt QImage instead
        _tonemappedImage = QImage(filepath);

        if (_tonemappedImage.format() != QImage::Format_RGB888) {
            _tonemappedImage = _tonemappedImage.convertToFormat(QImage::Format_RGB888);
        }

        if (_tonemappedImage.width() > 0) {
            _linearRGBImage = std::unique_ptr<mrf::image::ColorImage>(new mrf::image::EXRColorImage);

            _linearRGBImage->init(
                        uint(_tonemappedImage.width()),
                        uint(_tonemappedImage.height()));

            for (int y = 0; y < _tonemappedImage.height(); y++) {
                const uchar* scanline = _tonemappedImage.scanLine(y);
                for (int x = 0; x < _tonemappedImage.width(); x++) {
                    _linearRGBImage->setPixel(
                                x, y,
                                fromsRGB(Color(
                                    float(scanline[3*x    ]),
                                    float(scanline[3*x + 1]),
                                    float(scanline[3*x + 2]))
                                    / std::numeric_limits<unsigned char>::max()));
                }
            }

            setWidth(_tonemappedImage.width());
            setHeight(_tonemappedImage.height());

            setOpenedFileMeta(QMap<QString, QString>());
        } else {            
            status = mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_ERROR_DECODING_FILE;
        }
    }

    if (status != mrf::image::MRF_NO_ERROR) {
        resetFileinfo();
        setOpenedFilename("No Image");
    }

    BENCHMARK_END("openRgbImage");

    return status;
}


void SpectralViewerModel::saveSpectralImage(
        QString const & filepath
        )
{
    BENCHMARK_START;
    emit loadingProgress(0);
    auto status = mrf::image::MRF_NO_ERROR;

    if (   filepath.endsWith(".artraw", Qt::CaseInsensitive)
        || filepath.endsWith(".exr", Qt::CaseInsensitive)
        || filepath.endsWith(".hdr", Qt::CaseInsensitive)) {
         status = mrf::image::save(*_spectralImage, filepath.toStdString(), "", [this](int p) {
            emit loadingProgress(p);
        });
    } else {
        status = _spectralImage->saveAsGrayPngImages(filepath.toStdString());
    }

    emit loadingProgress(100);
    emit loadSaveStatus(status);

    BENCHMARK_END("saveSpectralImage");
}


// void SpectralViewerModel::saveSpectralValuesAsExr(
//         QString const & filepath,
//         uint            wavelengthR,
//         uint            wavelengthG,
//         uint            wavelengthB
//         )
// {
//     BENCHMARK_START;

//     uint waves[3];
//     waves[0] = wavelengthR;
//     waves[1] = wavelengthG;
//     waves[2] = wavelengthB;

//     auto status = _spectralImage->saveAsExr(filepath.toStdString(), waves);
//     emit loadSaveStatus(status);

//     BENCHMARK_END("saveSpectralValuesAsExr");
// }


bool SpectralViewerModel::saveColorImage(
        QString const & filepath
        )
{
    BENCHMARK_START;
    emit loadingProgress(0);

    mrf::image::IMAGE_LOAD_SAVE_FLAGS status = mrf::image::MRF_NO_ERROR;
    bool noError = true;

    if (filepath.endsWith(".exr", Qt::CaseInsensitive)
     || filepath.endsWith(".hdr", Qt::CaseInsensitive)
     || filepath.endsWith(".pfm", Qt::CaseInsensitive)) {
        mrf::image::EXRColorImage cimg(*_linearRGBImage);

        for (size_t i = 0; i < cimg.width() * cimg.height(); i++) {
            cimg[i] *= std::exp2(_exposure);
        }


        status = mrf::image::save(cimg, filepath.toStdString());
        noError = status == mrf::image::MRF_NO_ERROR;
    }
#ifdef HAS_TIFF
    else if (  filepath.endsWith(".tif", Qt::CaseInsensitive)
            || filepath.endsWith(".tiff", Qt::CaseInsensitive)) {
        try {
            mrf::image::TIFFColorImage img(*_linearRGBImage);
            status = img.save(
                        filepath.toStdString(),
                        "",
                        [this](int p) {
                            emit loadingProgress(p);
                        });
        }  catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS &s) {
            status = s;
            noError = false;
        }
    }
#endif // HAS_TIFF
    else {
        noError = _tonemappedImage.save(filepath);
        if (!noError) {
            status = mrf::image::MRF_ERROR_SAVING_FILE;
        }
    }

    emit loadingProgress(100);
    emit loadSaveStatus(status);

    BENCHMARK_END("saveColorImage");

    return noError;
}


void SpectralViewerModel::saveSpectrum(
        uint i, uint j,
        size_t          kernel_size,
        QString const & filepath)
{
    BENCHMARK_START;

    _spectralImage->savePixelToFile(i, j, uint(kernel_size), filepath.toStdString());

    BENCHMARK_END("saveSpectrum");
}


void SpectralViewerModel::saveSpectrumAtSelectedPixel(
        QString const & filepath)
{
    saveSpectrum(uint(_selectedPixel.x()),
                 uint(_selectedPixel.y()),
                 kernelSize(),
                 filepath);
}


void SpectralViewerModel::saveSpectrumAtSelectedPixel(
        QString const & filepath,
        size_t          kernel_size
        )
{
    saveSpectrum(uint(_selectedPixel.x()),
                 uint(_selectedPixel.y()),
                 kernel_size,
                 filepath);
}


void SpectralViewerModel::prepareSpectralImage()
{
    BENCHMARK_START;

    emit processingStarted();
    emit loadingProgress(0);
    emit loadingMessage("Preparing spectral image...");

    setWidth(_spectralImage->width());
    setHeight(_spectralImage->height());
    setBands(_spectralImage->wavelengths().size());
    setOpenedFileMeta(_spectralImage->metadata());
    // Commented for now: this is done by image loading functions since not
    // all spectral image format provide a type
    // setSpectrumType(_spectralImage->getSpectrumType());

    //-------------------------------------------------------------------------
    // Memory allocation
    //-------------------------------------------------------------------------

    _grayImage.setZero(height(), width());
    _xyzImageX.setZero(height(), width());
    _xyzImageY.setZero(height(), width());
    _xyzImageZ.setZero(height(), width());

    if (isPolarised()) {
        _s1Image.setZero(height(), width());
        _s2Image.setZero(height(), width());
        _s3Image.setZero(height(), width());
    }

    _linearRGBImage = std::unique_ptr<mrf::image::ColorImage>(new mrf::image::EXRColorImage);
    _linearRGBImage->init(width(), height());
    _falseColorImage = QImage(static_cast<int>(width()), static_cast<int>(height()), QImage::Format_RGB888);
    _tonemappedImage = QImage(static_cast<int>(width()), static_cast<int>(height()), QImage::Format_RGB888);

    //-------------------------------------------------------------------------
    // Spectral -> Gray conversion
    //-------------------------------------------------------------------------

    // set min / max wavelength
    setMinMaxWavelength(_spectralImage->wavelengths()[0], _spectralImage->wavelengths()[_spectralImage->wavelengths().size() - 1]);
    setMinSelectedWavelength(_spectralImage->wavelengths()[0]);
    setMaxSelectedWavelength(_spectralImage->wavelengths()[_spectralImage->wavelengths().size() - 1]);

    recalcGrayImage(false);

    emit loadingProgress(60);

    getGrayImageStatistics(_grayMin, _grayMax, _grayAvg, _grayMed);
    setGrayLuminance(.5 / (_grayMed + _grayAvg));

    updateGrayImage();

    _wavelengthImages.clear();
    _wavelengthImages.resize(bands());

    #pragma omp parallel for schedule(static) default(none)
    for (int i = 0; i < int(_spectralImage->wavelengths().size()); i++) {
        _wavelengthImages[i] = QImage(static_cast<int>(width()), static_cast<int>(height()), QImage::Format_Grayscale8);

        double wl_delta = 0;
        if (i == 0 && _spectralImage->wavelengths().size() > 1) {
            wl_delta = _spectralImage->wavelengths()[1] - _spectralImage->wavelengths()[0];
        } else {
            wl_delta = _spectralImage->wavelengths()[i] - _spectralImage->wavelengths()[i - 1];
        }

        const double mult = _spectralImage->wavelengths().size() * _luminance * wl_delta;

        for (size_t y = 0; y < _spectralImage->height(); y++) {
            for (size_t x = 0; x < _spectralImage->width(); x++) {
                double gray = mult * _spectralImage->data()[i](y, x) ;
                gray = CLAMP(gray, 0., 1.);
                _wavelengthImages[i].scanLine(static_cast<int>(y))[x] = uchar(gray * (double)std::numeric_limits<unsigned char>::max());
            }
        }
    }

    emit loadingProgress(100);

    //-------------------------------------------------------------------------
    // Spectral -> XYZ conversion -> RGB conversion
    //-------------------------------------------------------------------------

    updateXYZImage(true, false);

    //-------------------------------------------------------------------------
    // RGB -> sRGB tonemapped conversion
    //-------------------------------------------------------------------------

    setDefaultTonemappingValues();
    updateTonemappedRGBImage();

    BENCHMARK_END("prepareSpectralImage")
}


void SpectralViewerModel::prepareHDRImage()
{
    BENCHMARK_START;

    //-------------------------------------------------------------------------
    // Memory allocation
    //-------------------------------------------------------------------------

    _xyzImageX.setZero(height(), width());
    _xyzImageY.setZero(height(), width());
    _xyzImageZ.setZero(height(), width());

    _tonemappedImage = QImage(
        static_cast<int>(width()), 
        static_cast<int>(height()), 
        QImage::Format::Format_RGB888);

    //-------------------------------------------------------------------------
    // RGB -> XYZ conversion
    //-------------------------------------------------------------------------

    #pragma omp parallel for schedule(static)
    for (int i = 0; i < int(width() * height()); i++)
    {
        // TODO: future, shall take into account the ICC profile of the opened file
        mrf::color::Color xyz = (*_linearRGBImage)(i).linearRGBtoXYZ();
        _xyzImageX(i) = xyz.x();
        _xyzImageY(i) = xyz.y();
        _xyzImageZ(i) = xyz.z();
    }

    //-------------------------------------------------------------------------
    // RGB -> sRGB tonemapped conversion
    //-------------------------------------------------------------------------

    setDefaultTonemappingValues();
    updateTonemappedRGBImage();

    BENCHMARK_END("prepareHDRImage")
}


void SpectralViewerModel::updateXYZImage(bool updateRGB, bool updateTonemapped)
{
    emit processingStarted();
    emit loadingProgress(0);
    emit loadingMessage(tr("Converting Spectral to XYZ..."));

    BENCHMARK_START;

    float const* illu_spd;
    uint illu_first_wl, illu_precision, illu_size;

    switch (_reflectiveIlluminant) {
    case mrf::radiometry::MRF_ILLUMINANTS::D50:
        illu_spd       = mrf::radiometry::D_50_SPD;
        illu_first_wl  = mrf::radiometry::D_50_FIRST_WAVELENGTH;
        illu_precision = mrf::radiometry::D_50_PRECISION;
        illu_size      = mrf::radiometry::D_50_ARRAY_SIZE;
        break;
    case mrf::radiometry::MRF_ILLUMINANTS::D65:
        illu_spd       = mrf::radiometry::D_65_SPD;
        illu_first_wl  = mrf::radiometry::D_65_FIRST_WAVELENGTH;
        illu_precision = mrf::radiometry::D_65_PRECISION;
        illu_size      = mrf::radiometry::D_65_ARRAY_SIZE;
        break;
    // TODO: this shall be changed in MRF
    case mrf::radiometry::MRF_ILLUMINANTS::NONE: // Illuminant E
        illu_spd       = mrf::radiometry::E_SPD;
        illu_first_wl  = mrf::radiometry::E_FIRST_WAVELENGTH;
        illu_precision = mrf::radiometry::E_PRECISION;
        illu_size      = mrf::radiometry::E_ARRAY_SIZE;
        break;
    default:
        qWarning() << "No suitable reflective illuminant provided. Default to D65.";
        illu_spd = mrf::radiometry::D_65_SPD;
        illu_first_wl = mrf::radiometry::D_65_FIRST_WAVELENGTH;
        illu_precision = mrf::radiometry::D_65_PRECISION;
        illu_size = mrf::radiometry::D_65_ARRAY_SIZE;
        break;
    }

    // TODO: make less hacky + optimized way to do that
    if (auto img = dynamic_cast<mrf::image::PolarisedSpectralImage*>(_spectralImage.get())) {
        _xyzImageX.setZero(height(), width());
        _xyzImageY.setZero(height(), width());
        _xyzImageZ.setZero(height(), width());

        mrf::data_struct::Array2D accX, accY, accZ;

        if (isBispectral()) {
            _converter.bispectralFramebufferToXYZImage(
                        img->reflectiveFramebuffer(),
                        img->reradiation(),
                        img->wavelengths(),
                        illu_spd,
                        illu_first_wl,
                        illu_precision,
                        illu_size,
                        accX, accY, accZ,
                        [this](int p) {
                emit loadingProgress(p);
            });

            _xyzImageX += accX;
            _xyzImageY += accY;
            _xyzImageZ += accZ;
        } else if (isReflective()) {
            _converter.reflectiveFramebufferToXYZImage(
                        img->reflectiveFramebuffer(),
                        img->wavelengths(),
                        illu_spd,
                        illu_first_wl,
                        illu_precision,
                        illu_size,
                        accX, accY, accZ,
                        [this](int p) {
                emit loadingProgress(p);
            });

            _xyzImageX += accX;
            _xyzImageY += accY;
            _xyzImageZ += accZ;
        }

        if (isEmissive()) {
            _converter.emissiveFramebufferToXYZImage(
                        img->emissiveFramebuffer(),
                        img->wavelengths(),
                        accX, accY, accZ,
                        [this](int p) {
                emit loadingProgress(p);
            });

            _xyzImageX += accX;
            _xyzImageY += accY;
            _xyzImageZ += accZ;
        }
    } else {
        if (isReflective()) {
            _converter.reflectiveSpectralImageToXYZImage(
                        *_spectralImage,
                        illu_spd,
                        illu_first_wl,
                        illu_precision,
                        illu_size,
                        _xyzImageX, _xyzImageY, _xyzImageZ,
                        [this](int p) {
                emit loadingProgress(p);
            });
        } else {
            _converter.emissiveSpectralImageToXYZImage(
                        *_spectralImage,
                        _xyzImageX, _xyzImageY, _xyzImageZ,
                        [this](int p) {
                emit loadingProgress(p);
            });
        }
    }

    emit loadingProgress(100);
    emit processingEnded();

    BENCHMARK_END("updateXYZImage")

    if (_isLoaded) { emit xyzUpdated(); }
    if (updateRGB) { updateLinearRGBImage(updateTonemapped); }
}


Color SpectralViewerModel::spectrumToXYZ(
        std::vector<uint>  const & spectrumWavelengths,
        std::vector<float> const & spectrumValues
        ) const
{    
    return _converter.emissiveSpectrumToXYZ(
                spectrumWavelengths,
                spectrumValues);
}


void SpectralViewerModel::updateLinearRGBImage(bool updateTonemapped)
{
    emit processingStarted();
    emit loadingProgress(0);

    BENCHMARK_START;

    #pragma omp parallel for schedule(static) default(none)
    for (int i = 0; i < static_cast<int>(_xyzImageX.size()); i++)
    {
        Color c = Color(_xyzImageX(i),
                        _xyzImageY(i),
                        _xyzImageZ(i));

        c = c.XYZtoLinearRGB(_colorSpace, _whitePoint);

        _linearRGBImage->setPixel(i, c);
    }

    BENCHMARK_END("updateLinearRGBImage");

    emit loadingProgress(100);
    emit processingEnded();

    if (_isLoaded) { emit linearRGBUpdated(); }
    if (updateTonemapped) { updateTonemappedRGBImage();  }
}

void SpectralViewerModel::recalcGrayImage(bool updateGrayImageDisplay)
{
    emit processingStarted();
    emit loadingMessage(tr("Converting Spectral to Gray..."));

    BENCHMARK_START;

    std::vector<uint> const & wavelengths = _spectralImage->wavelengths();

    // Mo wavelength selected: no wavelength defined for this image in
    // [start - end]
    if (wavelengths.empty() ||
        _minSelectedWavelength > wavelengths[wavelengths.size() - 1] ||
        _maxSelectedWavelength < wavelengths[0])
    {
        return;
    }

    if (std::abs(_minSelectedWavelength - _maxSelectedWavelength) < 1) {
        size_t i = 0;

        while (_minSelectedWavelength > wavelengths[i])
        {
            i++;
        }
        if (i > 0) {
            i -= 1;
        }

        float wl_a = wavelengths[i];
        float wl_b = wavelengths[i+1];
        float t = (_minSelectedWavelength - wl_a) / (wl_b - wl_a);
        _grayImage = _spectralImage->data()[i] + (_spectralImage->data()[i+1] - _spectralImage->data()[i]) * t;
    
        // TODO: make it less hacky
        if (isPolarised()) {
            mrf::image::PolarisedSpectralImage* img = (mrf::image::PolarisedSpectralImage*)(_spectralImage.get());

            _s1Image = img->emissiveFramebuffer(1)[i] + (img->emissiveFramebuffer(1)[i+1] - img->emissiveFramebuffer(1)[i]) * t;
            _s2Image = img->emissiveFramebuffer(2)[i] + (img->emissiveFramebuffer(2)[i+1] - img->emissiveFramebuffer(2)[i]) * t;
            _s3Image = img->emissiveFramebuffer(3)[i] + (img->emissiveFramebuffer(3)[i+1] - img->emissiveFramebuffer(3)[i]) * t;
        }
    
    } else {
        mrf::color::SpectrumConverter::spectralBufferToGrayImage(
                    _spectralImage->data(),
                    wavelengths,
                    _grayImage,
                    _minSelectedWavelength, _maxSelectedWavelength);

        // TODO: make it less hacky
        if (isPolarised()) {
            mrf::image::PolarisedSpectralImage* img = (mrf::image::PolarisedSpectralImage*)(_spectralImage.get());

            mrf::color::SpectrumConverter::spectralBufferToGrayImage(
                        img->emissiveFramebuffer(1),
                        wavelengths,
                        _s1Image,
                        _minSelectedWavelength, _maxSelectedWavelength);

            mrf::color::SpectrumConverter::spectralBufferToGrayImage(
                        img->emissiveFramebuffer(2),
                        wavelengths,
                        _s2Image,
                        _minSelectedWavelength, _maxSelectedWavelength);

            mrf::color::SpectrumConverter::spectralBufferToGrayImage(
                        img->emissiveFramebuffer(3),
                        wavelengths,
                        _s3Image,
                        _minSelectedWavelength, _maxSelectedWavelength);
        }
    }

    BENCHMARK_END("recalcGrayImage");

    emit processingEnded();

    if (updateGrayImageDisplay) {
        updateGrayImage();
    }
}


void SpectralViewerModel::updateGrayImage()
{
    emit processingStarted();
    emit loadingMessage(tr("Converting Spectral to Gray..."));

    for (size_t j = 0; j < _grayImage.height(); j++) {
        for (size_t i = 0; i < _grayImage.width(); i++) {
            const float gray = _grayImage(j, i) * float(_luminance);

            // basic polvis
            // TODO: https://cgg.mff.cuni.cz/~wilkie/Website/Home_files/polvis_sccg_2010.pdf
            float polvis[3] = {0};

            if (_stokesStates[1]) {
                const float s1 = std::abs(_s1Image(j, i));
                if (_stokesStates[0]) {
                    polvis[0] = s1 * float(_luminance);
                } else {
                    polvis[0] = s1 / _grayImage(j, i);
                }
            }

            if (_stokesStates[2]) {
                const float s2 = std::abs(_s2Image(j, i));
                if (_stokesStates[0]) {
                    polvis[1] = s2 * float(_luminance);
                } else {
                    polvis[1] = s2 / _grayImage(j, i);
                }
            }

            if (_stokesStates[3]) {
                const float s3 = std::abs(_s3Image(j, i));
                if (_stokesStates[0]) {
                    polvis[2] = s3 * float(_luminance);
                } else {
                    polvis[2] = s3 / _grayImage(j, i);
                }
            }

            for (size_t c = 0; c < 3; c++) {
                float pixelValue;
                
                if (_stokesStates[0]) {
                    pixelValue = gray + polvis[c];
                } else {
                    pixelValue = polvis[c];
                }

                pixelValue = CLAMP(pixelValue, 0.F, 1.F) * std::numeric_limits<unsigned char>::max();
                _falseColorImage.scanLine(int(j))[3*i + c] = uchar(pixelValue);
            }
        }

        //emit loadingProgress(100.f * static_cast<float>(j) / static_cast<float>(_grayImage.height() - 1));
    }

    emit processingEnded();

    if (_isLoaded) { emit falseColorUpdated(); }
}


void SpectralViewerModel::updateTonemappedRGBImage() {
    emit processingStarted();
    emit loadingMessage(tr("Tonemapping..."));

    BENCHMARK_START;

#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
    QColorSpace cs = QColorSpace::SRgb;

    switch(_colorSpace) {
    case mrf::color::MRF_COLOR_SPACE::ADOBE_RGB:
        cs = QColorSpace::AdobeRgb;
        break;
    case mrf::color::MRF_COLOR_SPACE::SRGB:
        cs = QColorSpace::SRgb;
        break;
    default:
        qWarning() << "Unsupported colorspace! Default to sRGB";
        break;
    }

    _tonemappedImage.setColorSpace(cs);
#endif

    switch(exposureAdaptation()) {
    case Standard:
        for (size_t i = 0; i < height(); i++) {
            getColorImageByLineBasicTonemapping(
                        _tonemappedImage.scanLine(int(i)), i);
            emit loadingProgress(int(float(100 * i) / float(height() - 1)));
        }
        break;
    case Reinhard:
        for (size_t i = 0; i < height(); i++) {
            getColorImageByLineReinhardTonemapping(
                        _tonemappedImage.scanLine(int(i)), i);
            emit loadingProgress(int(float(100 * i) / float(height() - 1)));
        }
        break;
    }

    BENCHMARK_END("updateTonemappedRGBImage");

    emit processingEnded();

    if (_isLoaded) { emit tonemappedUpdated(); }
}


void SpectralViewerModel::getColorImageByLineBasicTonemapping(
        uchar * buffer,
        size_t  num_line
        )
{
    assert(num_line < _linearRGBImage->height());

    double grayPointGamma = log(0.5) / log(_grayPoint);

    for (size_t i = 0; i < _linearRGBImage->width(); i++)
    {
        Color color = (*_linearRGBImage)(i, num_line);
        color = color * exp2(_exposure);

        // Apply gray point
        color.pow(float(grayPointGamma));

        color = color.clamped(0.0, 1.0);
        color = mrf::color::getGammaFunction(_colorSpace)(color);

        for (unsigned int c = 0; c < 3; c++) {
            buffer[3*i + c] = uchar(std::numeric_limits<unsigned char>::max() * color[c]);
        }
    }
}


void SpectralViewerModel::getColorImageByLineReinhardTonemapping(
        uchar * buffer,
        size_t  num_line
        )
{
    assert(num_line < _linearRGBImage->height());

    for (size_t i = 0; i < _linearRGBImage->width(); i++)
    {
#ifdef MRF_WITH_EIGEN_SUPPORT
        Eigen::Array4f ce(
                (*_linearRGBImage)(i, num_line)[0],
                (*_linearRGBImage)(i, num_line)[1],
                (*_linearRGBImage)(i, num_line)[2], 1.f);

        ce = reinhard05Tonemapping(
                    ce,
                    float(_chromaticAdaptation),
                    Eigen::Array4f(
                        _linearRGBImage->redStatistics()[2],
                        _linearRGBImage->greenStatistics()[2],
                        _linearRGBImage->blueStatistics()[2], 1.f),
                    _linearRGBImage->lumStatistics()[2],
                    float(_lightAdaptation),
                    float(_contrast),
                    float(_intensity));

        for (int c = 0; c < 3; c++) {
            buffer[3*i + c] = uchar(255 * CLAMP(ce[c], 0.f, 1.f));
        }
#else
        Color color = (*_linearRGBImage)(uint(i), uint(num_line));
        color = color.reinhard05Tonemapping(
                    float(_chromaticAdaptation),
                    Color(
                        _linearRGBImage->redStatistics()[2],
                        _linearRGBImage->greenStatistics()[2],
                        _linearRGBImage->blueStatistics()[2]),
                    _linearRGBImage->lumStatistics()[2],
                    float(_lightAdaptation),
                    float(_contrast),
                    float(_intensity));

        color = color.clamped(0.0, 1.0);
        color = mrf::color::getGammaFunction(_colorSpace)(color);

        for (int c = 0; c < 3; c++) {
            buffer[3*i + c] = uchar(std::numeric_limits<unsigned char>::max() * color[c]);
        }
#endif
    }
}


//-------------------------------------------------------------------------
// File meta
//-------------------------------------------------------------------------

void SpectralViewerModel::setOpenedFilename(const QString &filename)
{
    if (_openedFilename == filename) { return; }
    _openedFilename = filename;

    QFileInfo fi(_openedFilename);
    _openedFileFolder = fi.absolutePath();
    _defaultOpenFolder = _openedFileFolder;

    emit openedFilenameChanged(_openedFilename);
}


void SpectralViewerModel::setOpenedFileMeta(
        const QMap<QString, QString> &meta)
{
    if (_openedFileMetadata == meta) { return; }
    _openedFileMetadata = meta;
    emit openedFileMetaChanged(_openedFileMetadata);
}


void SpectralViewerModel::setOpenedFileMeta(
        const std::map<std::string, std::string> &meta)
{
    QMap<QString, QString> metadata;

    for (auto const& el: meta) {
        metadata[QString::fromStdString(el.first)] = QString::fromStdString(el.second);
    }

    setOpenedFileMeta(metadata);
}


void SpectralViewerModel::setWidth(size_t width)
{
    if (_width == width) { return; }
    _width = width;
    emit widthChanged(static_cast<int>(_width));
}


void SpectralViewerModel::setHeight(size_t height)
{
    if (_height == height) { return; }
    _height = height;
    emit heightChanged(static_cast<int>(_height));
}


void SpectralViewerModel::setBands(size_t bands)
{
    if (_bands == bands) { return; }
    _bands = bands;
    emit bandsChanged(static_cast<int>(_bands));
}

void SpectralViewerModel::setSpectrumType(mrf::radiometry::SpectrumType type)
{
    if (_spectrumType == type) { return; }
    _spectrumType = type;
    emit spectrumTypeChanged(_spectrumType);
    emit emissiveModeChanged(isEmissive());
}


void SpectralViewerModel::setMinMaxWavelength(double wl_min, double wl_max)
{
    if (DBL_NEQ(_minSelectedWavelength, wl_min)) {
        _minWavelength = wl_min;
        emit minWavelengthChanged(_minWavelength);
    }

    if (DBL_NEQ(_maxSelectedWavelength, wl_max)) {
        _maxWavelength = wl_max;
        emit maxWavelengthChanged(_maxWavelength);
    }

    setSelectedWavelengthRange(_minWavelength, _maxWavelength);
}


void SpectralViewerModel::setFileModel(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType type)
{
    if (_model == mode && _spectrumType == type) { return; }
    _model = mode;
    _spectrumType = type;
    emit spectralModeChanged(mode, type);
}


void SpectralViewerModel::resetFileinfo()
{
    _spectralImage->clear();
    _xyzImageX.resize(0, 0);
    _xyzImageY.resize(0, 0);
    _xyzImageZ.resize(0, 0);

    _grayImage.resize(0, 0);
    _s1Image.resize(0, 0);
    _s2Image.resize(0, 0);
    _s3Image.resize(0, 0);
    
    setWidth(0);
    setHeight(0);
    setBands(0);
    setS0Enabled(true);
    setS1Enabled(false);
    setS2Enabled(false);
    setS3Enabled(false);
    QString defaultOpenedFolder = _defaultOpenFolder;
    //setOpenedFilename("No Image");
    _defaultOpenFolder = defaultOpenedFolder;
    setOpenedFileMeta(QMap<QString, QString>());
    setZoomLevel(1.);
    setRotation(0.);
    setFileModel(NoFile, mrf::radiometry::SPECTRUM_NONE);
    setKernelSize(0);
    _isLoaded = false;
    _editableEmissive = false;
}


void SpectralViewerModel::setDefaultTonemappingValues()
{
    // We save the state:
    // setter are going to trigger converstion each time if image is loaded.
    // We call the conversion manually at the end of the function.
    const bool loaded = _isLoaded;
    _isLoaded = false;

    setExposureAdaptationMode(Standard);

    // Reinhard
    setReinhardContrast(1.0);
    setReinhardChromaticAdaptation(DEFAULT_CHROMATIC_ADAPTATION);
    setReinhardLightAdaptation(DEFAULT_LIGHT_ADAPTATION);
    setReinhardIntensity(0.5);

    // Standard
    setExposure(0);
    setGrayPoint(DEFAULT_GRAY_POINT);

    _isLoaded = loaded;

    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::setTonemappingValues(
        double const & chromatic_adaptation,
        double const & light_adaptation,
        double const & contrast,
        double const & intensity
        )
{
    _contrast            = std::max(0., contrast);
    _chromaticAdaptation = std::max(0., chromatic_adaptation);
    _lightAdaptation     = std::max(0., light_adaptation);
    _intensity           = std::max(0., intensity);

    emit reinhardContrastChanged           (_contrast);
    emit reinhardChromaticAdaptationChanged(_chromaticAdaptation);
    emit reinhardLightAdaptationChanged    (_lightAdaptation);
    emit reinhardIntensityChanged          (_intensity);

    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::setTonemappingValues(
        double const & exposure,
        double const & gray_point
        )
{
    _exposure  = exposure;
    _grayPoint = CLAMP(gray_point, 0.01, 0.99);

    emit grayPointChanged(_grayPoint);
    emit exposureChanged (_exposure);

    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::getTonemappingValues(
        double & chromatic_adaptation,
        double & light_adaptation,
        double & contrast,
        double & intensity
        ) const
{
    chromatic_adaptation = _chromaticAdaptation;
    light_adaptation     = _lightAdaptation;
    contrast             = _contrast;
    intensity            = _intensity;
}


//void SpectralViewerModel::rebuildLuminanceHistogram()
//{
//    //find max luminance
//    _maxLuminance = 0.;
//    for (size_t i = 0; i < static_cast<size_t>(_xyzImageY.size()); i++) {
//        _maxLuminance = std::max(float(_maxLuminance), _xyzImageY(i));
//    }

//    //rebuild luminance histogram
//    std::fill(_luminance_histogram.begin(), _luminance_histogram.end(), 0);

//    if (_maxLuminance > 0.f) {
//        for (size_t i = 0; i < static_cast<size_t>(_xyzImageY.size()); i++) {
//            int bin = int((_xyzImageY(i) / _maxLuminance)*(_luminance_histogram.size() - 1));
//            _luminance_histogram[bin]++;
//        }
//    }
//}


void SpectralViewerModel::getColorImageStatistics(
        float & min_luminance,
        float & max_luminance,
        float & average_luminance,
        float & median_value_above_zero
        ) const
{
    min_luminance = std::numeric_limits<float>::max();
    max_luminance = 0;
    average_luminance = 0;
    median_value_above_zero = 0;

    std::vector<float> values;
    values.reserve(_xyzImageY.size());

    for (uint i = 0; i < _xyzImageY.size(); i++) {
        float Y = _xyzImageY(i);
        if (!std::isnan(Y) && !std::isinf(Y) && Y > 0) {
            average_luminance += Y;
            values.push_back(Y);
            min_luminance = std::min(min_luminance, Y);
            max_luminance = std::max(max_luminance, Y);
        }
    }

    if (values.size() > 0) {
        average_luminance /= static_cast<float>(values.size());

        std::sort(values.begin(), values.end());

        if (values.size() % 2 == 0) {
            median_value_above_zero = (values[values.size() / 2 - 1] + values[values.size() / 2]) / 2;
        } else  {
            median_value_above_zero = values[values.size() / 2];
        }
    }
}


void SpectralViewerModel::getGrayImageStatistics(
        float & min_luminance,
        float & max_luminance,
        float & average_luminance,
        float & median_value
        ) const
{
    min_luminance = std::numeric_limits<float>::max();
    max_luminance = 0;
    average_luminance = 0;
    median_value = 0;

    std::vector<float> values;
    values.reserve(_grayImage.size());

    for (uint i = 0; i < _grayImage.size(); i++) {
        float G = _grayImage(i);

        if (!std::isnan(G) && !std::isinf(G) && G > 0) {
            average_luminance += G;
            values.push_back(G);
            min_luminance = std::min(min_luminance, G);
            max_luminance = std::max(max_luminance, G);
        }
    }

    if (values.size() > 0) {
        average_luminance /= static_cast<float>(values.size());
        std::sort(values.begin(), values.end());

        if (values.size() % 2 == 0) {
            median_value = (values[values.size() / 2 - 1] + values[values.size() / 2]) / 2;
        } else  {
            median_value = values[values.size() / 2];
        }
    }
}


void SpectralViewerModel::setSpectrumWavelengths(
        std::vector < mrf::uint> const & wavelengths
        )
{
    _spectralImage->wavelengths() = wavelengths;
}


void SpectralViewerModel::setSpectrumWavelengths(
        std::vector < mrf::uint> && wavelengths
        )
{
    _spectralImage->wavelengths() = wavelengths;
}

void SpectralViewerModel::setDefaultOpenFolder(const QString &f)
{
    _defaultOpenFolder = f;
}


double SpectralViewerModel::memoryOccupancy() const
{
    double size = static_cast<double>(_xyzImageX.size());
    size += _xyzImageY.size();
    size += _xyzImageZ.size();
    size += _grayImage.size();
    size += _linearRGBImage->size() * 3;//3 channel image
    size *= double(sizeof(float)) / (1000.0 * 1000.0);
    size += _spectralImage->memoryOccupancy();
    return  size;
}


double SpectralViewerModel::expectedMemoryOccupancy(
        int image_width,
        int image_height,
        int image_bands
        ) const
{
    //_xyz_image_x, _xyz_image_y, _xyz_image_z
    double size = static_cast<double>(3*image_width*image_height);
    size += image_width*image_height; //_rgb_gray_image.size();
    size += image_width*image_height * 3; // _rgb_image.size() * 3;
    size *= double(sizeof(float)) / (1000.0 * 1000.0);
    size += _spectralImage->expectedMemoryOccupancy(
                image_width, image_height,
                image_bands);
    return  size;
}


void SpectralViewerModel::getSelectedPixelInfo(
        mrf::color::Color  & xyz,
        mrf::color::Color  & rgb,
        mrf::color::Color  & tone_mapped_rgb,
        std::vector<uint>  & wavelengths,
        std::vector<float> & avg_spectrum,
        float              & avg_spectrum_single_value
        ) const
{
    if (!_isLoaded) { return; }

    const int px = selectedPixel().x();
    const int py = selectedPixel().y();
	
	const int min_x = px - static_cast<int>(_kernelSize);
	const int max_x = px + static_cast<int>(_kernelSize);

	const int min_y = py - static_cast<int>(_kernelSize);
	const int max_y = py + static_cast<int>(_kernelSize);
	
    xyz             = Color(0);
    rgb             = Color(0);
    tone_mapped_rgb = Color(0);
    int tonemaped_R = 0;
    int tonemaped_G = 0;
    int tonemaped_B = 0;

    int nb_spectrums_averaged = 0;

    avg_spectrum.clear();
    wavelengths.clear();

    wavelengths = _spectralImage->wavelengths();

    if (   min_x < 0 || max_x >= int(width())
        || min_y < 0 || max_y >= int(height())) {
        return;
    }

    // Average spectrum values
    for (int x = min_x; x <= max_x; x++) {
		assert(x >= 0 && x < int(width()));
		
        for (int y = min_y; y <= max_y; y++) {
            assert(y >= 0 && y < int(height()));

            if (_model == SpectralImage || _model == Spectrum) {
                std::vector<float> temp_spectrum =
                        _spectralImage->getPixel(
                            uint(x),
                            uint(y));

                xyz += spectrumToXYZ(wavelengths, temp_spectrum);

                if (avg_spectrum.empty()) {
                    avg_spectrum = temp_spectrum;
                } else {
                    for (size_t it_spectrum = 0; it_spectrum < avg_spectrum.size(); it_spectrum++) {
                        avg_spectrum[it_spectrum] += temp_spectrum[it_spectrum];
                    }
                }
            }

            rgb += _linearRGBImage->getPixel(
                        uint(x),
                        uint(y));

            const QRgb tonemapped_rgb = _tonemappedImage.pixel(x, y);
            tonemaped_R += qRed(tonemapped_rgb);
            tonemaped_G += qGreen(tonemapped_rgb);
            tonemaped_B += qBlue(tonemapped_rgb);

            nb_spectrums_averaged++;
        }
    }

    tone_mapped_rgb =
            Color(float(tonemaped_R), float(tonemaped_G), float(tonemaped_B))
            / std::numeric_limits<unsigned char>::max();

    // Average color
    tone_mapped_rgb /= nb_spectrums_averaged;
    rgb /= nb_spectrums_averaged;

    if (_model == SpectralImage ||
        _model == Spectrum) {
        xyz /= nb_spectrums_averaged;// float(2 * kernelSize() + 1) * float(2 * kernelSize() + 1);

        //average spectrum values
        for (size_t i = 0; i < avg_spectrum.size(); i++) {
            avg_spectrum[i] /= float(nb_spectrums_averaged);
            avg_spectrum_single_value += avg_spectrum[i];
        }

        avg_spectrum_single_value /= float(avg_spectrum.size());
    }
}


void SpectralViewerModel::downsampleSpectralImage(
        uint downsampling_factor
        )
{
    BENCHMARK_START;

    if (!_isLoaded) { return; }

    if (_spectralImage->data().empty() ||
        _spectralImage->data().size() < downsampling_factor)
    {
        return;
    }

    if ( downsampling_factor < 1 ||
         downsampling_factor > _spectralImage->wavelengths().size())
    {
        return;
    }

    _spectralImage->spectralDownsampling(downsampling_factor);
    prepareSpectralImage();

    BENCHMARK_END("downsampleSpectralImage");

    emit kernelSizeChanged(int(_kernelSize));
}


mrf::image::IMAGE_LOAD_SAVE_FLAGS SpectralViewerModel::refresh()
{
    switch(_openedMode) {
    case SINGLE_FILE   : return openFile(_openedFilename);
    case MULTIPLE_FILES: return openFilesWavelength(_openedFileWavelengths, _startWavelength, _offsetWavelength);
    }

    return mrf::image::MRF_ERROR_WRONG_FILE_PATH;
}


void SpectralViewerModel::setEmissive(bool isEmissive)
{
    // TODO: Make it less hacky
    if (_isLoaded) {
        if (!isEditableEmissive()) { return; }
        if (this->isEmissive() == isEmissive) { return; }
        if (isEmissive) {
            setSpectrumType(mrf::radiometry::SPECTRUM_EMISSIVE);
        } else {
            setSpectrumType(mrf::radiometry::SPECTRUM_REFLECTIVE);
        }
        // Bit inversion
        updateXYZImage(true, false);

        // Auto expose
        setDefaultTonemappingValues();
        updateTonemappedRGBImage();
    }

    emit emissiveModeChanged(this->isEmissive());
}


void SpectralViewerModel::setReflectiveIlluminant(mrf::radiometry::MRF_ILLUMINANTS illuminant)
{
    if (_reflectiveIlluminant == illuminant) { return; }

    _reflectiveIlluminant = illuminant;
    if (_isLoaded) { updateXYZImage(true, true); }

    emit reflectiveIlluminantChanged(_reflectiveIlluminant);

}


void SpectralViewerModel::setSensitivityCurve(mrf::color::SensitivityCurveName curve)
{
    if (curve == _sensitivityCurve) { return; }

    _sensitivityCurve = curve;
    _converter = mrf::color::SpectrumConverter(_sensitivityCurve);

    if (_isLoaded) { updateXYZImage(true, true); }

    emit sensitivityCurveChanged(curve);
}


void SpectralViewerModel::setWhitePoint(
        mrf::color::MRF_WHITE_POINT whitepoint
        )
{
    if (_whitePoint == whitepoint) { return; }

    switch (model()) {
    case SpectralViewerModel::SpectralImage:
    case SpectralViewerModel::HDRImage:
        _whitePoint = whitepoint;
        emit whitePointChanged(whitepoint);
        if (_isLoaded) { updateLinearRGBImage(); }
        break;

    default:
        break;
    }
}


void SpectralViewerModel::setColorSpace(
        mrf::color::MRF_COLOR_SPACE color_space
        )
{
    if (_colorSpace == color_space) { return; }

    switch (model()) {
    case SpectralViewerModel::SpectralImage:
    case SpectralViewerModel::HDRImage:
        _colorSpace = color_space;
        emit colorSpaceChanged(_colorSpace);
        if (_isLoaded) { updateLinearRGBImage(); }
        break;

    default:
        break;
    }
}


void SpectralViewerModel::setExposureAdaptationMode(
        SpectralViewerModel::ExposureAdaptation mode
        )
{
    if (_exposureAdaptation == mode) { return; }
    _exposureAdaptation = mode;
    emit exposureAdaptationModeChanged(_exposureAdaptation);
    if (_isLoaded) { updateTonemappedRGBImage(); }
}

void SpectralViewerModel::setAutoExposure()
{
    getColorImageStatistics(_yMin, _yMax, _yAvg, _yMed);

    _linearRGBImage->computeStatistics();
    const float lmin = std::log(std::min(1e-7F, _yMin));
    const float lmax = std::log(std::max(1e-7F, _yMax));
    const float k    = (lmax - std::log(std::max(1e-7F, _yAvg))) / (lmax - lmin);
    const float m0   = 0.3F + 0.7F * std::pow(k, 1.4F);

    // We save the state:
    // setter are going to trigger converstion each time if image is loaded.
    // We call the conversion manually at the end of the function.
    const bool loaded = _isLoaded;
    _isLoaded = false;

    // Reinhard
    setReinhardContrast(m0);
    setReinhardChromaticAdaptation(DEFAULT_CHROMATIC_ADAPTATION);
    setReinhardLightAdaptation(DEFAULT_LIGHT_ADAPTATION);
    setReinhardIntensity(0.35 * 1. / double(_yAvg));

    // Standard
    setExposure(std::log2(.5/(_yMed + _yAvg)));
    setGrayPoint(DEFAULT_GRAY_POINT);

    _isLoaded = loaded;

    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::setExposure(double value)
{
    if (std::isinf(value) || std::isnan(value)) {
        value = 1.;
    }

    if (DBL_EQ(_exposure, value)) { return; }
    _exposure = value;
    emit exposureChanged(_exposure);
    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::setGrayPoint(double value)
{
    if (std::isinf(value) || std::isnan(value)) {
        value = 1.;
    }

    if (DBL_EQ(_grayPoint, value)) { return; }
    _grayPoint = CLAMP(value, 0.01, 0.99);
    emit grayPointChanged(_grayPoint);
    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::setReinhardChromaticAdaptation(double value)
{
    if (std::isinf(value) || std::isnan(value)) {
        value = 1.;
    }

    if (DBL_EQ(_chromaticAdaptation, value)) { return; }
    _chromaticAdaptation = std::max(0., value);
    emit reinhardChromaticAdaptationChanged(_chromaticAdaptation);
    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::setReinhardLightAdaptation(double value)
{
    if (std::isinf(value) || std::isnan(value)) {
        value = 1.;
    }

    if (DBL_EQ(_lightAdaptation, value)) { return; }
    _lightAdaptation = std::max(0., value);
    emit reinhardLightAdaptationChanged(_lightAdaptation);
    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::setReinhardContrast(double value)
{   
    if (std::isinf(value) || std::isnan(value)) {
        value = 1.;
    }

    if (DBL_EQ(_contrast, value)) { return; }
    _contrast = std::max(0., value);
    emit reinhardContrastChanged(_contrast);
    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::setReinhardIntensity(double value)
{   
    if (std::isinf(value) || std::isnan(value)) {
        value = 1.;
    }

    if (DBL_EQ(_intensity, value)) { return; }
    _intensity = std::max(0., value);
    emit reinhardIntensityChanged(_intensity);
    if (_isLoaded) { updateTonemappedRGBImage(); }
}


void SpectralViewerModel::setGrayLuminance(double value)
{
    if (std::isinf(value) || std::isnan(value)) {
        value = 1.;
    }

    if (DBL_EQ(_luminance, value)) { return; }
    _luminance = std::max(0., value);
    emit grayLuminanceChanged(_luminance);
    if (_isLoaded) { recalcGrayImage(); }
}


void SpectralViewerModel::setS0Enabled(bool enable)
{
    if (_stokesStates[0] == enable) { return; }

    _stokesStates[0] = enable;
    emit s0StateChanged(_stokesStates[0]);
    if (_isLoaded) { recalcGrayImage(); }
}


void SpectralViewerModel::setS1Enabled(bool enable)
{
    if (_stokesStates[1] == enable || !mrf::radiometry::isPolarised(spectrumType())) { return; }

    _stokesStates[1] = enable;
    emit s1StateChanged(_stokesStates[1]);
    if (_isLoaded) { recalcGrayImage(); }
}


void SpectralViewerModel::setS2Enabled(bool enable)
{
    if (_stokesStates[2] == enable || !mrf::radiometry::isPolarised(spectrumType())) { return; }

    _stokesStates[2] = enable;
    emit s2StateChanged(_stokesStates[2]);
    if (_isLoaded) { recalcGrayImage(); }
}


void SpectralViewerModel::setS3Enabled(bool enable)
{
    if (_stokesStates[3] == enable || !mrf::radiometry::isPolarised(spectrumType())) { return; }

    _stokesStates[3] = enable;
    emit s3StateChanged(_stokesStates[3]);
    if (_isLoaded) { recalcGrayImage(); }
}


void SpectralViewerModel::setSelectedPixel(const QPointF &p)
{
    if (int(p.x()) == _selectedPixel.x() && int(p.y()) == _selectedPixel.y()) { return; }

    // Ensure pixel is within the image
    const int px = CLAMP(int(p.x()), int(kernelSize()), int(width()  - kernelSize()) - 1);
    const int py = CLAMP(int(p.y()), int(kernelSize()), int(height() - kernelSize()) - 1);

    _selectedPixel = QPoint(px, py);

    emit selectedPixelChanged(_selectedPixel);
}


void SpectralViewerModel::setZoomLevel(double value)
{
    if (std::isinf(value) || std::isnan(value)) {
        value = 1.;
    }

    if (DBL_EQ(_zoomLevel, value)) { return; }
    _zoomLevel = std::max(0.01, value);
    emit zoomLevelChanged(_zoomLevel);
}


void SpectralViewerModel::setRotation(double value)
{
    if (std::isinf(value) || std::isnan(value)) {
        value = 0.;
    }

    if (DBL_EQ(_rotation, value)) { return; }
    emit rotationDeltaChanged(value - _rotation);
    _rotation = value;
    emit rotationChanged(_rotation);
}


void SpectralViewerModel::rotateLeft()
{
    setRotation(_rotation - 90.);
}


void SpectralViewerModel::rotateRight()
{
    setRotation(_rotation + 90.);
}


void SpectralViewerModel::setMinSelectedWavelength(double wl)
{
    setSelectedWavelengthRange(wl, _maxSelectedWavelength);
}


void SpectralViewerModel::setMaxSelectedWavelength(double wl)
{
    setSelectedWavelengthRange(_minSelectedWavelength, wl);
}


void SpectralViewerModel::setSelectedWavelengthRange(
        double wl_min,
        double wl_max
        )
{
    if (DBL_EQ(_minSelectedWavelength, wl_min) &&
        DBL_EQ(_maxSelectedWavelength, wl_max)) {
        return;
    }

    if (DBL_NEQ(_minSelectedWavelength, wl_min)) {
        _minSelectedWavelength = CLAMP(wl_min, _minWavelength, _maxWavelength);

        if (_minSelectedWavelength > _maxSelectedWavelength) {
            _minSelectedWavelength = _maxSelectedWavelength;
        }

        emit minSelectedWavelengthChanged(_minSelectedWavelength);
    }

    if (DBL_NEQ(_maxSelectedWavelength, wl_max)) {
        _maxSelectedWavelength = CLAMP(wl_max, _minWavelength, _maxWavelength);

        if (_maxSelectedWavelength < _minSelectedWavelength) {
            _maxSelectedWavelength = _minSelectedWavelength;
        }

        emit maxSelectedWavelengthChanged(_maxSelectedWavelength);
    }

    if (_isLoaded) { recalcGrayImage(); }
}


void SpectralViewerModel::setKernelSize(int size) {
    if (!_isLoaded) { return; }
    if (size < 0 || _kernelSize == static_cast<size_t>(size)) { return; }
    _kernelSize = size;
    emit kernelSizeChanged(size);
}

