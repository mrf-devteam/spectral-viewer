/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <QApplication>
#include <QSharedPointer>
#include <QCommandLineParser>
#include <QString>

#include "mainwindow.h"
#include "spectral_viewer_model.h"
#include "console_logger.h"

enum CommandLineParseResult
{
    CommandLineOk,
    CommandLineError,
    CommandLineVersionRequested,
    CommandLineHelpRequested,
    CommandLineLicenseRequested
};

struct Command {
    // Use the program in CLI rather than GUI mode
    bool isNoGUI = false;
    bool hasInputFile = false;

    QString inputFile;
    QString outputFile;

    // Set the exposure adaptation mode
    SpectralViewerModel::ExposureAdaptation exposureAdaptation = SpectralViewerModel::Standard;

    // Standard tonemapping
    double exposure  = SpectralViewerModel::DEFAULT_EXPOSURE;
    double grayPoint = SpectralViewerModel::DEFAULT_GRAY_POINT;

    // Reinhard tonemapping
    double chromaticAdaptation = SpectralViewerModel::DEFAULT_CHROMATIC_ADAPTATION;
    double lightAdaptation     = SpectralViewerModel::DEFAULT_LIGHT_ADAPTATION;
    double intensity           = SpectralViewerModel::DEFAULT_CONTRAST;
    double contrast            = SpectralViewerModel::DEFAULT_INTENSITY;

    // Set dimensions of window
    int width = 0;
    int height = 0;
    bool maximized = true;
};

SpectralViewerModel::ExposureAdaptation typeFromParameter(QString const& type) {
    if (type == "Reinhard") {
        return SpectralViewerModel::Reinhard;
    }

    return SpectralViewerModel::Standard;
}

CommandLineParseResult parseCommandLine(QCommandLineParser &parser, Command *result, QString *errorMessage) {
    parser.setApplicationDescription(
"Spectral Viewer " + QApplication::applicationVersion() + "\n\n\
Application for displaying spectral and tristimulus images.\n\
\n\
Copyright (C) CNRS, INRIA 2017 - 2021.\n\
This program comes with ABSOLUTELY NO WARRANTY; for details type `-licence'.\n\
This is free software, and you are welcome to redistribute it\n\
under certain conditions.\n"
);

    parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);

    const QCommandLineOption helpOption      (parser.addHelpOption());
    const QCommandLineOption versionOption   (parser.addVersionOption());
    const QCommandLineOption licenseOption   ("license");

    const QCommandLineOption outputFileOption("o", "Sets the output file.", "output image");

    // Standard tonemapping options
    const QCommandLineOption grayPointOption("gray_point", "Sets gray point value.", "value");
    const QCommandLineOption exposureOption ("exposure"  , "Sets exposure value."  , "value");

    // Reinhard tonemapping options
    const QCommandLineOption chromaticAdaptationOption ("chromatic_adaptation", "Sets Reinhard chromatic adaptation. If specified, Reinhard tomenamping is used.", "value");
    const QCommandLineOption lightAdaptationOption     ("light_adaptation"    , "Sets Reinhard light adapatation. If specified, Reinhard tomenamping is used."   , "value");
    const QCommandLineOption intensityOption           ("intensity"           , "Sets Reihnard intensity. If specified, Reinhard tomenamping is used."           , "value");
    const QCommandLineOption contrastOption            ("contrast"            , "Sets Reinhard contrast. If specified, Reinhard tomenamping is used."            , "value");

    const QCommandLineOption widthOption ("ww", "Sets window width in GUI mode." , "value");
    const QCommandLineOption heightOption("wh", "Sets window height in GUI mode.", "value");

    parser.addOption(licenseOption);
    parser.addPositionalArgument("input file", "Sets file to process.");
    parser.addOption(outputFileOption);

    parser.addOption(grayPointOption);
    parser.addOption(exposureOption);

    parser.addOption(chromaticAdaptationOption);
    parser.addOption(lightAdaptationOption);
    parser.addOption(intensityOption);
    parser.addOption(contrastOption);

    parser.addOption(widthOption);
    parser.addOption(heightOption);

    // Basic handling, parsing error & version & help
    if (!parser.parse(QCoreApplication::arguments())) {
        *errorMessage = parser.errorText();
        return CommandLineError;
    }

    if (parser.isSet(versionOption)) {
        return CommandLineVersionRequested;
    }

    if (parser.isSet(helpOption)) {
        return CommandLineHelpRequested;
    }

    if (parser.isSet(licenseOption)) {
        return CommandLineLicenseRequested;
    }


    // Input file
    const QStringList positionalArguments = parser.positionalArguments();

    if (positionalArguments.isEmpty()) {
        return CommandLineOk;
    }

    if (positionalArguments.size() > 1) {
        *errorMessage = "Several 'input file' arguments specified.";
        return CommandLineError;
    } else if (positionalArguments.size() == 1 && !positionalArguments.first().isEmpty()) {
        result->inputFile = positionalArguments.first();
        QFileInfo info(result->inputFile);
        result->hasInputFile = info.isFile();
    } else {
        result->hasInputFile = false;
    }

    // Output file
    if (parser.isSet(outputFileOption)) {
        result->outputFile = parser.value(outputFileOption);

        if (result->outputFile.isNull()) {
            *errorMessage = "Bad filename: " + result->outputFile;
            return CommandLineError;
        }

        result->isNoGUI = true;
    }

    //-------------------------------------------------------------------------
    // Standard Tonemapping
    //-------------------------------------------------------------------------

    // Tonemapping options
    if (parser.isSet(grayPointOption)) {
        bool parseDoubleOk = false;
        result->grayPoint = parser.value(grayPointOption).toDouble(&parseDoubleOk);

        if (!parseDoubleOk) {
            *errorMessage = "Incorrect option for gamma value: " + parser.value(grayPointOption);
            return CommandLineError;
        }

        result->isNoGUI = true;
    }

    // Exposure options
    if (parser.isSet(exposureOption)) {
        bool parseDoubleOk = false;
        result->exposure = parser.value(exposureOption).toDouble(&parseDoubleOk);

        if (!parseDoubleOk) {
            *errorMessage = "Incorrect option for exposure value: " + parser.value(exposureOption);
            return CommandLineError;
        }

        result->isNoGUI = true;
    }


    //-------------------------------------------------------------------------
    // Reinhard Tonemapping
    //-------------------------------------------------------------------------
    struct ReinhardCommandParams
    {
        const QCommandLineOption& cmd;
        double * v;
    };

    std::array< ReinhardCommandParams, 4> a {{
    { chromaticAdaptationOption, &(result->chromaticAdaptation) },
    { lightAdaptationOption    , &(result->lightAdaptation) },
    { intensityOption          , &(result->intensity) },
    { contrastOption           , &(result->contrast) }
    }};

    for (const auto & c : a) {
        if (parser.isSet(c.cmd)) {
            bool parseDoubleOk = false;
            *(c.v) = parser.value(c.cmd).toDouble(&parseDoubleOk);

            if (!parseDoubleOk) {
                *errorMessage =
                        "Incorrect option for Reinhard parameter " +
                        c.cmd.valueName() + " " +
                        parser.value(c.cmd);
                return CommandLineError;
            }

            result->exposureAdaptation = SpectralViewerModel::Reinhard;
            result->isNoGUI = true;
        }
    }

    if (parser.isSet(widthOption)) {
        bool parseIntOK = false;
        result->width = parser.value(widthOption).toInt(&parseIntOK);

        if (!parseIntOK) {
            *errorMessage =
                    "Incorrect option for width parameter " +
                    widthOption.valueName() + " " + parser.value(widthOption);
            return CommandLineError;
        }

        result->maximized = false;
    }

    if (parser.isSet(heightOption)) {
        bool parseIntOK = false;
        result->height = parser.value(heightOption).toInt(&parseIntOK);

        if (!parseIntOK) {
            *errorMessage =
                    "Incorrect option for height parameter " +
                    widthOption.valueName() + " " + parser.value(heightOption);
            return CommandLineError;
        }

        result->maximized = false;
    }

    return CommandLineOk;
}


#ifdef WIN32
#ifdef WIN32NOCONSOLE

#include <Windows.h>

int CALLBACK WinMain(
        _In_ HINSTANCE /*hInstance*/,
        _In_ HINSTANCE /*hPrevInstance*/,
        _In_ LPSTR     /*lpCmdLine*/,
        _In_ int       /*nCmdShow*/
        )
{
    int         argc = 0;

    char**      argv = nullptr;
    argc = __argc;

    argv = __argv;

#else
int main(int argc, char *argv[])
{
#endif
#else
int main(int argc, char *argv[])
{
#endif
    
    QApplication a(argc, argv);

    // Get application version
    QFile version_file(":version.txt");
    QString version_str;

    if (version_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&version_file);
        version_str = in.readAll();
    } else {
        version_str = "N.A.";
    }

    version_str.remove("\n");
    QApplication::setApplicationVersion(version_str);

    QCommandLineParser parser;

    Command command;
    QString errorMessage;

    const CommandLineParseResult commandLineParseResult =
            parseCommandLine(parser, &command, &errorMessage);

    switch (commandLineParseResult) {
    case CommandLineVersionRequested:
        parser.showVersion();
        break;
    case CommandLineHelpRequested:
        parser.showHelp();
        break;
    case CommandLineError:
        std::cerr << errorMessage.toStdString() << std::endl;
        exit(-1);
    case CommandLineOk:
        break;
    case CommandLineLicenseRequested:
        QFile license_file(":LICENSE.txt");
        QString licenseStr;
        if (license_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QTextStream in(&license_file);
            licenseStr = in.readAll();
        } else {
            licenseStr = "N.A.";
        }
        std::cout << licenseStr.toStdString() << std::endl;
        return 0;
    }

    QSharedPointer<SpectralViewerModel> model(new SpectralViewerModel);

    if (command.isNoGUI) {
        ConsoleLogger l(model);

        l.showMessage("Loading: " + command.inputFile);

        model->openFile(command.inputFile);

        // Configure exposure adaptation mode
        model->setExposureAdaptationMode(command.exposureAdaptation);

        switch (command.exposureAdaptation) {
        case SpectralViewerModel::Standard:
            model->setExposure(command.exposure);
            model->setGrayPoint(command.grayPoint);
            break;
        case SpectralViewerModel::Reinhard:
            model->setReinhardChromaticAdaptation(command.chromaticAdaptation);
            model->setReinhardLightAdaptation    (command.lightAdaptation);
            model->setReinhardIntensity          (command.intensity);
            model->setReinhardContrast           (command.contrast);
            break;
        }

        // Some formats does not handle tonemapping
        // alert the user in such cases
        if (command.outputFile.endsWith(".exr", Qt::CaseInsensitive)
         || command.outputFile.endsWith(".hdr", Qt::CaseInsensitive)
         || command.outputFile.endsWith(".pfm", Qt::CaseInsensitive)
        #ifdef HAS_TIFF
         || command.outputFile.endsWith(".tif", Qt::CaseInsensitive)
         || command.outputFile.endsWith(".tiff", Qt::CaseInsensitive)
        #endif
        ) {
            if (command.exposureAdaptation == SpectralViewerModel::Standard) {
                if (command.grayPoint != 0.5f || command.exposure != 0.f) {
                    l.showMessage("Warning: This output format does not support exposure and graypoint compensation. They will be ignored");
                }
            } else {
                l.showMessage("Warning: This output format does not support tonemapping operator. It will be ignored");
            }
        }

        bool saved_success = model->saveColorImage(command.outputFile);

        if (saved_success) {
            l.showMessage("\nSaved as: " + command.outputFile);
        } else {
            l.showMessage("\nError saving file: " + command.outputFile);
            l.showMessage("\n Check if the format extension is supported and if you have write access");
        }

        return 0;
    }

    // Set theme
    QFile f(":/theme.qss");

    if (!f.exists()) {
        qWarning() << "Unable to set stylesheet, file not found";
    } else {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        a.setStyleSheet(ts.readAll());
    }

    MainWindow w(model);

    if (command.maximized) {
        w.showMaximized();
    } else {
        w.show();
        w.setGeometry(0, 0, command.width, command.height);
    }

    QStringList arguments = QApplication::arguments();

    if (command.hasInputFile) {
        QFileInfo info(command.inputFile);

        if (info.isFile()) {
            w.openFile(command.inputFile);
        } else if (info.isDir()) {
            std::cerr << "Opening folder not supported yet." << std::endl;
            //w.openFolder(command.inputFile);
        } else {
            std::cerr << "Invalid file path." << std::endl;
        }
    }


    return QApplication::exec();
}
