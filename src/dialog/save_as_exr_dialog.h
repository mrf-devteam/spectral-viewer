/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *  - Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QDialog>
#include <array>

namespace Ui
{
class SaveAsExrDialog;
}

class SaveAsExrDialog : public QDialog
{
  Q_OBJECT

 public:
  explicit SaveAsExrDialog(QWidget *parent, QString const & default_dir, const std::array<uint, 3>& wavelengths);
  ~SaveAsExrDialog();

  QString filePath();

  uint wavelengthR()
  {
    return _wavelengths[0];
  }
  uint wavelengthG()
  {
    return _wavelengths[1];
  }
  uint wavelengthB()
  {
    return _wavelengths[2];
  }
  bool exportColorImage()
  {
    return _export_color_image;
  }

 public slots:
  void showSaveFileDialog();
  void setExportColorImage(bool);
  void setWavelengthR(int val);
  void setWavelengthG(int val);
  void setWavelengthB(int val);

 private:
  Ui::SaveAsExrDialog *ui;
  //QString _file_path;
  QString _default_dir;
  std::array<uint, 3> _wavelengths;
  bool _export_color_image;
};
