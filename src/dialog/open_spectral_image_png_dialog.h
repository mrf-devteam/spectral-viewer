/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *  - Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QDialog>

namespace Ui
{
class OpenSpectralImagePngDialog;
}

class OpenSpectralImagePngDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OpenSpectralImagePngDialog(QWidget *parent = 0, QString const & default_dir="");
    ~OpenSpectralImagePngDialog();
    uint startWavelength();
    uint offsetWavelength();
    QString folderPath();

public slots:
    void showOpenFolderDialog();
    void caveDatabasePreset();
    void regular300780Preset();

    void updateFileNumber();
    void updateEndWavelength();


    void setStartWavelength(int start);
    void setOffsetWavelength(int offset);

private:
    Ui::OpenSpectralImagePngDialog *ui;
    QString _folderPath;
    uint _startWavelength;
    uint _offsetWavelength;

    static const size_t CAVE_START_WAVELENGTH = 400;
    static const size_t CAVE_SAMPLING_WAVELENGTH = 10;
    static const size_t REGULAR_300780_START_WAVELENGTH = 300;
    static const size_t REGULAR_300780_SAMPLING_WAVELENGTH = 1;
};
