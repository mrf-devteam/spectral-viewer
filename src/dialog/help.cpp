/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "help.h"
#include "ui_help.h"

#include <QFile>
#include <QTextStream>

Help::Help(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Help)
{
    ui->setupUi(this);

    QFile readme(":Help.html");

    if (!readme.open(QIODevice::ReadOnly | QIODevice::Text)) {
        ui->_text_browser->setText("Cannot open Help.html\n");
        return;
    }

    QTextStream readmeStream(&readme);
    ui->_text_browser->setHtml(readmeStream.readAll());

    QFontMetrics font_metrics(ui->_text_browser->font());

    const size_t number_of_lines = 50;
    const int height = static_cast<int>(font_metrics.height() * number_of_lines + 20);
    const int width  =
        #if QT_VERSION < QT_VERSION_CHECK(5,11,0)
            font_metrics.width("a") * 80 + 20;
        #else
            font_metrics.horizontalAdvance("a") * 80 + 20;
        #endif

    resize(width, height);
}

Help::~Help()
{
    delete ui;
}
