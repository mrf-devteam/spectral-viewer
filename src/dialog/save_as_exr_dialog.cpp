/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "save_as_exr_dialog.h"
#include "ui_save_as_exr_dialog.h"

#include <QFileDialog>

SaveAsExrDialog::SaveAsExrDialog(
        QWidget *parent,
        QString const & default_dir,
        const std::array<uint, 3> &wavelengths)
  : QDialog(parent)
  , ui(new Ui::SaveAsExrDialog)
  , _wavelengths(wavelengths)
{
  ui->setupUi(this);
  setWindowTitle("Save as exr");
  ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Export");

  _default_dir = default_dir;
  ui->_line_edit_folder_path->setText(_default_dir);

  _export_color_image = true;

  ui->_spin_box_wavelength_1->setValue(_wavelengths[0]);
  ui->_spin_box_wavelength_2->setValue(_wavelengths[1]);
  ui->_spin_box_wavelength_3->setValue(_wavelengths[2]);

  QObject::connect(ui->_pb_open_folder, SIGNAL(clicked()), this, SLOT(showSaveFileDialog()));

  QObject::connect(ui->radio_button_spectral, SIGNAL(toggled(bool)), ui->_spin_box_wavelength_1, SLOT(setEnabled(bool)));
  QObject::connect(ui->radio_button_spectral, SIGNAL(toggled(bool)), ui->_spin_box_wavelength_2, SLOT(setEnabled(bool)));
  QObject::connect(ui->radio_button_spectral, SIGNAL(toggled(bool)), ui->_spin_box_wavelength_3, SLOT(setEnabled(bool)));
  QObject::connect(ui->radio_button_spectral, SIGNAL(toggled(bool)), ui->group_box_spectral_values, SLOT(setEnabled(bool)));

  QObject::connect(ui->radio_button_color, SIGNAL(toggled(bool)), this, SLOT(setExportColorImage(bool)));

  QObject::connect(ui->_spin_box_wavelength_1, SIGNAL(valueChanged(int)), this, SLOT(setWavelengthR(int)));
  QObject::connect(ui->_spin_box_wavelength_2, SIGNAL(valueChanged(int)), this, SLOT(setWavelengthG(int)));
  QObject::connect(ui->_spin_box_wavelength_3, SIGNAL(valueChanged(int)), this, SLOT(setWavelengthB(int)));
}

SaveAsExrDialog::~SaveAsExrDialog()
{
  delete ui;
}

QString SaveAsExrDialog::filePath()
{
  //return _file_path;
  return ui->_line_edit_folder_path->text();
}

void SaveAsExrDialog::showSaveFileDialog()
{
  auto file_path = QFileDialog::getSaveFileName(this, tr("Export as exr"), _default_dir, tr("Exr Images (*.exr)"));
  ui->_line_edit_folder_path->setText(file_path);
}

void SaveAsExrDialog::setExportColorImage(bool val)
{
  _export_color_image = val;
}

void SaveAsExrDialog::setWavelengthR(int val)
{
  _wavelengths[0] = val;
}

void SaveAsExrDialog::setWavelengthG(int val)
{
  _wavelengths[1] = val;
}

void SaveAsExrDialog::setWavelengthB(int val)
{
  _wavelengths[2] = val;
}
