/*
 *
 * author : Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once
#include <QDialog>

namespace Ui {
class WelcomeDialog;
}

class WelcomeDialog : public QDialog
{
    Q_OBJECT

public:
    enum Mode {
        SINGLE_FILE,
        FOLDER
    };

    explicit WelcomeDialog(QWidget *parent = nullptr);
    ~WelcomeDialog();

    QString filepath() const;
    Mode mode() const;

private slots:

//    void on_pushButtonOpenFile_clicked();

//    void on_pushButtonOpenFolder_clicked();

//    void on_pushButtonExit_clicked();

private:
    Ui::WelcomeDialog *ui;

    QString _workingDirectory;
    QString _filepath;
    Mode _mode;
};
