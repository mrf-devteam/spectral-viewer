/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "export_spectrum_dialog.h"
#include "ui_export_spectrum_dialog.h"

#include <QFileDialog>

ExportSpectrumDialog::ExportSpectrumDialog(QWidget *parent,QString const & default_dir)
    : QDialog(parent)
    , ui(new Ui::ExportSpectrumDialog)
    , _default_dir(default_dir)
    , _export_kernel(false)
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setText(tr("Export"));

    ui->line_edit_file_path->setText(_default_dir);
    ui->radio_single_pixel ->setChecked(!_export_kernel);
    ui->radio_kernel       ->setChecked(_export_kernel);
}

ExportSpectrumDialog::~ExportSpectrumDialog()
{
    delete ui;
}

QString ExportSpectrumDialog::filePath()
{
    return ui->line_edit_file_path->text();
}

bool ExportSpectrumDialog::exportKernel() const
{
    return _export_kernel;
}

void ExportSpectrumDialog::on_open_folder_clicked()
{
    auto file_path =
            QFileDialog::getSaveFileName(
                this,
                tr("Export spectrum"),
                _default_dir,
                tr("Text files (*.txt *.spd)"));

    ui->line_edit_file_path->setText(file_path);
}

void ExportSpectrumDialog::on_radio_single_pixel_toggled(bool checked)
{
    _export_kernel = checked;
}
