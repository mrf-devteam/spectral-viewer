/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "open_spectral_image_png_dialog.h"
#include "ui_open_spectral_image_png_dialog.h"

#include <QFileDialog>

OpenSpectralImagePngDialog::OpenSpectralImagePngDialog(
        QWidget *parent,
        QString const & default_dir)
    : QDialog(parent)
    , ui(new Ui::OpenSpectralImagePngDialog)
    , _folderPath(default_dir)
    , _startWavelength(CAVE_START_WAVELENGTH)
    , _offsetWavelength(CAVE_SAMPLING_WAVELENGTH)

{
    ui->setupUi(this);

    ui->folderPath->setText(_folderPath);
    ui->startWavelength->setValue(_startWavelength);
    ui->offsetWavelength->setValue(_offsetWavelength);
    updateFileNumber();

    QObject::connect(ui->openFolder       , SIGNAL(clicked()),
                     this                 , SLOT(showOpenFolderDialog()));

    QObject::connect(ui->_pb_cave_database, SIGNAL(clicked()),
                     this                 , SLOT(caveDatabasePreset()));

    QObject::connect(ui->_pb_300_780      , SIGNAL(clicked()),
                     this                 , SLOT(regular300780Preset()));

    QObject::connect(ui->nbImages         , SIGNAL(valueChanged(int)),
                     this                 , SLOT(updateEndWavelength()));

    QObject::connect(ui->startWavelength  , SIGNAL(valueChanged(int)),
                     this                 , SLOT(setStartWavelength(int)));

    QObject::connect(ui->offsetWavelength , SIGNAL(valueChanged(int)),
                     this                 , SLOT(setOffsetWavelength(int)));
}

OpenSpectralImagePngDialog::~OpenSpectralImagePngDialog()
{
    delete ui;
}

uint OpenSpectralImagePngDialog::startWavelength()
{
    return _startWavelength;
}

uint OpenSpectralImagePngDialog::offsetWavelength()
{
    return _offsetWavelength;
}

QString OpenSpectralImagePngDialog::folderPath()
{
    return _folderPath;
}

void OpenSpectralImagePngDialog::showOpenFolderDialog()
{
    _folderPath = QFileDialog::getExistingDirectory(
                this, tr("Open Directory"),
                _folderPath,
                QFileDialog::ShowDirsOnly
                | QFileDialog::DontResolveSymlinks);

    ui->folderPath->setText(_folderPath);

    updateFileNumber();
}

void OpenSpectralImagePngDialog::updateFileNumber()
{
    // Tries to open directory and count png images
    QDir dir(_folderPath);

    if (dir.exists()) {
        QStringList filters;
        filters << "*.png";
        dir.setNameFilters(filters);
        ui->nbImages->setValue(dir.entryList().size());
    } else {
        ui->nbImages->setValue(0);
    }

    updateEndWavelength();
}

void OpenSpectralImagePngDialog::caveDatabasePreset()
{
    ui->startWavelength->setValue(CAVE_START_WAVELENGTH);
    ui->offsetWavelength->setValue(CAVE_SAMPLING_WAVELENGTH);
    updateEndWavelength();
}

void OpenSpectralImagePngDialog::regular300780Preset()
{
    ui->startWavelength->setValue(REGULAR_300780_START_WAVELENGTH);
    ui->offsetWavelength->setValue(REGULAR_300780_SAMPLING_WAVELENGTH);
    updateEndWavelength();
}

void OpenSpectralImagePngDialog::updateEndWavelength()
{
    int nb_png_files = ui->nbImages->value();
    int end_wavelength = 0;

    if(nb_png_files > 0 ) {
        end_wavelength = ui->startWavelength->value() + ui->offsetWavelength->value() * (nb_png_files - 1);
    }

    ui->endWavelength->setValue(end_wavelength);
}

void OpenSpectralImagePngDialog::setStartWavelength(int start)
{
    _startWavelength = start;
    updateEndWavelength();
}

void OpenSpectralImagePngDialog::setOffsetWavelength(int offset)
{
    _offsetWavelength = offset;
    updateEndWavelength();
}
