/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "about.h"
#include "ui_about.h"

#include <iostream>
#include <QFile>
#include <QTextStream>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVersionNumber>

About::About(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::About)
{
    ui->setupUi(this);

    ui->_text_browser->setHtml(
                "<h1>Spectral Viewer</h1>"
                "<p><em>" +
                tr("Version ") + QApplication::applicationVersion() + "</em></p>" +
                tr(
                "<h3>Authors:</h3>                                                      \
                </p>Arthur Dufay, Romain Pacanowski, Pascal Barla and Alban Fichet.     \
                </p>                                                                    \
                <h3>Licence GPLv3.</h3>                                                 \
                <p><strong>Copyright (c) CNRS, INRIA 2017 - 2021.</strong></p>          \
                <p>                                                                     \
                This program is free software: you can redistribute it and/or modify    \
                it under the terms of the GNU General Public License as published by    \
                the Free Software Foundation, either version 3 of the License, or       \
                (at your option) any later version.</p>                                 \
                <p>                                                                     \
                This program is distributed in the hope that it will be useful,         \
                but WITHOUT ANY WARRANTY; without even the implied warranty of          \
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           \
                GNU General Public License for more details.</p>                        \
                <p>                                                                     \
                You should have received a copy of the GNU General Public License       \
                along with this program.  If not, see <https://www.gnu.org/licenses/>.  \
                </p>"));

    resize(WIDTH, HEIGHT);
}

About::~About()
{
    delete ui;
}

void About::on_checkVerison_clicked()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager, &QNetworkAccessManager::finished,
            this, [=](QNetworkReply *reply) {
        if (reply->error() == QNetworkReply::NoError) {
            QJsonDocument d = QJsonDocument::fromJson(reply->readAll());

            QVersionNumber latestVersion = QVersionNumber::fromString(d.object().value("LastVersion").toString());
            QVersionNumber currentVerison = QVersionNumber::fromString(QApplication::applicationVersion());

            if (latestVersion > currentVerison) {
                QMessageBox newerVersionInfo;
                newerVersionInfo.setText(tr("A newer version of this program is available."));
                newerVersionInfo.exec();
            } else {
                QMessageBox newerVersionInfo;
                newerVersionInfo.setText(tr("You're up to date."));
                newerVersionInfo.exec();
            }
        } else {
            QMessageBox err;
            err.setText(tr("Error contacting the server. Ensure you're connected to the Internet."));
            err.exec();
        }
    });

    manager->get(QNetworkRequest(QUrl("https://mrf-devteam.gitlab.io/spectral-viewer/last_release.json")));
}
