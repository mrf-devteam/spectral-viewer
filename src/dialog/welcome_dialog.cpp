/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "welcome_dialog.h"
#include "ui_welcome_dialog.h"

#include <QFileDialog>
#include <QStyle>

WelcomeDialog::WelcomeDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::WelcomeDialog)
    , _mode(SINGLE_FILE)
{
    ui->setupUi(this);
    _workingDirectory = "";
}


WelcomeDialog::~WelcomeDialog()
{
    delete ui;
}


QString WelcomeDialog::filepath() const
{
    return _filepath;
}


WelcomeDialog::Mode WelcomeDialog::mode() const
{
    return _mode;
}


//void WelcomeDialog::on_pushButtonOpenFile_clicked()
//{
//    QString fileName = QFileDialog::getOpenFileName(
//                this,
//                tr("Open ENVI / ARTRAW / EXR / Image"),
//                _workingDirectory,
//                tr("Image Files (*.hdr *.artraw *.spd *.exr *.hdr *.pfm *.png *.bmp *.gif *.jpg *.jpeg *.pbm *. pgm *.ppm *.xbm *.xpm)"));

//    if (fileName.size() != 0) {
//        _filepath = fileName;
//        _mode = SINGLE_FILE;

//        accept();
//    }
//}


//void WelcomeDialog::on_pushButtonOpenFolder_clicked()
//{

//}


//void WelcomeDialog::on_pushButtonExit_clicked()
//{
//    reject();
//}
