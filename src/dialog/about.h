/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *  - Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QMessageBox>
#include <QImage>

namespace Ui
{
class About;
}

class About : public QDialog
{
  Q_OBJECT

 public:
  explicit About(QWidget *parent);
  virtual ~About();

 public slots:

private slots:
    void on_checkVerison_clicked();

private:
  Ui::About *ui;

  static const size_t WIDTH = 800;
  static const size_t HEIGHT = 500;
};
