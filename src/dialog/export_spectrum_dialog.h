/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *  - Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QDialog>

namespace Ui
{
class ExportSpectrumDialog;
}

class ExportSpectrumDialog : public QDialog
{
  Q_OBJECT

 public:
  explicit ExportSpectrumDialog(QWidget *parent, QString const & default_dir);

  ~ExportSpectrumDialog();

  QString filePath();

  bool exportKernel() const;

private slots:
  void on_open_folder_clicked();

  void on_radio_single_pixel_toggled(bool checked);

private:
  Ui::ExportSpectrumDialog *ui;
  QString _default_dir;
  bool _export_kernel;
};
