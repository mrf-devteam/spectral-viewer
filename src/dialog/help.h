/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *  - Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QMessageBox>
#include <QImage>

namespace Ui
{
class Help;
}

class Help : public QDialog
{
  Q_OBJECT

 public:
  explicit Help(QWidget *parent);
  ~Help();

 public slots:

 private:
  Ui::Help *ui;
};
