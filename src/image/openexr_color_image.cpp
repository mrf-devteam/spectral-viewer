/**
 *
 * author: Alban Fichet <alban.fichet@gmx.fr>
 * Copyright Charles University 2021
 *
 **/

#ifdef HAS_OPENEXR

#include "openexr_color_image.hpp"
#include <string>

#include <OpenEXR/ImfRgbaFile.h>
#include <OpenEXR/ImfArray.h>
#include <OpenEXR/ImfHeader.h>
#include <OpenEXR/ImfChromaticitiesAttribute.h>
#include <OpenEXR/ImfStringAttribute.h>
#include <Imath/ImathBox.h>

using mrf::color::Color;

namespace mrf
{
namespace image
{
OpenEXRColorImage::OpenEXRColorImage(size_t width, size_t height): ColorImage(width, height) {}

OpenEXRColorImage::OpenEXRColorImage(
        std::string const &filepath,
        std::function<void(int)> progress)
    : ColorImage()
{
    progress(0);

    Imf::RgbaInputFile file(filepath.c_str());

    const Imath::Box2i dw = file.dataWindow();
    const size_t w  = dw.max.x - dw.min.x + 1;
    const size_t h = dw.max.y - dw.min.y + 1;

    MultiChannelImage::init(w, h);

    // This can be faster but letting OpenEXR figuring out everything greatly
    // simplifies the various cases we can encounter: RGB, RGBA, YC...
    Imf::Array2D<Imf::Rgba> pixels(h, w);

    file.setFrameBuffer(&pixels[0][0] - dw.min.x - dw.min.y * width(), 1, width());
    file.readPixels (dw.min.y, dw.max.y);

    // Check if there is specific chromaticities
    const Imf::ChromaticitiesAttribute *c = file.header().findTypedAttribute<Imf::ChromaticitiesAttribute>("chromaticities");

    Imf::Chromaticities chromaticities;

    if (c != nullptr) {
        chromaticities = c->value();
    }

    // Handle custom chromaticities
    Imath::M44f RGB_XYZ = Imf::RGBtoXYZ(chromaticities, 1.f);
    Imath::M44f XYZ_RGB = Imf::XYZtoRGB(Imf::Chromaticities(), 1.f);

    Imath::M44f conversionMatrix = RGB_XYZ * XYZ_RGB;

    // Now copy back to the object's local buffer
    for (size_t y = 0; y < height(); y++) {
        for (size_t x = 0; x < width(); x++) {
            Imath::V3f rgb(pixels[y][x].r, pixels[y][x].g, pixels[y][x].b);
            rgb *= conversionMatrix;

            (*this)[y * width() + x][0] = rgb.x;
            (*this)[y * width() + x][1] = rgb.y;
            (*this)[y * width() + x][2] = rgb.z;
        }
    }

    // Read string metadata
    for (Imf::Header::ConstIterator it = file.header().begin();
         it != file.header().end();
         it++) {
        // We just handle string attributes for now
        if (strcmp(it.attribute().typeName(),Imf::StringAttribute::staticTypeName()) == 0) {
            auto attr = Imf::StringAttribute::cast(it.attribute());
            _metadata[it.name()] = attr.value();
        }
    }
    progress(100);
}

OpenEXRColorImage::OpenEXRColorImage(const ColorImage &img): ColorImage(img) {}

OpenEXRColorImage::~OpenEXRColorImage() {}

IMAGE_LOAD_SAVE_FLAGS OpenEXRColorImage::save(
        const std::string &      /*filepath*/,
        const std::string &      /*additional_comments*/,
        std::function<void(int)> progress
        ) const
{
    progress(0);
    // TODO
    progress(100);

    return MRF_NO_ERROR;
}


} // namespace image
} // namespace mrf

#endif // HAS_OPENEXR
