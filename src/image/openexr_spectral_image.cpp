/**
 *
 * author: Alban Fichet <alban.fichet@gmx.fr>
 *
 * Copyright (c) 2020 Alban Fichet, Romain Pacanowski, Alexander Wilkie
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
 * used to endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "openexr_spectral_image.hpp"

#ifdef HAS_OPENEXR

// We use some static functions declared here
#include <mrf_core/image/exr_spectral_image.hpp>

#include <regex>
#include <array>
#include <algorithm>

#include <OpenEXR/ImfInputFile.h>
#include <OpenEXR/ImfOutputFile.h>
#include <OpenEXR/ImfChannelList.h>
#include <OpenEXR/ImfStringAttribute.h>
#include <OpenEXR/ImfFrameBuffer.h>
#include <OpenEXR/ImfHeader.h>

namespace mrf
{
namespace image
{

OpenEXRSpectralImage::OpenEXRSpectralImage(
        size_t                   width,
        size_t                   height,
        uint                     start_wavelength,
        uint                     stop_wavelength,
        uint                     nb_wavelength,
        radiometry::SpectrumType spectrumType)
    : PolarisedSpectralImage(width, height, start_wavelength, stop_wavelength, nb_wavelength, spectrumType)
{}


OpenEXRSpectralImage::OpenEXRSpectralImage(
        const std::string &filename,
        std::function<void(int)> progress)
    : PolarisedSpectralImage(radiometry::SPECTRUM_NONE)
{
    progress(0);

    Imf::InputFile      exrIn(filename.c_str());
    const Imf::Header & exrHeader     = exrIn.header();
    const Imath::Box2i &exrDataWindow = exrHeader.dataWindow();

    const int w = exrDataWindow.max.x - exrDataWindow.min.x + 1;
    const int h = exrDataWindow.max.y - exrDataWindow.min.y + 1;

    // -----------------------------------------------------------------------
    // Determine channels' position
    // -----------------------------------------------------------------------

    const Imf::ChannelList &exrChannels = exrHeader.channels();

    std::array<std::vector<std::pair<float, std::string>>, 4> wavelengths_nm_S;
    std::vector<std::pair<float, std::string>> wavelengths_nm_diagonal;
    std::vector<std::pair<std::pair<float, float>, std::string>>
            reradiation_wavelengths_nm;

    for (Imf::ChannelList::ConstIterator channel = exrChannels.begin();
         channel != exrChannels.end();
         channel++) {
        // Check if the channel is a spectral or a bispectral one
        int    polarisationComponent;
        double in_wavelength_nm, out_wavelength_nm;

        radiometry::SpectrumType currChannelType = channelType(
                    channel.name(),
                    polarisationComponent,
                    in_wavelength_nm,
                    out_wavelength_nm);

        if (currChannelType != radiometry::SPECTRUM_NONE) {
            _spectrumType = _spectrumType | currChannelType;

            if (isReflective(currChannelType)) {
                if (!isBispectral(currChannelType)) {
                    wavelengths_nm_diagonal.push_back(
                                std::make_pair(in_wavelength_nm, channel.name()));
                } else {
                    reradiation_wavelengths_nm.push_back(std::make_pair(
                                                             std::make_pair(in_wavelength_nm, out_wavelength_nm),
                                                             channel.name()));
                }
            } else if (isEmissive(currChannelType)) {
                assert(polarisationComponent < 4);
                wavelengths_nm_S[polarisationComponent].push_back(
                            std::make_pair(in_wavelength_nm, channel.name()));
            }
        }
    }

    // Sort by ascending wavelengths
    for (size_t s = 0; s < nStokesComponents(); s++) {
        std::sort(wavelengths_nm_S[s].begin(), wavelengths_nm_S[s].end());
    }

    std::sort(wavelengths_nm_diagonal.begin(), wavelengths_nm_diagonal.end());

    struct {
        bool operator()(
                    std::pair<std::pair<float, float>, std::string> a,
                    std::pair<std::pair<float, float>, std::string> b) const
        {
            if (a.first.first == b.first.first) {
                return a.first.second < b.first.second;
            } else {
                return a.first.first < b.first.first;
            }
        }
    } sortBispectral;

    std::sort(
                reradiation_wavelengths_nm.begin(),
                reradiation_wavelengths_nm.end(),
                sortBispectral);

    // -------------------------------------------------------------------------
    // Sanity check
    // -------------------------------------------------------------------------

    if (_spectrumType == radiometry::SPECTRUM_NONE) {
        // Probably an RGB EXR, not our job to handle it
        throw MRF_ERROR_WRONG_PIXEL_FORMAT;
    }

    if (emissive()) {
        // Check we have the same wavelength for each Stokes component
        // Wavelength vectors must be of the same size
        const float base_size_emissive = wavelengths_nm_S[0].size();

        for (size_t s = 1; s < nStokesComponents(); s++) {
            if (wavelengths_nm_S[s].size() != base_size_emissive) {
                throw MRF_ERROR_WRONG_PIXEL_FORMAT;
            }

            // Wavelengths must correspond
            for (size_t wl_idx = 0; wl_idx < base_size_emissive; wl_idx++) {
                if (
                        wavelengths_nm_S[s][wl_idx].first
                        != wavelengths_nm_S[0][wl_idx].first) {
                    throw MRF_ERROR_WRONG_PIXEL_FORMAT;
                }
            }
        }
    }

    // If both reflective and emissive, we need to perform a last sanity check
    if (emissive() && reflective()) {
        const size_t n_emissive_wavelengths   = wavelengths_nm_S[0].size();
        const size_t n_reflective_wavelengths = wavelengths_nm_diagonal.size();

        if (n_emissive_wavelengths != n_reflective_wavelengths)
            throw MRF_ERROR_WRONG_PIXEL_FORMAT;

        for (size_t wl_idx = 0; wl_idx < n_emissive_wavelengths; wl_idx++) {
            if (wavelengths_nm_S[0][wl_idx] != wavelengths_nm_diagonal[wl_idx])
                throw MRF_ERROR_WRONG_PIXEL_FORMAT;
        }
    }

    // Check every single reradiation have all upper wavelength values
    // Note: this shall not be mandatory in the format but, we only support that for now
    if (bispectral()) {
        if (reradiation_wavelengths_nm.size() != reradiationSize()) {
            std::cerr << "Reradiation is incomplete" << std::endl;
        }

        float  currDiagWl  = wavelengths_nm_diagonal[0].first;
        size_t diagonalIdx = 0;
        size_t reradIdx    = 1;

        for (size_t rr = 0; rr < reradiation_wavelengths_nm.size(); rr++) {
            const auto &rerad = reradiation_wavelengths_nm[rr];

            if (rerad.first.first > currDiagWl) {
                if (diagonalIdx + reradIdx != nSpectralBands()) {
                    std::cerr << "We need the full spectral reradiation "
                             "specification"
                          << std::endl;
                }
                diagonalIdx++;
                reradIdx = 1;

                currDiagWl = wavelengths_nm_diagonal[diagonalIdx].first;
            }

            if (rerad.first.first != wavelengths_nm_diagonal[diagonalIdx].first) {
                std::cerr << "We need the full spectral reradiation specification"
                        << std::endl;
            }

            if (rerad.first.second != wavelengths_nm_diagonal[diagonalIdx + reradIdx].first) {
                std::cerr << "We need the full spectral reradiation specification"
                        << std::endl;
            }

            reradIdx++;
        }
    }

    // -----------------------------------------------------------------------
    // Allocate memory
    // -----------------------------------------------------------------------
    std::vector<mrf::uint> wavelengths;

    if (emissive()) {
        wavelengths.reserve(wavelengths_nm_S[0].size());

        for (const auto &wl_index : wavelengths_nm_S[0]) {
            wavelengths.push_back(static_cast<mrf::uint>(std::round(wl_index.first)));
        }
    } else {
        wavelengths.reserve(wavelengths_nm_diagonal.size());

        for (const auto &wl_index : wavelengths_nm_diagonal) {
            wavelengths.push_back(static_cast<mrf::uint>(std::round(wl_index.first)));
        }
    }

    allocateMemory(w, h, wavelengths);

    // -----------------------------------------------------------------------
    // Read the pixel data
    // -----------------------------------------------------------------------

    Imf::FrameBuffer exrFrameBuffer;

    const Imf::PixelType compType = Imf::FLOAT;

    // Set the diagonal for reading

    for (size_t s = 0; s < nStokesComponents(); s++) {
        for (size_t wl_idx = 0; wl_idx < nSpectralBands(); wl_idx++) {
            char *ptrS = (char *)(emissiveFramebuffer(s)[wl_idx].data());
            Imf::Slice slice = Imf::Slice::Make(compType, ptrS, exrDataWindow);

            exrFrameBuffer.insert(wavelengths_nm_S[s][wl_idx].second, slice);
        }
    }

    if (reflective()) {
        for (size_t wl_idx = 0; wl_idx < nSpectralBands(); wl_idx++) {
            char *ptrR = (char *)(reflectiveFramebuffer()[wl_idx].data());
            Imf::Slice slice = Imf::Slice::Make(compType, ptrR, exrDataWindow);

            exrFrameBuffer.insert(wavelengths_nm_diagonal[wl_idx].second, slice);
        }

        if (bispectral()) {
            // Set the reradiation part fo reading
            // TODO
//            const size_t xStrideReradiation = sizeof(float) * reradiationSize();
//            const size_t yStrideReradiation = xStrideReradiation * width();

//            for (size_t rr = 0; rr < reradiationSize(); rr++) {
//                char *framebuffer = (char *)(&_reradiation[rr]);
//                exrFrameBuffer.insert(
//                            reradiation_wavelengths_nm[reradiationSize() - rr - 1].second,
//                        Imf::Slice(
//                            compType,
//                            framebuffer,
//                            xStrideReradiation,
//                            yStrideReradiation));
//            }
        }
    }

    exrIn.setFrameBuffer(exrFrameBuffer);
    exrIn.readPixels(exrDataWindow.min.y, exrDataWindow.max.y);

    // -----------------------------------------------------------------------
    // Read metadata
    // -----------------------------------------------------------------------

    // Check if the version match
    const Imf::StringAttribute *versionAttr
            = exrHeader.findTypedAttribute<Imf::StringAttribute>(VERSION_ATTR);

    if (
            versionAttr == nullptr
            || strcmp(versionAttr->value().c_str(), "1.0") != 0) {
        std::cerr << "WARN: The version is different from the one expected by "
                       "this library or unspecified"
                    << std::endl;
    }

    // Units (required for emissive images)
    const Imf::StringAttribute *emissiveUnitsAttr
            = exrHeader.findTypedAttribute<Imf::StringAttribute>(EMISSIVE_UNITS_ATTR);

    if (emissive()
            && (emissiveUnitsAttr == nullptr
                || strcmp(emissiveUnitsAttr->value().c_str(), "W.m^-2.sr^-1") != 0)) {
        std::cerr << "WARN: This unit is not supported or unspecified. We are "
                       "going to use "
                       "W.m^-2.sr^-1 instead"
                    << std::endl;
    }

    // Lens transmission data
//    const Imf::StringAttribute *lensTransmissionAttr
//            = exrHeader.findTypedAttribute<Imf::StringAttribute>(
//                LENS_TRANSMISSION_ATTR);

//    if (lensTransmissionAttr != nullptr) {
//        try {
//            _lensTransmissionSpectra = SpectrumAttribute(*lensTransmissionAttr);
//        } catch (SpectrumAttribute::Error &e) {
//            throw INCORRECT_FORMED_FILE;
//        }
//    }

//    // Camera spectral response
//    const Imf::StringAttribute *cameraResponseAttr
//            = exrHeader.findTypedAttribute<Imf::StringAttribute>(
//                CAMERA_RESPONSE_ATTR);

//    if (cameraResponseAttr != nullptr) {
//        try {
//            _cameraReponse = SpectrumAttribute(*cameraResponseAttr);
//        } catch (SpectrumAttribute::Error &e) {
//            throw INCORRECT_FORMED_FILE;
//        }
//    }

//    // Each channel sensitivity
//    _channelSensitivities.resize(nSpectralBands());

//    for (size_t i = 0; i < wavelengths_nm_S[0].size(); i++) {
//        const Imf::StringAttribute *filterTransmissionAttr
//                = exrHeader.findTypedAttribute<Imf::StringAttribute>(
//                    wavelengths_nm_S[0][i].second);

//        if (filterTransmissionAttr != nullptr) {
//            try {
//                _channelSensitivities[i] = SpectrumAttribute(*filterTransmissionAttr);
//            } catch (SpectrumAttribute::Error &e) {
//                throw INCORRECT_FORMED_FILE;
//            }
//        }
//    }

//    // Exposure compensation value
//    const Imf::FloatAttribute *exposureCompensationAttr
//            = exrHeader.findTypedAttribute<Imf::FloatAttribute>(
//                EXPOSURE_COMPENSATION_ATTR);

//    if (exposureCompensationAttr != nullptr) {
//        try {
//            _ev = exposureCompensationAttr->value();
//        } catch (std::invalid_argument &e) {
//            throw INCORRECT_FORMED_FILE;
//        }
//    }

//    // Polarisation handedness
//    const Imf::StringAttribute *polarisationHandednessAttr
//            = exrHeader.findTypedAttribute<Imf::StringAttribute>(
//                POLARISATION_HANDEDNESS_ATTR);

//    if (polarisationHandednessAttr != nullptr) {
//        if (polarisationHandednessAttr->value() == "left") {
//            _polarisationHandedness = LEFT_HANDED;
//        } else if (polarisationHandednessAttr->value() == "right") {
//            _polarisationHandedness = RIGHT_HANDED;
//        } else {
//            throw INCORRECT_FORMED_FILE;
//        }
//    }
    progress(100);
}


OpenEXRSpectralImage::OpenEXRSpectralImage(UniformSpectralImage const &other)
    : PolarisedSpectralImage(other)
{}


OpenEXRSpectralImage::~OpenEXRSpectralImage() {}


IMAGE_LOAD_SAVE_FLAGS OpenEXRSpectralImage::save(
        const std::string &      filepath,
        const std::string &      additional_comments,
        std::function<void(int)> progress) const
{
    progress(0);
    // TODO
    progress(100);

    return MRF_NO_ERROR;
}


bool OpenEXRSpectralImage::isSpectral(std::string const &filename)
{
    Imf::InputFile      exrIn(filename.c_str());
    const Imf::Header & exrHeader     = exrIn.header();
    const Imf::ChannelList &exrChannels = exrHeader.channels();

    // Determine if there is spectral channels

    for (Imf::ChannelList::ConstIterator channel = exrChannels.begin();
         channel != exrChannels.end();
         channel++) {

        // Check if the channel is a spectral or a bispectral one
        int    polarisationComponent;
        double in_wavelength_nm, out_wavelength_nm;

        radiometry::SpectrumType currChannelType = channelType(
                    channel.name(),
                    polarisationComponent,
                    in_wavelength_nm,
                    out_wavelength_nm);

        if (currChannelType != radiometry::SPECTRUM_NONE) {
            return true;
        }
    }

    return false;
}

radiometry::SpectrumType OpenEXRSpectralImage::channelType(
        const std::string &channelName,
        int &polarisationComponent,
        double &wavelength_nm,
        double &reradiation_wavelength_nm)
{
    const std::string exprRefl   = "T";
    const std::string exprStokes = "S([0-3])";
    const std::string exprPola   = "((" + exprStokes + ")|" + exprRefl + ")";
    const std::string exprValue  = "(\\d*,?\\d*([Ee][+-]?\\d+)?)";
    const std::string exprUnits  = "(Y|Z|E|P|T|G|M|k|h|da|d|c|m|u|n|p)?(m|Hz)";

    const std::regex exprDiagonal(
                "^" + exprPola + "\\." + exprValue + exprUnits + "$");
    const std::regex exprRerad(
                "^" + exprRefl + "\\." + exprValue + exprUnits + "\\." + exprValue
                + exprUnits + "$");

    std::smatch matches;

    const bool matchedDiagonal
            = std::regex_search(channelName, matches, exprDiagonal);

    if (matchedDiagonal) {
        if (matches.size() != 8) {
            // Something went wrong with the parsing. This shall not occur.
            throw MRF_ERROR_DECODING_FILE;
        }

        radiometry::SpectrumType type;

        switch (matches[1].str()[0]) {
        case 'S':
            type                  = radiometry::SPECTRUM_EMISSIVE;
            polarisationComponent = std::stoi(matches[3].str());
            if (polarisationComponent > 0) {
                type = type | radiometry::SPECTRUM_POLARISED;
            }
            break;

        case 'T':
            type = radiometry::SPECTRUM_REFLECTIVE;
            break;

        default:
            return radiometry::SPECTRUM_NONE;
        }

        // Get value illumination
        std::string centralValueStr(matches[4].str());
        std::replace(centralValueStr.begin(), centralValueStr.end(), ',', '.');
        const double value = std::stod(centralValueStr);

        wavelength_nm = EXRSpectralImage::strToNanometers(
                    value,              // Comma separated floating point value
                    matches[6].str(),   // Unit multiplier
                    matches[7].str()    // Units
                );

        return type;
    }

    const bool matchedRerad
            = std::regex_search(channelName, matches, exprRerad);

    if (matchedRerad) {
        if (matches.size() != 9) {
            // Something went wrong with the parsing. This shall not occur.
            throw MRF_ERROR_DECODING_FILE;
        }

        // Get value illumination
        std::string centralValueStrI(matches[1].str());
        std::replace(centralValueStrI.begin(), centralValueStrI.end(), ',', '.');
        const float value_i = std::stof(centralValueStrI);

        wavelength_nm = EXRSpectralImage::strToNanometers(
                    value_i,
                    matches[3].str(),   // Unit multiplier
                    matches[4].str()    // Units
                );

        // Get value reradiation
        std::string centralValueStrO(matches[5].str());
        std::replace(centralValueStrO.begin(), centralValueStrO.end(), ',', '.');
        const float value_o = std::stof(centralValueStrO);

        reradiation_wavelength_nm = EXRSpectralImage::strToNanometers(
                    value_o,
                    matches[7].str(),   // Unit multiplier
                    matches[8].str()    // Units
                );

        return radiometry::SPECTRUM_BISPECTRAL;
    }

    return radiometry::SPECTRUM_NONE;
}

} // namespace image
} // namespace mrf

#endif // HAS_OPENEXR
