/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core/image/color_image.hpp>

#ifdef HAS_TIFF

namespace mrf
{
namespace image
{
class TIFFColorImage: public ColorImage
{
public:
  TIFFColorImage(size_t width = 0, size_t height = 0);

  TIFFColorImage(
      const std::string &      file_path,
      std::function<void(int)> progress = [](int) {
      });

  TIFFColorImage(ColorImage const &img);

  virtual ~TIFFColorImage();

  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      addional_comments = "",
      std::function<void(int)> progress =
          [](int) {
          }) const;
};
}   // namespace image
}   // namespace mrf

#endif // HAS_TIFF
