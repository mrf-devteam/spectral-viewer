/**
 *
 * author: Alban Fichet <alban.fichet@gmx.fr>
 *
 * Copyright (c) 2020 Alban Fichet, Romain Pacanowski, Alexander Wilkie
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
 * used to endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once


#include <mrf_core/image/polarised_spectral_image.hpp>

#ifdef HAS_OPENEXR

namespace mrf {
namespace image {

class OpenEXRSpectralImage: public PolarisedSpectralImage
{
public:
    /**
     * @brief Creates a new empty EXR spectral image
     *
     * @param width            Image width.
     * @param height           Image height.
     * @param start_wavelength Start wavelength sampled.
     * @param stop_wavelength  Last wavelength sampled.
     * @param nb_wavelength    Number of wavelength sampled between
     *                         `start_wavelength` and `stop_wavelength`.
     * @param spectrumType     Sets the type of spectrum stored in image pixels.
     */
    OpenEXRSpectralImage(
        size_t                   width            = 0,
        size_t                   height           = 0,
        uint                     start_wavelength = 360,
        uint                     stop_wavelength  = 830,
        uint                     nb_wavelength    = 12,
        radiometry::SpectrumType spectrumType     = radiometry::SPECTRUM_EMISSIVE);

    /**
     * @brief Creates a new EXR spectral image from a stored file
     *
     * @param file_path Path the the EXR spectral file.
     * @param progress  A callback to notify the loading progress in
     *                  percents.
     * @throws IMAGE_LOAD_SAVE_FLAGS if unsucessfull
     */
    OpenEXRSpectralImage(
        const std::string &      filename,
        std::function<void(int)> progress = [](int) {
        });

    /**
     * @brief Creates a new EXR spectral image from another Uniform Spectral
     * Image
     *
     * @param other Other UniformSpectralImage image object.
     */
    OpenEXRSpectralImage(const UniformSpectralImage &other);

    virtual ~OpenEXRSpectralImage();

    /**
     * @brief Saves a file
     *
     * @param file_path The path to the file to save.
     * @param progress  A callback to notify the saving progress in
     *                  percents.
     *
     * @return The save state. MRF_NO_ERROR if successfull.
     */
    virtual IMAGE_LOAD_SAVE_FLAGS save(
        const std::string &      filename,
        const std::string &      additional_comments = "",
        std::function<void(int)> progress            = [](int) {
        }) const;

    static bool isSpectral(std::string const &filepath);


    static radiometry::SpectrumType channelType(
      const std::string &channelName,
      int &              polarisationComponent,
      double &           wavelength_nm,
      double &           reradiation_wavelength_nm);



    static constexpr const char *VERSION_ATTR        = "spectralLayoutVersion";
    static constexpr const char *SPECTRUM_TYPE_ATTR  = "spectrumType";
    static constexpr const char *EMISSIVE_UNITS_ATTR = "emissiveUnits";
    static constexpr const char *LENS_TRANSMISSION_ATTR = "lensTransmission";
    static constexpr const char *CAMERA_RESPONSE_ATTR   = "cameraResponse";
    static constexpr const char *EXPOSURE_COMPENSATION_ATTR = "EV";
    static constexpr const char *POLARISATION_HANDEDNESS_ATTR
      = "polarisationHandedness";
};


} // namespace image
} // namespace mrf

#endif // HAS_OPENEXR
