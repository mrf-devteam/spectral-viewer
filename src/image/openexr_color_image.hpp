/**
 *
 * author: Alban Fichet <alban.fichet@gmx.fr>
 * Copyright Charles University 2021
 *
 **/

#pragma once

#include <mrf_core/image/color_image.hpp>

#ifdef HAS_OPENEXR

namespace mrf {
namespace image {
class OpenEXRColorImage: public ColorImage
{
public:
    OpenEXRColorImage(size_t width = 0, size_t height = 0);

    OpenEXRColorImage(
            const std::string &filepath,
            std::function<void(int)> progress = [](int) {}
            );

    OpenEXRColorImage(ColorImage const &img);

    virtual ~OpenEXRColorImage();

    virtual IMAGE_LOAD_SAVE_FLAGS save(
            const std::string &      file_path,
            const std::string &      addional_comments = "",
            std::function<void(int)> progress =
                [](int) {
                }) const;
};

} // namespace image
} // namespace mrf


#endif // HAS_OPENEXR
