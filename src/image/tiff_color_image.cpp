/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#ifdef HAS_TIFF

#include "tiff_color_image.hpp"
#include <tiffio.h>
#include <string>
#include <cstdint>

using mrf::color::Color;

namespace mrf
{
namespace image
{
TIFFColorImage::TIFFColorImage(size_t width, size_t height): ColorImage(width, height) {}

TIFFColorImage::TIFFColorImage(
        std::string const &file_path,
        std::function<void(int)> progress)
    : ColorImage()
{
    progress(0);

    TIFF* tif = TIFFOpen(file_path.c_str(), "r");

    if (tif)
    {
        uint32_t w, h;
        uint16_t bps, spp;
        uint16_t config;

        TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
        TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
        TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bps);
        TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &spp);
        TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &config);

        tdata_t buf = _TIFFmalloc(TIFFScanlineSize(tif));
        MultiChannelImage::init(w, h);

        auto inverseGamma = mrf::color::getInverseGammaFunction();

        switch(config) {
        case PLANARCONFIG_CONTIG:
            switch(bps) {
            case 8:
                for (uint32_t row = 0; row < height(); row++) {
                    TIFFReadScanline(tif, buf, row, 0);
                    uint8_t *scanline = (uint8_t*)buf;

                    for (uint32_t col = 0; col < width(); col++) {
                        (*this)[row * width() + col][3] = 1.F;

                        switch(spp) {
                        case 1:
                            for (int c = 0; c < 3; c++) {
                                (*this)[row * width() + col][c] = float(scanline[col]) / float(std::numeric_limits<uint8_t>::max());
                            }
                            break;
                        case 3:
                        case 4:
                            for (int c = 0; c < spp; c++) {
                                (*this)[row * width() + col][c] = float(scanline[spp * col + c]) / float(std::numeric_limits<uint8_t>::max());
                            }
                            break;
                        }

                        if (inverseGamma) {
                            (*this)[row * width() + col] = inverseGamma((*this)[row * width() + col]);
                        }
                    }

                    progress(int(float(row) / float(height() - 1) * 100.F));
                }
                break;
            case 16:
                for (uint32_t row = 0; row < height(); row++) {
                    TIFFReadScanline(tif, buf, row, 0);
                    uint16_t *scanline = (uint16_t*)buf;

                    for (uint32_t col = 0; col < width(); col++) {
                        (*this)[row * width() + col][3] = 1.F;

                        switch(spp) {
                        case 1:
                            for (int c = 0; c < 3; c++) {
                                (*this)[row * width() + col][c] = float(scanline[col]) / float(std::numeric_limits<uint16_t>::max());
                            }
                            break;
                        case 3:
                        case 4:
                            for (int c = 0; c < spp; c++) {
                                (*this)[row * width() + col][c] = float(scanline[spp * col + c]) / float(std::numeric_limits<uint16_t>::max());
                            }
                            break;
                        }
                    }

                    progress(int(float(row) / float(height() - 1) * 100.F));
                }
                break;
            case 32:
                for (uint32_t row = 0; row < height(); row++) {
                    TIFFReadScanline(tif, buf, row, 0);
                    uint32_t *scanline = (uint32_t*)buf;

                    for (uint32_t col = 0; col < width(); col++) {
                        (*this)[row * width() + col][3] = 1.F;

                        switch(spp) {
                        case 1:
                            for (int c = 0; c < 3; c++) {
                                (*this)[row * width() + col][c] = float(scanline[col]) / float(std::numeric_limits<uint32_t>::max());
                            }
                            break;
                        case 3:
                        case 4:
                            for (int c = 0; c < spp; c++) {
                                (*this)[row * width() + col][c] = float(scanline[spp * col + c]) / float(std::numeric_limits<uint32_t>::max());
                            }
                            break;
                        }
                    }

                    progress(int(float(row) / float(height() - 1) * 100.F));
                }
                break;
            default:
                _TIFFfree(buf);
                TIFFClose(tif);
                throw MRF_ERROR_WRONG_PIXEL_FORMAT;
                break;
            }
            break;

        // case PLANARCONFIG_SEPARATE:
        default:
            _TIFFfree(buf);
            TIFFClose(tif);
            throw MRF_ERROR_WRONG_PIXEL_FORMAT;
            break;

        }

        _TIFFfree(buf);
        TIFFClose(tif);
    }
    else {
        throw MRF_ERROR_LOADING_FILE;
    }

    progress(100);
}

TIFFColorImage::TIFFColorImage(const ColorImage &img): ColorImage(img) {}


TIFFColorImage::~TIFFColorImage() {}

IMAGE_LOAD_SAVE_FLAGS TIFFColorImage::save(
        const std::string &      file_path,
        const std::string &      /*additional_comments*/,
        std::function<void(int)> progress
        ) const
{
    progress(0);

    TIFF* tif = TIFFOpen(file_path.c_str(), "w");

    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, width());
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, height());
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 16);
    //TIFFSetField(tif, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 4);
    TIFFSetField(tif, TIFFTAG_EXTRASAMPLES, EXTRASAMPLE_ASSOCALPHA);
    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);

    tdata_t buf = _TIFFmalloc(TIFFScanlineSize(tif));

    for (uint32_t row = 0; row < height(); row++) {
        uint16_t *scanline = (uint16_t*)buf;

        for (uint32_t col = 0; col < width(); col++) {
            for (int c = 0; c < 4; c++) {
                const float pixelValue = std::max(std::min((*this)[row * width() + col][c], 1.F), 0.F);
                scanline[4 * col + c] = static_cast<uint16_t>(pixelValue * std::numeric_limits<uint16_t>::max());
                //scanline[4 * col + c] = (*this)[row * width() + col][c];
            }
        }

        TIFFWriteScanline(tif, buf, row, 0);
        progress(int(float(row) / float(height() - 1) * 100.F));
    }

    TIFFClose(tif);

    progress(100);

    return MRF_NO_ERROR;
}

}   // namespace image
}   // namespace mrf

#endif // HAS_TIFF
