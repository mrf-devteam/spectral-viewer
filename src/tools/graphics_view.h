/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QObject>
#include <QWidget>
#include <QGraphicsView>
#include <array>

#include "abstract_controller.h"
#include "spectral_viewer_model.h"
#include "graphics_scene.h"

class GraphicsView : public QGraphicsView, public AbstractController
{
    Q_OBJECT
public:
    GraphicsView(QWidget * parent = 0);

    void setIsColor(bool isColor) {
        _isColorMode = isColor;
    }

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const override;

    virtual void setModel(QSharedPointer<SpectralViewerModel> model) override;

    static bool isLockedOnPixel() { return _isLockedOnPixel; }

    void setAutoscale(bool enabled);

protected:
    void wheelEvent( QWheelEvent * event ) override;
    void mouseMoveEvent(QMouseEvent * event) override;
    void mousePressEvent(QMouseEvent * event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void resizeEvent(QResizeEvent *) override;
    void dropEvent(QDropEvent *ev) override;
    void dragEnterEvent(QDragEnterEvent *ev) override;

public slots:
    void changeSelectionRectangleSize(int size);
    void updateHandleBarsImage(std::pair<int, int> bar_values);
    void onTonemappedUpdated();
    void onFalseColorUpdated();

    void autoscale();

private slots:
    void drawSelectionRectangle(const QPointF &position);

    void onZoomLevelChanged(double zoom);
    void onRotationChanged(double angle);

    void onStartLoading();
    void onLoadSuccess();
    void onLoadFailure();


signals:
    void updatePixelViewerPosition(QPointF);
    void dragImage(std::pair<int, int>);

    void horizontalTranslate(int value);
    void verticalTranslate(int value);

    void autoscaled();

private:
    //true if pixel value to display is locked, happens after a left click
    //decativated on a right click
    static bool _isLockedOnPixel;
    int _selection_rectangle_size;
    QGraphicsRectItem* _selection_rectangle_item;
    std::array<QGraphicsLineItem*, 4> _selection_lines_items{};
    QPoint _start_drag;

    bool _isColorMode;
    bool _isAutoscale;
    bool _userSpecifiedZoom;
    GraphicsScene* _scene;

    static constexpr double ZOOM_FACTOR_DELTA = 1.2;
    static constexpr uchar BLUE_COLOR_R = 0;
    static constexpr uchar BLUE_COLOR_G = 122;
    static constexpr uchar BLUE_COLOR_B = 204;
    static constexpr uchar BLUE_COLOR_A = 128;

    static constexpr qreal CROSS_SIZE = 5;
};
