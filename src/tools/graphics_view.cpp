/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "graphics_view.h"

#include <QScrollBar>
#include <QWheelEvent>
#include <QGraphicsItem>
#include <QGuiApplication>
#include <QMimeData>

#include "math.h"
#include "math_macros.h"

bool GraphicsView::_isLockedOnPixel = false;

GraphicsView::GraphicsView(QWidget * parent)
    : QGraphicsView(parent)
    , _selection_rectangle_size(0)
    , _selection_rectangle_item(nullptr)
    , _start_drag(0, 0)
    , _isColorMode(true)
    , _isAutoscale(false)
    , _userSpecifiedZoom(false)
    , _scene(new GraphicsScene(this))
{
    setScene(_scene);
    setMouseTracking(true);
    setAcceptDrops(true);
}


bool GraphicsView::isActive(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
const
{
    switch (mode) {
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::sRGBImage:
    case SpectralViewerModel::Spectrum:
        // B&W view is irrelevant in such cases
        return _isColorMode;

    case SpectralViewerModel::SpectralImage:
        // We want both colour and B&W view for SpectralImage
        return true;

    case SpectralViewerModel::NoFile:
        return false;
    }

    return false;
}


void GraphicsView::setModel(QSharedPointer<SpectralViewerModel> model) {
    AbstractController::setModel(model);

    _connections.append({
        // Refresh
        connect(_model.data(), SIGNAL(tonemappedUpdated()),
                this         , SLOT(onTonemappedUpdated())),
        connect(_model.data(), SIGNAL(falseColorUpdated()),
                this         , SLOT(onFalseColorUpdated())),

        // Transform
        connect(_model.data(), SIGNAL(zoomLevelChanged(double)),
                this         , SLOT(onZoomLevelChanged(double))),
        connect(_model.data(), SIGNAL(rotationChanged(double)),
                this         , SLOT(onRotationChanged(double))),

        // Selection
        connect(_model.data(), SIGNAL(selectedPixelChanged(QPointF const &)),
                this         , SLOT(drawSelectionRectangle(QPointF const &))),
        connect(_model.data(), SIGNAL(kernelSizeChanged(int)),
                this         , SLOT(changeSelectionRectangleSize(int))),

        // Loading
        connect(_model.data(), SIGNAL(startLoading()),
                this         , SLOT(onStartLoading())),
        connect(_model.data(), SIGNAL(loadSuccess()),
                this         , SLOT(onLoadSuccess())),
        connect(_model.data(), SIGNAL(loadFailure()),
                this         , SLOT(onLoadFailure())),
    });
}

void GraphicsView::setAutoscale(bool enabled) {
    _isAutoscale = enabled;
    if (_isAutoscale) { autoscale(); };
}


void GraphicsView::wheelEvent( QWheelEvent * event )
{
    if (!_model || !_model->isLoaded()) { return; }

    if((event->modifiers() & Qt::ControlModifier) != 0U) {
        QGraphicsView::wheelEvent(event);
    } else {
        const QPoint delta = event->angleDelta();

        if(delta.y() != 0) {
            const double zoom_factor = ZOOM_FACTOR_DELTA * float(std::abs(delta.y())) / 120.F;

            if(delta.y() > 0) {
                _model->setZoomLevel(_model->zoomLevel() * zoom_factor);
            } else {
                _model->setZoomLevel(_model->zoomLevel() / zoom_factor);
            }
        }
    }
}


void GraphicsView::mouseMoveEvent(QMouseEvent * event)
{
    if (!_model || !_model->isLoaded()) { return; }

    if ( ((event->buttons() & Qt::MidButton) != 0U) ||
         (((event->buttons() & Qt::LeftButton) != 0U) && QGuiApplication::keyboardModifiers() == Qt::ControlModifier) )
    {
        QScrollBar *hBar = horizontalScrollBar();
        QScrollBar *vBar = verticalScrollBar();
        QPoint delta = event->pos() - _start_drag;
        std::pair<int, int> bar_values;
        bar_values.first = hBar->value() + (isRightToLeft() ? delta.x() : -delta.x());
        bar_values.second = vBar->value() - delta.y();
        hBar->setValue(bar_values.first);
        vBar->setValue(bar_values.second);
        emit dragImage(bar_values);
        _start_drag = event->pos();
        return;
    }

    //if user didn't lock on a pixel update displayed pixel value
    if (!_isLockedOnPixel) {
        QPointF position = mapToScene(event->pos());

        _model->setSelectedPixel(position);
    }
}


void GraphicsView::mousePressEvent(QMouseEvent * event)
{
    if (!_model || !_model->isLoaded()) { return; }

    if (event->button() == Qt::RightButton)
    {
        _isLockedOnPixel = false;
    }
    else if (event->button() == Qt::LeftButton && QGuiApplication::keyboardModifiers() != Qt::ControlModifier)
    {
        _isLockedOnPixel = true;
    }
    else if ( (event->button() == Qt::MidButton) ||
              (event->button() == Qt::LeftButton && QGuiApplication::keyboardModifiers() == Qt::ControlModifier) )
    {
        QGraphicsView::mousePressEvent(event);
        setCursor(Qt::ClosedHandCursor);
        _start_drag = event->pos();
        return;
    }

    QPointF position = mapToScene(event->pos());

    _model->setSelectedPixel(position);
}


void GraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    QGraphicsView::mouseReleaseEvent(event);
    setCursor(Qt::ArrowCursor);
}


void GraphicsView::resizeEvent(QResizeEvent * /*event*/)
{
    if (!_model || !_model->isLoaded()) { return; }

    if (!_userSpecifiedZoom) {
        autoscale();
    }
}

void GraphicsView::dropEvent(QDropEvent *ev)
{
    if (!_model || !_model->isLoaded()) { return; }

    QList<QUrl> urls = ev->mimeData()->urls();

    if (!urls.empty())
    {
        QString fileName = urls[0].toString();
        QString startFileTypeString =
            #ifdef _WIN32
                "file:///";
            #else
                "file://";
            #endif

        if (fileName.startsWith(startFileTypeString))
        {
            fileName = fileName.remove(0, startFileTypeString.length());
            _model->openFile(fileName);
        }
    }
}


void GraphicsView::dragEnterEvent(QDragEnterEvent *ev)
{
    ev->acceptProposedAction();
}


void GraphicsView::changeSelectionRectangleSize(int size)
{
    if (!_model || !_model->isLoaded()) { return; }

    _selection_rectangle_size = size;
    drawSelectionRectangle(_model->selectedPixel());
}

void GraphicsView::updateHandleBarsImage(std::pair<int, int> bar_values)
{
    QScrollBar *hBar = horizontalScrollBar();
    QScrollBar *vBar = verticalScrollBar();
    hBar->setValue(bar_values.first);
    vBar->setValue(bar_values.second);
}


void GraphicsView::onTonemappedUpdated()
{
    if (!_model || !_model->isLoaded()) { return; }

    if (_isColorMode) {
        const QImage & colorImage = _model->getTonemappedImage();
        scene()->clear();
        scene()->addPixmap(QPixmap::fromImage(colorImage));
    }
}


void GraphicsView::onFalseColorUpdated()
{
    if (!_model->isLoaded()) { return; }

    if (!_isColorMode) {
        const QImage & grayImage = _model->getFalseColorImage();
        scene()->clear();
        scene()->addPixmap(QPixmap::fromImage(grayImage));
    }
}


void GraphicsView::autoscale()
{
    if (!_model || !_model->isLoaded()) { return; }

    const double r = _model->rotation() * M_PI/180.F;
    const double w = std::abs(std::cos(r) * _model->width() + std::sin(r) * _model->height());
    const double h = std::abs(-std::sin(r) * _model->width() + std::cos(r) * _model->height());

    if (_model->model() != SpectralViewerModel::Spectrum)  {
        fitInView(0, 0, w, h, Qt::KeepAspectRatio);

        if (_isAutoscale) {
            double zoom = std::min(viewportTransform().m11(), viewportTransform().m22());

            // We ignore zoom levels above 1 for anything but Spectrum which are 1x1
            // patches
            if (_model->model() != SpectralViewerModel::Spectrum) {
                zoom = std::min(1., zoom);
            }

            // We set the zoom level but we do not want to get notification of this
            // action
            disconnect(_model.data(), SIGNAL(zoomLevelChanged(double)),
                       this         , SLOT(onZoomLevelChanged(double)));

            _model->setZoomLevel(zoom);

            connect(_model.data(), SIGNAL(zoomLevelChanged(double)),
                    this         , SLOT(onZoomLevelChanged(double)));

            emit autoscaled();
        }

        resetTransform();
        rotate(_model->rotation());
        scale(_model->zoomLevel(), _model->zoomLevel());
    } else {
        fitInView(0, 0, w, h, Qt::IgnoreAspectRatio);
    }

    // The zoom level was automatically guessed
    // if the GUI is resized, we need to keep autoscaling
    // (set in resizeEvent)
    _userSpecifiedZoom = false;
}


void GraphicsView::drawSelectionRectangle(QPointF const & position)
{
    if (!_model || !_model->isLoaded()) { return; }
    if (_model->model() == SpectralViewerModel::Spectrum) { return; }

    if (_model->width() > 0 && _model->height() > 0) {
        QPointF pos(
                    CLAMP(position.x(), 0., _model->width() - 1.),
                    CLAMP(position.y(), 0., _model->height() - 1.)
                    );

        // Remove previous selection rectangle
        if ((_selection_rectangle_item != nullptr) && scene()->items().size() > 1) {
            if (scene()->items().contains(_selection_rectangle_item)) {
                scene()->removeItem(_selection_rectangle_item);
            }
            delete _selection_rectangle_item;
            _selection_rectangle_item = nullptr;
        }

        for (QGraphicsLineItem* item : _selection_lines_items) {
            if ((item != nullptr) && scene()->items().size() > 1) {
                if (scene()->items().contains(_selection_rectangle_item)) {
                    scene()->removeItem(item);
                }
                delete item;
                item = nullptr;
            }
        }

        const int line_width = 1;
        QPen blue_pen(QColor(BLUE_COLOR_R, BLUE_COLOR_G, BLUE_COLOR_B));
        blue_pen.setWidth(line_width);

        qreal x1 = NAN;
        qreal x2 = NAN;
        qreal y1 = NAN;
        qreal y2 = NAN;

        y1 = pos.y() - CROSS_SIZE;
        y2 = pos.y() + CROSS_SIZE;

        x1 = x2 = pos.x() - _selection_rectangle_size;
        _selection_lines_items[0] = scene()->addLine(x1, y1, x2, y2, blue_pen);

        x1 = x2 = pos.x() + _selection_rectangle_size;
        _selection_lines_items[1] = scene()->addLine(x1, y1, x2, y2, blue_pen);


        x1 = pos.x() - CROSS_SIZE;
        x2 = pos.x() + CROSS_SIZE;

        y1 = y2 = pos.y() - _selection_rectangle_size;
        _selection_lines_items[2] = scene()->addLine(x1, y1, x2, y2, blue_pen);

        y1 = y2 = pos.y() + _selection_rectangle_size;
        _selection_lines_items[3] = scene()->addLine(x1, y1, x2, y2, blue_pen);


        if (_selection_rectangle_size > 0)
        {
            const qreal width  = 2 * _selection_rectangle_size + 1 - line_width;
            const qreal height = 2 * _selection_rectangle_size + 1 - line_width;

            blue_pen.setWidth(0);

            QBrush brush(
                        QColor(BLUE_COLOR_R, BLUE_COLOR_G, BLUE_COLOR_B, BLUE_COLOR_A),
                        Qt::SolidPattern);

            pos.setX(pos.x() - _selection_rectangle_size);
            pos.setY(pos.y() - _selection_rectangle_size);

            _selection_rectangle_item = scene()->addRect(
                        pos.x(), pos.y(),
                        width, height,
                        blue_pen, brush);
        }
    }
    else
    {
        //setCursor(Qt::ArrowCursor);
    }
}


void GraphicsView::onZoomLevelChanged(double zoom) {
    if (!_model || !_model->isLoaded()) { return; }
    //if (DBL_EQ(viewportTransform().m11(), zoom)) { return; }
    // Don't care about transforms for Spectrum (1x1 patch)
    if (_model->model() == SpectralViewerModel::Spectrum) { return; }

    resetTransform();
    rotate(_model->rotation());
    scale(zoom, zoom);

    // The zoom level was provided by the user
    // -> we do not autoscale anymore for this image
    _userSpecifiedZoom = true;
}

void GraphicsView::onRotationChanged(double angle)
{
    _userSpecifiedZoom = true;

    resetTransform();
    rotate(angle);
    const double zoom = _model->zoomLevel();
    scale(zoom, zoom);

//    autoscale();
}

void GraphicsView::onStartLoading()
{
    scene()->clear();

    // Clenup local references
    // They have been deleted by scene()->clear();
    _selection_rectangle_item = nullptr;

    for (size_t i = 0; i < _selection_lines_items.size(); i++) {
        _selection_lines_items[i] = nullptr;
    }

    //scene()->addText("Loading...");
    //QGraphicsSvgItem *loading = new QGraphicsSvgItem(":/icons/loading.svg");
    //scene()->addItem(loading);
}

void GraphicsView::onLoadSuccess()
{
    scene()->setSceneRect(0, 0, _model->width(), _model->height());

    onTonemappedUpdated();
    onFalseColorUpdated();

    autoscale();
}

void GraphicsView::onLoadFailure()
{
    onTonemappedUpdated();
    onFalseColorUpdated();
}

