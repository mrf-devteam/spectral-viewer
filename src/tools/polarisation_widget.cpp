#include "polarisation_widget.h"
#include "ui_polarisation_widget.h"

PolarisationWidget::PolarisationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PolarisationWidget)
{
    ui->setupUi(this);
}

PolarisationWidget::~PolarisationWidget()
{
    delete ui;
}

bool PolarisationWidget::isActive(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType type)
const {
    return (   mode == SpectralViewerModel::SpectralImage
            && (type & mrf::radiometry::SPECTRUM_EMISSIVE)
            && (type & mrf::radiometry::SPECTRUM_POLARISED));
}

void PolarisationWidget::on_cbS0_stateChanged(int arg1)
{
    _model->setS0Enabled(arg1 == Qt::Checked);
}

void PolarisationWidget::on_cbS1_stateChanged(int arg1)
{
    _model->setS1Enabled(arg1 == Qt::Checked);
}

void PolarisationWidget::on_cbS2_stateChanged(int arg1)
{
    _model->setS2Enabled(arg1 == Qt::Checked);
}

void PolarisationWidget::on_cbS3_stateChanged(int arg1)
{
    _model->setS3Enabled(arg1 == Qt::Checked);
}



void PolarisationWidget::setModel(QSharedPointer<SpectralViewerModel> model)
{
    AbstractController::setModel(model);

    // Force first update
    ui->cbS0->setChecked(model->s0Enabled());
    ui->cbS1->setChecked(model->s1Enabled());
    ui->cbS2->setChecked(model->s2Enabled());
    ui->cbS3->setChecked(model->s3Enabled());

    _connections.append(
    {
        connect(_model.data(), SIGNAL(s0StateChanged(bool)),
        ui->cbS0, SLOT(setChecked(bool))),

        connect(_model.data(), SIGNAL(s1StateChanged(bool)),
        ui->cbS1, SLOT(setChecked(bool))),

        connect(_model.data(), SIGNAL(s2StateChanged(bool)),
        ui->cbS2, SLOT(setChecked(bool))),

        connect(_model.data(), SIGNAL(s3StateChanged(bool)),
        ui->cbS3, SLOT(setChecked(bool))),
    });
}
