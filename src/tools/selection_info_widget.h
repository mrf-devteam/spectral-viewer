/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QWidget>
#include "abstract_controller.h"

namespace Ui {
class SelectionInfoWidget;
}

class SelectionInfoWidget : public QWidget, public AbstractController
{
    Q_OBJECT

public:
    explicit SelectionInfoWidget(QWidget *parent = nullptr);
    virtual ~SelectionInfoWidget();

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const override;

    void setModel(QSharedPointer<SpectralViewerModel> model) override;

signals:
    void selectedPixelChanged(const QPointF& pos);

private slots:
    void onSelectedPixelChanged(const QPointF&);
    void onKernelSizeChanged(int);
    void updateSelectedPixelInfo();

    void setPositionX(const QString &arg1);
    void setPositionY(const QString &arg1);
    void exportSpectrum();

    void onSpectralModeChanged(SpectralViewerModel::Model mode, mrf::radiometry::SpectrumType type);

    void on_linePositionX_editingFinished();

    void on_linePositionY_editingFinished();

private:
    Ui::SelectionInfoWidget *ui;
};
