/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QObject>
#include <QWidget>
#include <QGraphicsScene>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QDropEvent>

class GraphicsScene : public QGraphicsScene
{
  Q_OBJECT

 public:
  GraphicsScene();
  virtual ~GraphicsScene();
  GraphicsScene(QObject * parent = 0 ) : QGraphicsScene(parent) {}
  void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
  void dragMoveEvent(QGraphicsSceneDragDropEvent *event);
  void dropEvent(QGraphicsSceneDragDropEvent *event);
 signals:
  void mouseMove(QPointF position);
  void mouseClickLeft(QPointF position);
  void mouseClickRight(QPointF position);
  void openFileOnDropEvent(QString);
};
