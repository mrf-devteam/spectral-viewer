/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "selection_info_widget.h"
#include "ui_selection_info_widget.h"
#include "dialog/export_spectrum_dialog.h"
#include <limits>
#include <mrf_core/color/color.hpp>

SelectionInfoWidget::SelectionInfoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SelectionInfoWidget)
{
    ui->setupUi(this);
}


SelectionInfoWidget::~SelectionInfoWidget()
{
    delete ui;
}


bool SelectionInfoWidget::isActive(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
const
{
    switch (mode) {
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::SpectralImage:
    case SpectralViewerModel::sRGBImage:
    case SpectralViewerModel::Spectrum:
        return true;

    case SpectralViewerModel::NoFile:
        return false;
    }

    return false;
}


void SelectionInfoWidget::setModel(QSharedPointer<SpectralViewerModel> model)
{
    AbstractController::setModel(model);

    // Force first update
    onSelectedPixelChanged(_model->selectedPixel());
    ui->sbKernelSize->setValue(int(_model->kernelSize()));
    onSpectralModeChanged(_model->model(), _model->spectrumType());

    _connections.append(
    {
        connect(_model.data()   , SIGNAL(selectedPixelChanged(const QPointF&)),
                this            , SLOT(onSelectedPixelChanged(const QPointF&))),

        connect(this            , SIGNAL(selectedPixelChanged(const QPointF&)),
                _model.data()   , SLOT(setSelectedPixel(const QPointF&))),

        connect(_model.data()   , SIGNAL(kernelSizeChanged(int)),
                this            , SLOT(onKernelSizeChanged(int))),

        connect(_model.data()   , SIGNAL(xyzUpdated()),
                this            , SLOT(updateSelectedPixelInfo())),

        connect(_model.data()   , SIGNAL(linearRGBUpdated()),
                this            , SLOT(updateSelectedPixelInfo())),

        connect(_model.data()   , SIGNAL(tonemappedUpdated()),
                this            , SLOT(updateSelectedPixelInfo())),

        connect(_model.data()   , SIGNAL(spectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType)),
                this            , SLOT(onSpectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType))),

        connect(_model.data()   , SIGNAL(kernelSizeChanged(int)),
                ui->sbKernelSize, SLOT(setValue(int))),

        connect(ui->sbKernelSize, SIGNAL(valueChanged(int)),
                _model.data()   , SLOT(setKernelSize(int))),

        connect(ui->linePositionX  , SIGNAL(textChanged(const QString &)),
                this               , SLOT(setPositionX(const QString&))),
        connect(ui->linePositionY  , SIGNAL(textChanged(const QString &)),
                this               , SLOT(setPositionY(const QString&))),
        connect(ui->bExportSpectrum, SIGNAL(clicked()),
                this               , SLOT(exportSpectrum())),
    });
}


void SelectionInfoWidget::onSelectedPixelChanged(const QPointF& position)
{
    if (_model->width() == 0 || _model->height() == 0) {
        return;
    }

    //compute image coordinates
    int px = int(position.x());
    int py = int(position.y());

    ui->linePositionX->setText(QString::number(px));
    ui->linePositionY->setText(QString::number(py));

    updateSelectedPixelInfo();
}


void SelectionInfoWidget::onKernelSizeChanged(int size)
{
    Q_UNUSED(size)

    updateSelectedPixelInfo();
}


void SelectionInfoWidget::updateSelectedPixelInfo()
{
    mrf::color::Color xyz(0.F);
    mrf::color::Color rgb(0.F);
    mrf::color::Color tone_mapped_rgb(0.F);
    mrf::color::Color color(0.F);
    std::vector<float> avg_spectrum_values;
    std::vector<uint> spectrum_wavelengths;
    float avg_spectrum_single_value = 0.F;

    _model->getSelectedPixelInfo(
                xyz,
                rgb,
                tone_mapped_rgb,
                spectrum_wavelengths,
                avg_spectrum_values,
                avg_spectrum_single_value);

    ui->labelValueX->setText(QString::number(xyz.x()));
    ui->labelValueY->setText(QString::number(xyz.y()));
    ui->labelValueZ->setText(QString::number(xyz.z()));

    ui->labelValueX->setCursorPosition(0);
    ui->labelValueY->setCursorPosition(0);
    ui->labelValueZ->setCursorPosition(0);


    ui->labelValueRLinear->setText(QString::number(rgb.r()));
    ui->labelValueGLinear->setText(QString::number(rgb.g()));
    ui->labelValueBLinear->setText(QString::number(rgb.b()));

    ui->labelValueRLinear->setCursorPosition(0);
    ui->labelValueGLinear->setCursorPosition(0);
    ui->labelValueBLinear->setCursorPosition(0);

    ui->labelValueRTonemapped->setText(QString::number(tone_mapped_rgb.r()));
    ui->labelValueGTonemapped->setText(QString::number(tone_mapped_rgb.g()));
    ui->labelValueBTonemapped->setText(QString::number(tone_mapped_rgb.b()));

    ui->labelValueRTonemapped->setCursorPosition(0);
    ui->labelValueGTonemapped->setCursorPosition(0);
    ui->labelValueBTonemapped->setCursorPosition(0);

    const int w = ui->labelColourTonemappedPix->width();
    const int h = ui->labelColourTonemappedPix->height();
    QImage tempImage = QImage(w, h, QImage::Format_RGB888);

    mrf::color::Color t = tone_mapped_rgb.clamped(0, 1) * std::numeric_limits<unsigned char>::max();
    tempImage.fill(
                QColor(
                    static_cast<int>(t.r()),
                    static_cast<int>(t.g()),
                    static_cast<int>(t.b())));

    ui->labelColourTonemappedPix->setPixmap(QPixmap::fromImage(tempImage));

    ui->labelSpectrumAverageValue->setNum(double(avg_spectrum_single_value));
}


void SelectionInfoWidget::setPositionX(
        QString const & arg1
        )
{
    bool ok = false;
    const double x = arg1.toDouble(&ok);
    QPointF currentSelection = _model->selectedPixel();

    if (ok) {
        currentSelection.setX(static_cast<qreal>(x));
        _model->setSelectedPixel(currentSelection);
    } else {
        ui->linePositionX->setText(QString::number(currentSelection.x()));
    }
}


void SelectionInfoWidget::setPositionY(
        QString const & arg1
        )
{
    bool ok = false;
    const double y = arg1.toDouble(&ok);
    QPointF currentSelection = _model->selectedPixel();

    if (ok) {
        currentSelection.setY(static_cast<qreal>(y));
        _model->setSelectedPixel(currentSelection);
    } else {
        ui->linePositionY->setText(QString::number(currentSelection.y()));
    }
}


void SelectionInfoWidget::exportSpectrum()
{
    if (_model->model() == SpectralViewerModel::sRGBImage) { return;
}

    ExportSpectrumDialog widget(this, _model->openedFileFolder());

    if (widget.exec() != 0) {
        size_t kernelSize = _model->kernelSize();

        if (!widget.exportKernel()) {
            _model->saveSpectrumAtSelectedPixel(widget.filePath());
        } else {
            _model->saveSpectrumAtSelectedPixel(widget.filePath(), kernelSize);
        }
    }
}


void SelectionInfoWidget::onSpectralModeChanged(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
{
    switch(mode) {
    case SpectralViewerModel::sRGBImage:
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::NoFile:
        ui->groupBoxSpectrum->hide();
        ui->bExportSpectrum ->hide();
        ui->labelXYZ        ->hide();
        ui->labelValueX     ->hide();
        ui->labelValueY     ->hide();
        ui->labelValueZ     ->hide();
        break;
    case SpectralViewerModel::SpectralImage:
        ui->groupBoxSpectrum->show();
        ui->bExportSpectrum ->show();
        ui->labelXYZ        ->show();
        ui->labelValueX     ->show();
        ui->labelValueY     ->show();
        ui->labelValueZ     ->show();
        break;
    case SpectralViewerModel::Spectrum:
        ui->groupBoxSpectrum->show();
        ui->bExportSpectrum ->hide();
        ui->labelXYZ        ->show();
        ui->labelValueX     ->show();
        ui->labelValueY     ->show();
        ui->labelValueZ     ->show();
        break;
    }

    switch(mode) {
    case SpectralViewerModel::sRGBImage:
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::SpectralImage:
        ui->groupBoxPosition->show();
        break;
    case SpectralViewerModel::NoFile:
    case SpectralViewerModel::Spectrum:
        ui->groupBoxPosition->hide();
        break;
    }
}

void SelectionInfoWidget::on_linePositionX_editingFinished()
{
    QPointF newPoint (ui->linePositionX->text().toFloat(),
                      _model->selectedPixel().y());

    emit selectedPixelChanged(newPoint);
}


void SelectionInfoWidget::on_linePositionY_editingFinished()
{
    QPointF newPoint (_model->selectedPixel().x(),
                      ui->linePositionY->text().toFloat());

    emit selectedPixelChanged(newPoint);
}

