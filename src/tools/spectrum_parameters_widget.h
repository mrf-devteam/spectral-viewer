/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QWidget>

#include "abstract_controller.h"
#include "spectrum_chart.h"

namespace Ui {
class SpectrumParametersWidget;
}

class SpectrumParametersWidget : public QWidget, public AbstractController
{
    Q_OBJECT

public:
    explicit SpectrumParametersWidget(QWidget *parent = nullptr);
    virtual ~SpectrumParametersWidget();

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const override;

    virtual void setModel(QSharedPointer<SpectralViewerModel> model) override;


    void setSpectrumChart(SpectrumChart* pSpectrumChart);

public slots:
    void onSpectralModeChanged(SpectralViewerModel::Model mode, mrf::radiometry::SpectrumType);

    void onMinSelectedWavelengthChanged(double minSelectedWavelength);
    void onMaxSelectedWavelengthChanged(double maxSelectedWavelength);
    void onLuminanceMulChanged(double luminanceMul);

    void onMinWavelengthChanged(double minWavelength);
    void onMaxWavelengthChanged(double maxWavelenght);
    void onAutoscaleWavelengthChanged(bool autoscaleWavelength);

    void onMinIntensityChanged(double minIntensity);
    void onMaxIntensityChanged(double maxIntensity);
    void onAutoscaleIntensityChanged(bool autoscaleIntensity);

    void onInterpolationModeChanged(bool interpolate);
    void onShowPointChanged(bool showPoint);

    void onLoadSuccess();

signals:
    void minSelectedWavelengthChanged(double minSelectedWavelength);
    void maxSelectedWavelengthChanged(double maxSelectedWavelength);
    void luminanceMulChanged(double luminanceMul);

    void minWavelengthChanged(double minWavelength);
    void maxWavelengthChanged(double maxWavelength);
    void autoscaleWavelengthChanged(bool autoscaleWavelength);

    void minIntensityChanged(double minIntensity);
    void maxIntensityChanged(double maxIntensity);
    void autoscaleIntensityChanged(bool autoccaleIntensity);

private:
    Ui::SpectrumParametersWidget *ui;
    SpectrumChart* _pSpectrumChart;
    QVector<QMetaObject::Connection> _spectrumConnections;
};

