/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "graphics_scene.h"

#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <iostream>

GraphicsScene::GraphicsScene()
{
}

GraphicsScene::~GraphicsScene()
{
}

void GraphicsScene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    event->setAccepted(true);
}

void GraphicsScene::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
    event->setAccepted(true);
}

void GraphicsScene::dropEvent(QGraphicsSceneDragDropEvent *ev)
{
    QList<QUrl> urls = ev->mimeData()->urls();

    if (!urls.empty())
    {
        QString fileName = urls[0].toString();
        QString startFileTypeString =
            #ifdef _WIN32
                "file:///";
            #else
                "file://";
            #endif

        if (fileName.startsWith(startFileTypeString)) {
            fileName = fileName.remove(0, startFileTypeString.length());
            emit openFileOnDropEvent(fileName);
        }
    }
}
