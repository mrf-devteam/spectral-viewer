/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QObject>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QtCharts/QChartView>
#include <QtCharts/QChart>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QScatterSeries>
#include <QDropEvent>
#include <QValueAxis>

#include <mrf_core/color/spectrum_converter.hpp>

#include "abstract_controller.h"
#include "spectral_viewer_model.h"


class SpectrumChart : public QGraphicsView, public AbstractController
{
    Q_OBJECT
public:
    SpectrumChart( QWidget * parent = nullptr );
    virtual ~SpectrumChart();

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const override;

    void setModel(QSharedPointer<SpectralViewerModel> model) override;
    void writeSettings(QSettings &settings) const override;
    void readSettings (QSettings &settings) override;

    void removeSeries();
    void setDarkTheme();
    void setSpectrumPointList(const QVector<QPointF> &point_list);

    /**
     * Diplay the green wavelength selection rectangle on overlay of the spectrum chart
     **/
    void updateSelectionRectangle();

    /**
     * Displays the color band at bottom of spectrum graph
     **/

    void updateSpectrumColorBand();
    int defaultMinWavelength() const;
    int defaultMaxWavelength() const;

    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;
    void dropEvent(QDropEvent* event) override;
    void displayWavelengthSelectionRectangle(bool val)
    {
        _display_wavelength_selection_rectangle = val;
    }

    double minXRange() const { return _chart_min_x_range; }
    double maxXRange() const { return _chart_max_x_range; }

    double minYRange() const { return _chart_min_y_range; }
    double maxYRange() const { return _chart_max_y_range; }

    bool autoScaleX() const { return _auto_rescale_graph_x; }
    bool autoScaleY() const { return _auto_rescale_graph_y; }



    double pointSize()    const { return _spectrum_scatter_series.markerSize(); }
    bool   showPoints()   const { return _display_spectrum_points; }
    bool   interpolated() const { return _use_spline_series_for_spectrum_display; }

signals:
    void interpolationModeChanged(bool isInterpolated);
    void wavelengthSelectionRangeChanged(double minWl, double maxWl);
    void chartXRangeMinChanged(double xRangeMin);
    void chartXRangeMaxChanged(double xRangeMax);
    void chartYRangeMinChanged(double yRangeMin);
    void chartYRangeMaxChanged(double yRangeMax);
    void openFileOnDropEvent(QString filename);

    void pointSizeChanged(double pointSize);
    void showPointChanged(bool showPoints);

public slots:
    void setAutoRescaleX(bool state);
    void setAutoRescaleY(bool state);

    void setInterpolate(bool interpolate);
    void showSpectrumPoints(bool checked);
    void changePointSize(double size);
    void onMinSelectedWavelengthChanged(double value);
    void onMaxSelectedWavelengthChanged(double value);
    void setXAxisMin(double min);
    void setXAxisMax(double max);
    void setYAxisMin(double min);
    void setYAxisMax(double max);



private slots:
    void onSelectedPixelChanged(const QPointF&);
    void onKernelSizeChanged(int kernel);
    void onLoadFailure();

protected:
    void resizeEvent(QResizeEvent *event) override;
    void leaveEvent(QEvent *) override;
    void mouseMoveEvent       (QMouseEvent * event) override;
    void mousePressEvent      (QMouseEvent * event) override;
    void mouseReleaseEvent    (QMouseEvent * event) override;
    void mouseDoubleClickEvent(QMouseEvent * event) override;
    void wheelEvent(QWheelEvent * event) override;
    void keyPressEvent(QKeyEvent * event) override;

    void autoRescaleGraph();

    void unselectPoints();


    void updatePlot();

private:

    mrf::color::SpectrumConverter _converter;

    QtCharts::QChart         _chart;
    QtCharts::QSplineSeries  _spectrum_spline_series;
    QtCharts::QLineSeries    _spectrum_line_series;
    QtCharts::QScatterSeries _spectrum_scatter_series;
    QtCharts::QScatterSeries _spectrum_scatter_selected_series;
    QtCharts::QValueAxis     _axisX, _axisY;

    bool _use_spline_series_for_spectrum_display;
    bool _auto_rescale_graph_x;
    bool _auto_rescale_graph_y;
    bool _display_spectrum_points;
    bool _min_wavelength_selected;
    bool _max_wavelength_selected;
    bool _min_and_max_wavelength_selected;
    bool _drag_range_selection;
    bool _drag_x_axis;//on Ctrl+middle click drag the spectrum x axis
    bool _use_dark_theme;
    //stores the wavelength at which the drag mode started
    double _min_max_wavelength_drag_start_x;
    double _wavelength_selection_start_x;
    QPoint _wavelength_selection_start_x_scene;
    QGraphicsRectItem* _wavelength_selection_rectangle;
    QGraphicsPixmapItem* _spectrum_color_rectangle;
    QBrush _selection_rectangle_brush;
    QPen _selection_rectangle_pen;
    double _min_wavelength_selection;
    double _max_wavelength_selection;
    //range of chart
    double _chart_min_x_range;
    double _chart_max_x_range;
    double _chart_min_y_range;
    double _chart_max_y_range;

    //boolean used to display or not the wavelength selection rectangle
    //in graph mode (when user open a .spd file) the selection rectangle is not displayed
    bool _display_wavelength_selection_rectangle;

    static constexpr int DRAW_PEN_SIZE = 3;
    static constexpr int DRAW_SELECTION_SIZE = 2;

    static constexpr unsigned char DRAW_COLOR_R = 54;
    static constexpr unsigned char DRAW_COLOR_G = 169;
    static constexpr unsigned char DRAW_COLOR_B = 228;

    static constexpr unsigned char SELECTED_COLOR_R = 54;
    static constexpr unsigned char SELECTED_COLOR_G = 169;
    static constexpr unsigned char SELECTED_COLOR_B = 228;

    static constexpr unsigned char SELECTION_RECT_COLOR_R = 153;
    static constexpr unsigned char SELECTION_RECT_COLOR_G = 202;
    static constexpr unsigned char SELECTION_RECT_COLOR_B = 83;
    static constexpr unsigned char SELECTION_RECT_COLOR_A = 50;


};
