/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "spectrum_parameters_widget.h"
#include "ui_spectrum_parameters_widget.h"

SpectrumParametersWidget::SpectrumParametersWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::SpectrumParametersWidget)
    , _pSpectrumChart(nullptr)
{
    ui->setupUi(this);
}

SpectrumParametersWidget::~SpectrumParametersWidget()
{
    delete ui;
}

bool SpectrumParametersWidget::isActive(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType type)
const
{
    if (_pSpectrumChart != nullptr) {
        return _pSpectrumChart->isActive(mode, type);
    }

    return false;
}

void SpectrumParametersWidget::setModel(
        QSharedPointer<SpectralViewerModel> model
        )
{
    AbstractController::setModel(model);

    onLuminanceMulChanged(_model.data()->luminance());
    onMinSelectedWavelengthChanged(_model.data()->minSelectedWavelength());
    onMaxSelectedWavelengthChanged(_model.data()->maxSelectedWavelength());

    _connections.append({
        connect(_model.data(), SIGNAL(grayLuminanceChanged(double)),
                this         , SLOT(onLuminanceMulChanged(double))),

        connect(_model.data(), SIGNAL(minSelectedWavelengthChanged(double)),
                this         , SLOT(onMinSelectedWavelengthChanged(double))),

        connect(_model.data(), SIGNAL(maxSelectedWavelengthChanged(double)),
                this         , SLOT(onMaxSelectedWavelengthChanged(double))),

        connect(_model.data(), SIGNAL(loadSuccess()),
                this         , SLOT(onLoadSuccess())),

        connect(_model.data(), SIGNAL(spectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType)),
                this         , SLOT(onSpectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType))),

        // UI -> Model
        connect(this         , SIGNAL(luminanceMulChanged(double)),
                _model.data(), SLOT(setGrayLuminance(double))),

        connect(this         , SIGNAL(minSelectedWavelengthChanged(double)),
                _model.data(), SLOT(setMinSelectedWavelength(double))),

        connect(this         , SIGNAL(maxSelectedWavelengthChanged(double)),
                _model.data(), SLOT(setMaxSelectedWavelength(double))),

    });
}



void SpectrumParametersWidget::setSpectrumChart(
        SpectrumChart *pSpectrumChart
        )
{
    if (_pSpectrumChart == pSpectrumChart) { return; }
    if (_pSpectrumChart != nullptr) {
        for (QMetaObject::Connection& c: _spectrumConnections) {
            QObject::disconnect(c);
        }
        _spectrumConnections.clear();
    }

    _pSpectrumChart = pSpectrumChart;


    ui->sbMinWavelength->setValue(_pSpectrumChart->minXRange());
    ui->sbMaxWavelength->setValue(_pSpectrumChart->maxXRange());

    ui->cbAutoRescaleWavelength->setCheckState(_pSpectrumChart->autoScaleX()
                                             ? Qt::CheckState::Checked
                                             : Qt::CheckState::Unchecked);

    ui->sbMinIntensity->setValue(_pSpectrumChart->minYRange());
    ui->sbMaxIntensity->setValue(_pSpectrumChart->maxYRange());

    ui->cbAutoRescaleIntensity->setCheckState(_pSpectrumChart->autoScaleY()
                                             ? Qt::CheckState::Checked
                                             : Qt::CheckState::Unchecked);


    ui->cbInterpolateSpectrum->setCheckState(_pSpectrumChart->interpolated()
                                             ? Qt::CheckState::Checked
                                             : Qt::CheckState::Unchecked);

    ui->cbShowSpectrumPoints->setCheckState(_pSpectrumChart->showPoints()
                                             ? Qt::CheckState::Checked
                                             : Qt::CheckState::Unchecked);

    ui->sbPointSize->setValue(_pSpectrumChart->pointSize());

    _spectrumConnections.append(
    {
        // Wavelength controls
        connect(this           , SIGNAL(minWavelengthChanged(double)),
                _pSpectrumChart, SLOT(setXAxisMin(double))),

        connect(this           , SIGNAL(maxWavelengthChanged(double)),
                _pSpectrumChart, SLOT(setXAxisMax(double))),

        connect(ui->cbAutoRescaleWavelength, SIGNAL(toggled(bool)),
                _pSpectrumChart, SLOT(setAutoRescaleX(bool))),

        connect(_pSpectrumChart, SIGNAL(chartXRangeMinChanged(double)),
                this           , SLOT(onMinWavelengthChanged(double))),

        connect(_pSpectrumChart, SIGNAL(chartXRangeMaxChanged(double)),
                this           , SLOT(onMaxWavelengthChanged(double))),

        // Intensity controls
        connect(this           , SIGNAL(minIntensityChanged(double)),
                _pSpectrumChart, SLOT(setYAxisMin(double))),

        connect(this           , SIGNAL(maxIntensityChanged(double)),
                _pSpectrumChart, SLOT(setYAxisMax(double))),

        connect(ui->cbAutoRescaleIntensity, SIGNAL(toggled(bool)),
                _pSpectrumChart, SLOT(setAutoRescaleY(bool))),

        connect(_pSpectrumChart, SIGNAL(chartYRangeMinChanged(double)),
                this           , SLOT(onMinIntensityChanged(double))),

        connect(_pSpectrumChart, SIGNAL(chartYRangeMaxChanged(double)),
                this           , SLOT(onMaxIntensityChanged(double))),


        // Display
        connect(ui->cbInterpolateSpectrum, SIGNAL(toggled(bool)),
                _pSpectrumChart, SLOT(setInterpolate(bool))),

        connect(ui->cbShowSpectrumPoints, SIGNAL(toggled(bool)),
                _pSpectrumChart, SLOT(showSpectrumPoints(bool))),

        connect(ui->sbPointSize, SIGNAL(valueChanged(double)),
                _pSpectrumChart, SLOT(changePointSize(double))),


        connect(_pSpectrumChart, SIGNAL(interpolationModeChanged(bool)),
                this           , SLOT(onInterpolationModeChanged(bool))),

        connect(_pSpectrumChart, SIGNAL(showPointChanged(bool)),
                this           , SLOT(onShowPointChanged(bool))),

        connect(_pSpectrumChart, SIGNAL(pointSizeChanged(double)),
                ui->sbPointSize, SLOT(setValue(double))),
    });
}

void SpectrumParametersWidget::onSpectralModeChanged(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
{
    switch(mode) {
    case SpectralViewerModel::Spectrum:
        ui->groupBoxSelectionSpectrum->hide();
        break;
    default:
        ui->groupBoxSelectionSpectrum->show();
        break;
    }
}

void SpectrumParametersWidget::onMinSelectedWavelengthChanged(double minSelectedWavelength)
{
    ui->sbMinSelectedWavelength->setValue(minSelectedWavelength);
    emit minSelectedWavelengthChanged(minSelectedWavelength);
}

void SpectrumParametersWidget::onMaxSelectedWavelengthChanged(double maxSelectedWavelength)
{
    ui->sbMaxSelectedWavelength->setValue(maxSelectedWavelength);
    emit maxSelectedWavelengthChanged(maxSelectedWavelength);
}

void SpectrumParametersWidget::onLuminanceMulChanged(double luminanceMul)
{
    ui->sbLuminanceGrayImage->setValue(luminanceMul);
    emit luminanceMulChanged(luminanceMul);
}

void SpectrumParametersWidget::onMinWavelengthChanged(double minWavelength)
{
    ui->sbMinWavelength->setValue(minWavelength);
    emit minWavelengthChanged(minWavelength);
}

void SpectrumParametersWidget::onMaxWavelengthChanged(double maxWavelenght)
{
    ui->sbMaxWavelength->setValue(maxWavelenght);
    emit maxWavelengthChanged(maxWavelenght);
}

void SpectrumParametersWidget::onAutoscaleWavelengthChanged(bool autoscaleWavelength)
{
    ui->cbAutoRescaleWavelength->setCheckState(autoscaleWavelength
                                               ? Qt::CheckState::Checked
                                               : Qt::CheckState::Unchecked);
    emit autoscaleWavelengthChanged(autoscaleWavelength);
}

void SpectrumParametersWidget::onMinIntensityChanged(double minIntensity)
{
    ui->sbMinIntensity->setValue(minIntensity);
    emit minIntensityChanged(minIntensity);
}

void SpectrumParametersWidget::onMaxIntensityChanged(double maxIntensity)
{
    ui->sbMaxIntensity->setValue(maxIntensity);
    emit maxIntensityChanged(maxIntensity);
}

void SpectrumParametersWidget::onAutoscaleIntensityChanged(bool autoscaleIntensity)
{
    ui->cbAutoRescaleIntensity->setCheckState(
                autoscaleIntensity
                ? Qt::CheckState::Checked
                : Qt::CheckState::Unchecked);
    emit autoscaleIntensityChanged(autoscaleIntensity);
}

void SpectrumParametersWidget::onInterpolationModeChanged(bool interpolate)
{
    ui->cbInterpolateSpectrum->setCheckState(
                interpolate
                ? Qt::CheckState::Checked
                : Qt::CheckState::Unchecked);
}

void SpectrumParametersWidget::onShowPointChanged(bool showPoint)
{
    ui->cbShowSpectrumPoints->setCheckState(
                showPoint
                ? Qt::CheckState::Checked
                : Qt::CheckState::Unchecked);
}

void SpectrumParametersWidget::onLoadSuccess()
{
    float incSize = std::pow(10.f, std::floor(std::log10(_model->luminance())) - 1.f);

    ui->sbLuminanceGrayImage->setSingleStep(incSize);
    ui->sbLuminanceGrayImage->setValue(_model->luminance());
}
