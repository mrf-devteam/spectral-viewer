/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QSharedPointer>
#include <QSettings>
#include <QVector>

#include "../spectral_viewer_model.h"

class AbstractController
{
public:
    AbstractController() : _model(nullptr) {}

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const = 0;

    virtual void setModel(QSharedPointer<SpectralViewerModel> model) {
        if (_model == model) { return; }
        if (_model != nullptr) {
            for (QMetaObject::Connection& c: _connections) {
                QObject::disconnect(c);
            }
            _connections.clear();
        }

        _model = model;
    }

    virtual ~AbstractController() {}

    virtual void writeSettings(QSettings &) const {}
    virtual void readSettings (QSettings &) {}

protected:
    QSharedPointer<SpectralViewerModel> _model;
    QVector<QMetaObject::Connection> _connections;
//    bool _active;
};

// Future: plugin management
// Needs to inherit from QObject
// Q_DECLARE_INTERFACE(AbstractController, "fr.spectralviewer.AbstractController")
