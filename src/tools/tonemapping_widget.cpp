/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <QDebug>
#include "tonemapping_widget.h"
#include "ui_tonemapping_widget.h"

TonemappingWidget::TonemappingWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TonemappingWidget)
{
    ui->setupUi(this);
    ui->frameStandardExposure->setVisible(ui->cbExposureAdaptation->currentText() == "Standard");
    ui->frameReinhardExposure->setVisible(ui->cbExposureAdaptation->currentText() == "Reinhard");
}

TonemappingWidget::~TonemappingWidget()
{
    delete ui;
}


bool TonemappingWidget::isActive(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
const
{
    switch (mode) {
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::SpectralImage:
    case SpectralViewerModel::Spectrum:
    case SpectralViewerModel::sRGBImage:
        return true;

    case SpectralViewerModel::NoFile:
        return false;
    }

    return false;
}

void TonemappingWidget::setModel(QSharedPointer<SpectralViewerModel> model)
{
    AbstractController::setModel(model);

    // Force first update
    ui->cbEmissiveImage->setChecked(_model->isEmissive());
    ui->cbSensititivtyFunction->setCurrentIndex(_model->curveName());
    ui->cbExposureAdaptation->setCurrentIndex(_model->exposureAdaptation());

    ui->sbReinhardChromaticAdaptation->setValue(_model->chromaticAdaptation());
    ui->sbReinhardLightAdaptation    ->setValue(_model->lightAdaptation());
    ui->sbReinhardContrast           ->setValue(_model->contrast());
    ui->sbReinhardIntensity          ->setValue(_model->intensity());

    ui->sbExposure ->setValue(_model->exposure());
    ui->sbGrayPoint->setValue(_model->grayPoint());

    onSpectralModeChanged(_model->model(), _model->spectrumType());

    _connections.append(
    {
        connect(ui->cbSensititivtyFunction, SIGNAL(currentIndexChanged(int)),
                this                      , SLOT(setSensitivityCurve(int))),

//        connect(_model.data()             , SIGNAL(sensitivityCurveChanged(mrf::color::SensitivityCurveName)),
//                this                      , SLOT(onSensitivityCurveChanged(mrf::color::SensitivityCurveName))),

        ////////////////////////////////////////////////////////////////

        connect(ui->cbEmissiveImage      , SIGNAL(toggled(bool)),
                _model.data()            , SLOT(setEmissive(bool))),

        connect(_model.data()            , SIGNAL(emissiveModeChanged(bool)),
                ui->cbEmissiveImage      , SLOT(setChecked(bool))),

         connect(_model.data()            , SIGNAL(emissiveModeChanged(bool)),
                 ui->cbIlluminant         , SLOT(setHidden(bool))),

        connect(_model.data()             , SIGNAL(emissiveModeChanged(bool)),
                ui->labelIlluminant       , SLOT(setHidden(bool))),

        ////////////////////////////////////////////////////////////////

        connect(ui->cbIlluminant         , SIGNAL(currentIndexChanged(int)),
                this                     , SLOT(setReflectiveIlluminant(int))),

//        connect(_model.data( )           , SIGNAL(reflectiveIlluminantChanged(mrf::radiometry::MRF_ILLUMINANTS)),
//                this                     , SLOT(onReflectiveIlluminantChanged(mrf::radiometry::MRF_ILLUMINANTS))),

        ////////////////////////////////////////////////////////////////

        connect(ui->cbColorSpace        , SIGNAL(currentIndexChanged(int)),
                this                    , SLOT(setColorSpace(int))),

//        connect(_model.data()           , SIGNAL(colorSpaceChanged(mrf::color::MRF_COLOR_SPACE)),
//                this                    , SLOT(onColorSpaceChanged(mrf::color::MRF_COLOR_SPACE))),

        ////////////////////////////////////////////////////////////////

        connect(ui->cbWhitePoint        , SIGNAL(currentIndexChanged(int)),
                this                    , SLOT(setWhitePoint(int))),

//        connect(_model.data()           , SIGNAL(whitePointChanged(mrf::color::MRF_WHITE_POINT)),
//                this                    , SLOT(onWhitePointChanged(mrf::color::MRF_WHITE_POINT))),

        ////////////////////////////////////////////////////////////////

        connect(ui->cbExposureAdaptation, SIGNAL(currentIndexChanged(const QString&)),
                this                    , SLOT(setExposureAdaptation(const QString&))),

        connect(ui->_button_downsample  , SIGNAL(clicked()),
                this                    , SLOT(downsample())),

        connect(_model.data()           , SIGNAL(exposureAdaptationModeChanged(int)),
                ui->cbExposureAdaptation, SLOT(setCurrentIndex(int))),

        // Reinhard controls
        // Model -> UI
        connect(_model.data()                    , SIGNAL(reinhardChromaticAdaptationChanged(double)),
                ui->sbReinhardChromaticAdaptation, SLOT(setValue(double))),
        connect(_model.data()                    , SIGNAL(reinhardLightAdaptationChanged(double)),
                ui->sbReinhardLightAdaptation    , SLOT(setValue(double))),
        connect(_model.data()                    , SIGNAL(reinhardContrastChanged(double)),
                ui->sbReinhardContrast           , SLOT(setValue(double))),
        connect(_model.data()                    , SIGNAL(reinhardIntensityChanged(double)),
                ui->sbReinhardIntensity          , SLOT(setValue(double))),

        // UI -> Model
        connect(ui->sbReinhardChromaticAdaptation, SIGNAL(valueChanged(double)),
                _model.data()                    , SLOT(setReinhardChromaticAdaptation(double))),
        connect(ui->sbReinhardLightAdaptation    , SIGNAL(valueChanged(double)),
                _model.data()                    , SLOT(setReinhardLightAdaptation(double))),
        connect(ui->sbReinhardContrast           , SIGNAL(valueChanged(double)),
                _model.data()                    , SLOT(setReinhardContrast(double))),
        connect(ui->sbReinhardIntensity          , SIGNAL(valueChanged(double)),
                _model.data()                    , SLOT(setReinhardIntensity(double))),

        // Standard controls
        // Model -> UI
        connect(_model.data()         , SIGNAL(exposureChanged(double)),
                ui->sbExposure        , SLOT(setValue(double))),
        connect(_model.data()         , SIGNAL(grayPointChanged(double)),
                ui->sbGrayPoint       , SLOT(setValue(double))),

        // UI -> Model
        connect(ui->sbExposure        , SIGNAL(valueChanged(double)),
                _model.data()         , SLOT(setExposure(double))),
        connect(ui->sbGrayPoint       , SIGNAL(valueChanged(double)),
                _model.data()         , SLOT(setGrayPoint(double))),


        connect(_model.data(), SIGNAL(spectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType)),
                this         , SLOT(onSpectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType))),
     });


}

mrf::color::SensitivityCurveName TonemappingWidget::getCurrentSensitivityCurve() const
{
    if (ui->cbSensititivtyFunction->currentText() == "CIE XYZ 1931 2°") {
        return mrf::color::SensitivityCurveName::CIE_1931_2DEG;
    } else if (ui->cbSensititivtyFunction->currentText() == "CIE XYZ 1964 10°") {
        return mrf::color::SensitivityCurveName::CIE_XYZ_1964_10DEG;
    } else {
        qWarning() << "Invalid sensitivity curve specified! default to CIE XYZ 1931 2°.";
    }

    return mrf::color::SensitivityCurveName::CIE_1931_2DEG;
}

mrf::radiometry::MRF_ILLUMINANTS TonemappingWidget::getReflectiveIlluminant() const
{
    if (ui->cbIlluminant->currentText() == "D50") {
        return mrf::radiometry::D50;
    } else if (ui->cbIlluminant->currentText() == "D65") {
        return mrf::radiometry::D65;
    } else if (ui->cbIlluminant->currentText() == "E") {
        // TODO: This shall be more explicit in MRF
        return mrf::radiometry::NONE;
    } else {
        qWarning() << "Invalid reflective illuminant specified! default to D65.";
    }

    return mrf::radiometry::D65;
}

mrf::color::MRF_WHITE_POINT TonemappingWidget::getCurrentWhitePoint() const
{
    if (ui->cbWhitePoint->currentText() == "D65") {
        return mrf::color::MRF_WHITE_POINT::D65;
    } else if (ui->cbWhitePoint->currentText() == "D50") {
        return mrf::color::MRF_WHITE_POINT::D50;
    } else {
        qWarning() << "Invalid whitepoint specified! default to D65.";
    }

    return mrf::color::MRF_WHITE_POINT::D65;
}

mrf::color::MRF_COLOR_SPACE TonemappingWidget::getCurrentColorSpace() const
{
    if (ui->cbColorSpace->currentText() == "CIE RGB") {
        return mrf::color::MRF_COLOR_SPACE::CIE_RGB;
    } else if (ui->cbColorSpace->currentText() == "Adobe RGB") {
        return mrf::color::MRF_COLOR_SPACE::ADOBE_RGB;
    } else if (ui->cbColorSpace->currentText() == "Wide Gamut RGB") {
        return mrf::color::MRF_COLOR_SPACE::WIDE_GAMUT_RGB;
    } else {
        qWarning() << "Invalid colorspace specified! default to sRGB.";
    }

    return mrf::color::MRF_COLOR_SPACE::SRGB;
}

SpectralViewerModel::ExposureAdaptation TonemappingWidget::getUIExposureCompensation() const {
    if (ui->cbExposureAdaptation->currentText() == "Reinhard") {
        return SpectralViewerModel::Reinhard;
    }
    return SpectralViewerModel::Standard;

}

void TonemappingWidget::setSensitivityCurve(int index)
{
    Q_UNUSED(index)
    _model->setSensitivityCurve(getCurrentSensitivityCurve());
}

void TonemappingWidget::setReflectiveIlluminant(int index)
{
    Q_UNUSED(index)
    _model->setReflectiveIlluminant(getReflectiveIlluminant());
}


void TonemappingWidget::setColorSpace(int index)
{
    Q_UNUSED(index)
    _model->setColorSpace(getCurrentColorSpace());
}

void TonemappingWidget::setWhitePoint(int index)
{
    Q_UNUSED(index)
    _model->setWhitePoint(getCurrentWhitePoint());
}

void TonemappingWidget::setExposureAdaptation(
        QString const & arg1
        )
{
    if (arg1 == "Standard") {
        _model->setExposureAdaptationMode(SpectralViewerModel::Standard);
        ui->frameStandardExposure->show();
        ui->frameReinhardExposure->hide();
    } else if (arg1 == "Reinhard") {
        _model->setExposureAdaptationMode(SpectralViewerModel::Reinhard);
        ui->frameReinhardExposure->show();
        ui->frameStandardExposure->hide();
    }
}


void TonemappingWidget::onSpectralModeChanged(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
{
    switch(mode) {
    case SpectralViewerModel::sRGBImage:
    // TODO: RGB shall be converted back to XYZ first in the model
    // to enable these features
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::NoFile:
        ui->groupBoxColourSpace       ->hide();
        ui->groupBoxSpectrumConversion->hide();
        ui->goupBoxSpectrumEdition    ->hide();
        break;
    case SpectralViewerModel::SpectralImage:
    case SpectralViewerModel::Spectrum:
        ui->groupBoxColourSpace       ->show();
        ui->groupBoxSpectrumConversion->show();
        ui->goupBoxSpectrumEdition    ->show();
        ui->cbEmissiveImage->setVisible(_model->isEditableEmissive());
        break;
    }
}

void TonemappingWidget::onSensitivityCurveChanged(mrf::color::SensitivityCurveName sensitivityCurve)
{
    ui->cbSensititivtyFunction->setCurrentIndex(sensitivityCurve);
}

void TonemappingWidget::onReflectiveIlluminantChanged(mrf::radiometry::MRF_ILLUMINANTS illuminant)
{
    ui->cbIlluminant->setCurrentIndex(illuminant);
}

void TonemappingWidget::onColorSpaceChanged(mrf::color::MRF_COLOR_SPACE space)
{
    ui->cbColorSpace->setCurrentIndex(space);
}

void TonemappingWidget::onWhitePointChanged(mrf::color::MRF_WHITE_POINT whitePoint)
{
    ui->cbWhitePoint->setCurrentIndex(whitePoint);
}

void TonemappingWidget::downsample()
{
    _model->downsampleSpectralImage(ui->_spin_box_downsampling->value());
}

void TonemappingWidget::on_pbAutoExposure_clicked()
{
    _model->setAutoExposure();
}

