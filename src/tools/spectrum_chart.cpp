/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "spectrum_chart.h"

#include <iostream>
#include <limits>

#include <QDropEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QMimeData>

#include <mrf_core/radiometry/d65.hpp>

#include "math_macros.h"

using namespace QtCharts;


SpectrumChart::SpectrumChart( QWidget * parent )
    : QGraphicsView(new QGraphicsScene, parent)
    , _auto_rescale_graph_x(true)
    , _auto_rescale_graph_y(true)
    , _min_wavelength_selected(false)
    , _max_wavelength_selected(false)
    , _min_and_max_wavelength_selected(false)
    , _drag_range_selection(false)
    , _drag_x_axis(false)
    , _use_dark_theme(true)
    , _min_max_wavelength_drag_start_x(0)
    , _wavelength_selection_start_x(0)
    , _wavelength_selection_rectangle(nullptr)
    , _spectrum_color_rectangle(nullptr)
    , _min_wavelength_selection(0)
    , _max_wavelength_selection(0)
    , _chart_min_x_range(0)
    , _chart_max_x_range(0)
    , _chart_min_y_range(0)
    , _chart_max_y_range(0)
    , _display_wavelength_selection_rectangle(true)
{
    const QColor selection_rect_color      (SELECTION_RECT_COLOR_R, SELECTION_RECT_COLOR_G, SELECTION_RECT_COLOR_B);
    const QColor selection_rect_color_alpha(SELECTION_RECT_COLOR_R, SELECTION_RECT_COLOR_G, SELECTION_RECT_COLOR_B, SELECTION_RECT_COLOR_A);
    const QColor series_draw_color         (DRAW_COLOR_R, DRAW_COLOR_G, DRAW_COLOR_B);
    const QColor series_selected_draw_color(SELECTED_COLOR_R, SELECTED_COLOR_G, SELECTED_COLOR_B);

    setAcceptDrops(true);
    this->setMouseTracking(true);

    _chart.setAcceptHoverEvents(true);
    // _chart.setTitle("Spectrum");
    _chart.legend()->hide();
    //_spectrum_chart->setAnimationOptions(QChart::AllAnimations);

    //create serie to hold spectrum values and display in qtchart
    QPen blue_pen(QColor(DRAW_COLOR_R, DRAW_COLOR_G, DRAW_COLOR_B));
    blue_pen.setWidth(DRAW_PEN_SIZE);
    _spectrum_spline_series.setPen(blue_pen);

    //create line series
    _spectrum_line_series.setPen(blue_pen);

    //create scatter series
    _spectrum_scatter_series.setBorderColor(series_draw_color);
    _spectrum_scatter_series.setColor      (series_draw_color);

    //Scatter selected point serie
    _spectrum_scatter_selected_series.setBorderColor(series_selected_draw_color);
    _spectrum_scatter_selected_series.setColor      (series_selected_draw_color);

    _chart.addSeries(&_spectrum_line_series);
    _chart.addSeries(&_spectrum_scatter_series);
    _chart.addSeries(&_spectrum_scatter_selected_series);
    _chart.addSeries(&_spectrum_spline_series);

    _use_spline_series_for_spectrum_display = false;
    _display_spectrum_points = true;

    setRenderHint(QPainter::Antialiasing);

    //resize Qt chart to proper spectrum range
    _axisX.setRange(_chart_min_x_range, _chart_max_x_range);
    _axisY.setRange(_chart_min_y_range, _chart_max_y_range);
    _chart.addAxis(&_axisX, Qt::AlignBottom);
    _chart.addAxis(&_axisY, Qt::AlignLeft);

    _spectrum_line_series.attachAxis(&_axisX);
    _spectrum_line_series.attachAxis(&_axisY);
    _spectrum_scatter_series.attachAxis(&_axisX);
    _spectrum_scatter_series.attachAxis(&_axisY);
    _spectrum_scatter_selected_series.attachAxis(&_axisX);
    _spectrum_scatter_selected_series.attachAxis(&_axisY);
    _spectrum_spline_series.attachAxis(&_axisX);
    _spectrum_spline_series.attachAxis(&_axisY);

    // Set it to the opposite to trigger the plot configuration
    _use_spline_series_for_spectrum_display = true;
    _display_spectrum_points = false;

    setInterpolate(false);
    showSpectrumPoints(true);


    _selection_rectangle_brush.setStyle(Qt::SolidPattern);
    _selection_rectangle_brush.setColor(selection_rect_color_alpha);

    _selection_rectangle_pen.setColor(selection_rect_color);
    _selection_rectangle_pen.setWidth(DRAW_SELECTION_SIZE);

    if (_use_dark_theme) {
        setDarkTheme();
    }
    scene()->addItem(&_chart);
}

SpectrumChart::~SpectrumChart()
{
    if (_spectrum_color_rectangle != nullptr) {
        scene()->removeItem(_spectrum_color_rectangle);
        delete _spectrum_color_rectangle;
    }

    if (_wavelength_selection_rectangle != nullptr) {
        scene()->removeItem(_wavelength_selection_rectangle);
        delete _wavelength_selection_rectangle;
    }
}

bool SpectrumChart::isActive(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
const
{
    switch (mode) {
    case SpectralViewerModel::SpectralImage:
    case SpectralViewerModel::Spectrum:
        return true;

    case SpectralViewerModel::sRGBImage:
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::NoFile:
        return false;
    }

    return false;
}

void SpectrumChart::setModel(QSharedPointer<SpectralViewerModel> model) {
    AbstractController::setModel(model);

    // Force first update
    onSelectedPixelChanged(_model->selectedPixel());
    onMinSelectedWavelengthChanged(_model->minSelectedWavelength());
    onMaxSelectedWavelengthChanged(_model->maxSelectedWavelength());
    wavelengthSelectionRangeChanged(_model->minWavelength(), _model->maxWavelength());

    _connections.append({
        connect(_model.data(), SIGNAL(selectedPixelChanged(const QPointF&)),
                this  , SLOT(onSelectedPixelChanged(const QPointF&))),

        connect(_model.data(), SIGNAL(kernelSizeChanged(int)),
                this  , SLOT(onKernelSizeChanged(int))),

        connect(_model.data(), SIGNAL(minSelectedWavelengthChanged(double)),
                this  , SLOT(onMinSelectedWavelengthChanged(double))),

        connect(_model.data(), SIGNAL(maxSelectedWavelengthChanged(double)),
                this  , SLOT(onMaxSelectedWavelengthChanged(double))),

        connect(this  , SIGNAL(wavelengthSelectionRangeChanged(double, double)),
                _model.data(), SLOT(setSelectedWavelengthRange(double, double))),

        connect(_model.data(), SIGNAL(loadFailure()),
                this  , SLOT(onLoadFailure()))
    });
}

void SpectrumChart::writeSettings(QSettings &settings) const {
    AbstractController::writeSettings(settings);
    settings.setValue("point_size", pointSize());
    settings.setValue("show_points", showPoints());
    settings.setValue("interpolate", interpolated());
}

void SpectrumChart::readSettings(QSettings &settings) {
    AbstractController::readSettings(settings);
    changePointSize(settings.value("point_size").toDouble());
    showSpectrumPoints(settings.value("show_points").toBool());
    setInterpolate(settings.value("interpolate").toBool());
}

void SpectrumChart::onLoadFailure() {
    QVector<QPointF> point_list;
    setSpectrumPointList(point_list);
}

int SpectrumChart::defaultMinWavelength() const
{
    return int(_chart_min_x_range);
}

int SpectrumChart::defaultMaxWavelength() const
{
    return int(_chart_max_x_range);
}

void SpectrumChart::setAutoRescaleX(bool state)
{
    if (_auto_rescale_graph_x == state) { return; }

    _auto_rescale_graph_x = state;

    if (state)
    {
        autoRescaleGraph();
    }
}

void SpectrumChart::setAutoRescaleY(bool state)
{
    if (_auto_rescale_graph_y == state) { return; }

    _auto_rescale_graph_y = state;

    if (state)
    {
        autoRescaleGraph();
    }
}

void SpectrumChart::dragEnterEvent(QDragEnterEvent *event)
{
    event->setAccepted(true);
}

void SpectrumChart::dragMoveEvent(QDragMoveEvent *event)
{
    event->setAccepted(true);
}

void SpectrumChart::dropEvent(QDropEvent* ev)
{
    QList<QUrl> urls = ev->mimeData()->urls();

    if (!urls.empty())
    {
        QString fileName = urls[0].toString();
        QString startFileTypeString;
#ifdef _WIN32
        startFileTypeString = "file:///";
#else
        startFileTypeString = "file://";
#endif

        if (fileName.startsWith(startFileTypeString))
        {
            fileName = fileName.remove(0, startFileTypeString.length());
            emit openFileOnDropEvent(fileName);
        }
    }
}

void SpectrumChart::setDarkTheme()
{
    QBrush dark_brush;
    QColor dark_color;
    dark_color.setNamedColor("#31363b");
    dark_brush.setColor(dark_color);
    _chart.setBackgroundBrush(dark_brush);

    QColor text_color;
    text_color.setNamedColor("#eff0f1");
    _chart.setTitleBrush(QBrush(text_color));

    _axisX.setLabelsBrush(QBrush(text_color));
    _axisY.setLabelsBrush(QBrush(text_color));

    _use_dark_theme = true;
}

void SpectrumChart::resizeEvent(QResizeEvent *event)
{
    if (scene() != nullptr)
    {
        scene()->setSceneRect(QRect(QPoint(0, 0), event->size()));
        _chart.resize(event->size());
        updateSelectionRectangle();
        updateSpectrumColorBand();
    }
    QGraphicsView::resizeEvent(event);
}

void SpectrumChart::leaveEvent(QEvent * /*event*/)
{
    unselectPoints();
}

void SpectrumChart::mouseMoveEvent(QMouseEvent* event)
{
    if (!items().empty())
    {
        if(_min_wavelength_selected)
        {
            double x = _chart.mapToValue(event->pos()).x();
            onMinSelectedWavelengthChanged(x);
        }
        else if (_max_wavelength_selected)
        {
            double x = _chart.mapToValue(event->pos()).x();
            onMaxSelectedWavelengthChanged(x);
        }
        else if (_min_and_max_wavelength_selected)
        {
            double x = _chart.mapToValue(event->pos()).x();
            double diff = x - _min_max_wavelength_drag_start_x;
            _min_wavelength_selection = _min_wavelength_selection + diff;
            _max_wavelength_selection = _max_wavelength_selection + diff;
            _min_max_wavelength_drag_start_x = x;
            updateSelectionRectangle();
        }
        else if (_drag_range_selection)
        {
            _min_wavelength_selection = _chart.mapToValue(QPointF(std::min(_wavelength_selection_start_x_scene.x(), event->pos().x()), 0.0)).x();
            _max_wavelength_selection = _chart.mapToValue(QPointF(std::max(_wavelength_selection_start_x_scene.x(), event->pos().x()), 0.0)).x();
            updateSelectionRectangle();
        }
        else if (_drag_x_axis)
        {
            //int x = event->pos().x();//
            double  x =_chart.mapToValue(event->pos()).x();

            double delta = _min_max_wavelength_drag_start_x - x;

            //can't do this here mapping will change
            //_min_max_wavelength_drag_start_x = x;


            if (!_auto_rescale_graph_x)
            {
                _chart_min_x_range += delta;
                _chart_max_x_range += delta;
                _axisX.setRange(_chart_min_x_range, _chart_max_x_range);
                updateSelectionRectangle();
                updateSpectrumColorBand();


                emit wavelengthSelectionRangeChanged(_chart_min_x_range, _chart_max_x_range);
            }

            //mapping has changed need to recompute new x here
            _min_max_wavelength_drag_start_x = _chart.mapToValue(event->pos()).x();

            //setXAxisMin(std::max(0.f, _chart_min_x_range + delta));
            //setXAxisMax(_chart_max_x_range + delta);

            //updateSelectionRectangle();


        }
        //No selection activated, display info of spectrum value
        else
        {
            unselectPoints();

            QPointF selection = _chart.mapToValue(QPointF(event->pos()));


            QPointF clickedPoint = selection;
            // Find the closest point from series 1
            QPointF closest;
            double distance = std::numeric_limits<double>::max();
            const auto points = _spectrum_scatter_series.points();
            for (const QPointF &currentPoint : points)
            {
                double currentDistance = (currentPoint.x() - clickedPoint.x())
                        * (currentPoint.x() - clickedPoint.x())
                        + (currentPoint.y() - clickedPoint.y())
                        * (currentPoint.y() - clickedPoint.y());
                if (currentDistance < distance)
                {
                    distance = currentDistance;
                    closest = currentPoint;
                }
            }

            setToolTip(QString("<table>") +
                       "<tr>"  +
                       " <td><strong>wavelength</strong></td>" +
                       " <td>" + QString::number(closest.x()) + " nm</td>" +
                       "</tr>" +
                       "<tr>"  +
                       " <td><strong>value</strong></td>" +
                       " <td>" + QString::number(closest.y()) + "</td>" +
                       "</tr>" +
                       "</table>");

            _spectrum_scatter_series.remove(closest);
            _spectrum_scatter_selected_series.append(closest);
        }
    }
    QGraphicsView::mouseMoveEvent(event);
}

void SpectrumChart::mousePressEvent(QMouseEvent * event)
{
    if (event->button() == Qt::RightButton)
    {
        _min_wavelength_selection = _chart_min_x_range;
        _max_wavelength_selection = _chart_max_x_range;
        updateSelectionRectangle();
        emit wavelengthSelectionRangeChanged(_chart_min_x_range, _chart_max_x_range);
    }
    else if ( (event->button() == Qt::LeftButton) && !(event->modifiers() & Qt::ControlModifier) )
    {
        double y = _chart.mapToValue(event->pos()).y();

        //discard event when clickin outside (on top) of graph
        if (y < std::ceil(_chart_max_y_range))
        {
            double x = _chart.mapToValue(event->pos()).x();

            //click on min bar
            if (_min_wavelength_selection - 3 < x && x < _min_wavelength_selection + 3)
            {
                _min_wavelength_selected = true;
            }
            //click on max bar
            else if (_max_wavelength_selection - 3 < x && x < _max_wavelength_selection + 3)
            {
                _max_wavelength_selected = true;
            }
            //if click not on a bar activate drag selection mode
            else
            {
                _wavelength_selection_start_x = _chart.mapToValue(event->pos()).x();
                _drag_range_selection = true;

                _wavelength_selection_start_x_scene = event->pos();
            }
        }
    }
    else if(    (event->button() == Qt::MiddleButton)
             || ((event->button() == Qt::LeftButton) && ((event->modifiers() & Qt::ControlModifier) != 0U) ) )//drag
    {
        //drag x axis
        if ((event->modifiers() & Qt::ShiftModifier) != 0U)
        {
            _min_max_wavelength_drag_start_x = _chart.mapToValue(event->pos()).x(); //event->pos().x();
            _drag_x_axis = true;
        }
        else//drag selection
        {
            double x = _chart.mapToValue(event->pos()).x();

            //if click between bars or slighty around bars, activate min max selection for drag mode
            if (x > _min_wavelength_selection - 10 && x < _max_wavelength_selection + 10)
            {
                _min_and_max_wavelength_selected = true;
                _min_max_wavelength_drag_start_x = x;
            }
        }
    }
    QGraphicsView::mousePressEvent(event);
}

void SpectrumChart::mouseReleaseEvent(QMouseEvent * event)
{
    if (_min_wavelength_selected)
    {
        _min_wavelength_selected = false;
        emit wavelengthSelectionRangeChanged(_min_wavelength_selection, _max_wavelength_selection);
    }
    else if (_max_wavelength_selected)
    {
        _max_wavelength_selected = false;
        emit wavelengthSelectionRangeChanged(_min_wavelength_selection, _max_wavelength_selection);
    }
    else if (_min_and_max_wavelength_selected)
    {
        _min_and_max_wavelength_selected = false;
        emit wavelengthSelectionRangeChanged(_min_wavelength_selection, _max_wavelength_selection);
    }
    else if (_drag_range_selection)
    {
        double x = _chart.mapToValue(event->pos()).x();

        //if drag from right to left invert values
        if (x < _wavelength_selection_start_x)
        {
            std::swap(x, _wavelength_selection_start_x);
        }

        _min_wavelength_selection = _wavelength_selection_start_x;
        _max_wavelength_selection = x;
        updateSelectionRectangle();
        _drag_range_selection = false;
        emit wavelengthSelectionRangeChanged(_min_wavelength_selection, _max_wavelength_selection);
    }
    else if (_drag_x_axis)
    {
        _drag_x_axis = false;
    }
    QGraphicsView::mouseReleaseEvent(event);
}

void SpectrumChart::mouseDoubleClickEvent(QMouseEvent * event)
{
    QGraphicsView::mouseDoubleClickEvent(event);
}

void SpectrumChart::wheelEvent(QWheelEvent * event)
{
    const QPoint delta = event->angleDelta();

    if (delta.y() != 0) {
        const double increment_wavelength = 5.F * float(delta.y())/120.F;

        if ((event->modifiers() & Qt::ShiftModifier) != 0x0) {
            // Shift selection
            emit wavelengthSelectionRangeChanged(
                        _min_wavelength_selection + increment_wavelength,
                        _max_wavelength_selection + increment_wavelength);
        } else {
            // Increase / Decrease selection
            emit wavelengthSelectionRangeChanged(
                        _min_wavelength_selection - increment_wavelength,
                        _max_wavelength_selection + increment_wavelength);
        }
    }

    QGraphicsView::wheelEvent(event);
}

void SpectrumChart::keyPressEvent(QKeyEvent * event)
{
    if ((event->modifiers() & Qt::ShiftModifier) != 0U)
    {
        int speed = 2;
        int delta = 0;
        if (event->key() == Qt::Key_Left)
        {
            delta = speed;
        }
        else if (event->key() == Qt::Key_Right)
        {
            delta = -speed;
        }

        if (!_auto_rescale_graph_x)
        {
            _chart_min_x_range += delta;
            _chart_max_x_range += delta;
            _axisX.setRange(_chart_min_x_range, _chart_max_x_range);
            updateSelectionRectangle();
            updateSpectrumColorBand();

            emit chartXRangeMinChanged(_chart_min_x_range);
            emit chartXRangeMaxChanged(_chart_max_x_range);
        }
    }
    else
    {
        //std::cout << "Key press" << event->key() << std::endl;
        int speed = 2;
        if (event->key() == Qt::Key_Left)
        {
            _min_wavelength_selection -= speed;
            _max_wavelength_selection -= speed;
            updateSelectionRectangle();

            emit wavelengthSelectionRangeChanged(_min_wavelength_selection, _max_wavelength_selection);
        }
        else if (event->key() == Qt::Key_Right)
        {
            _min_wavelength_selection += speed;
            _max_wavelength_selection += speed;
            updateSelectionRectangle();

            emit wavelengthSelectionRangeChanged(_min_wavelength_selection, _max_wavelength_selection);
        }
    }

    QGraphicsView::keyPressEvent(event);
}

void SpectrumChart::setInterpolate(bool interpolate)
{
    if (_use_spline_series_for_spectrum_display == interpolate) { return;
}

    _use_spline_series_for_spectrum_display = interpolate;

    if (_use_spline_series_for_spectrum_display)
    {
        _spectrum_spline_series.show();
        _spectrum_line_series.hide();
    }
    else
    {
        _spectrum_spline_series.hide();
        _spectrum_line_series.show();
    }

    emit interpolationModeChanged(_use_spline_series_for_spectrum_display);
}

void SpectrumChart::setSpectrumPointList(QVector<QPointF> const & point_list)
{
    //add point to line series
    _spectrum_line_series.replace(point_list);

    //add point to spline series
    _spectrum_spline_series.replace(point_list);

    //add point to scatter series
    //replace is faster than clear + append
    _spectrum_scatter_series.replace(point_list);

    _spectrum_scatter_selected_series.clear();

    if(_auto_rescale_graph_x || _auto_rescale_graph_y)
    {
        autoRescaleGraph();
    }

    //resize Qt chart to proper spectrum range
    _axisX.setRange(_chart_min_x_range, _chart_max_x_range);
    _axisY.setRange(_chart_min_y_range, _chart_max_y_range);
}

void SpectrumChart::showSpectrumPoints(bool checked)
{
    if (_display_spectrum_points == checked) { return; }

    _display_spectrum_points = checked;

    if (_display_spectrum_points)
    {
        _spectrum_scatter_series.show();
        _spectrum_scatter_selected_series.show();
    }
    else
    {
        _spectrum_scatter_series.hide();
        _spectrum_scatter_selected_series.hide();
    }

    emit showPointChanged(_display_spectrum_points);
}

void SpectrumChart::changePointSize(double size)
{
    _spectrum_scatter_series.setMarkerSize(size);
    _spectrum_scatter_selected_series.setMarkerSize(size);

    emit pointSizeChanged(size);
}

void SpectrumChart::onMinSelectedWavelengthChanged(double value)
{
    _min_wavelength_selection = value;
    updateSelectionRectangle();
}

void SpectrumChart::onMaxSelectedWavelengthChanged(double value)
{
    _max_wavelength_selection = value;
    updateSelectionRectangle();
}

void SpectrumChart::setXAxisMin(double min)
{
    if (!_auto_rescale_graph_x && !_drag_x_axis)
    {
        if (DBL_EQ(_chart_min_x_range, min)) { return; }

        _chart_min_x_range = std::min(min, _chart_max_x_range - 10);
        _axisX.setMin(_chart_min_x_range);
        updateSelectionRectangle();
        updateSpectrumColorBand();
        emit chartXRangeMinChanged(_chart_min_x_range);
    }
}

void SpectrumChart::setXAxisMax(double max)
{
    if (!_auto_rescale_graph_x && !_drag_x_axis)
    {
        if (DBL_EQ(_chart_max_x_range, max)) { return; }

        _chart_max_x_range = std::max(max, _chart_min_x_range + 10);
        _axisX.setMax(_chart_max_x_range);
        updateSelectionRectangle();
        updateSpectrumColorBand();
        emit chartXRangeMaxChanged(_chart_max_x_range);
    }
}

void SpectrumChart::setYAxisMin(double min)
{
    if (!_auto_rescale_graph_y)
    {
        if (DBL_EQ(_chart_min_y_range, min)) { return; }

        _chart_min_y_range = std::min(min, _chart_max_y_range);
        _axisY.setMin(_chart_min_y_range);
        emit chartYRangeMinChanged(_chart_min_y_range);
    }
}

void SpectrumChart::setYAxisMax(double max)
{
    if (!_auto_rescale_graph_y)
    {
        if (DBL_EQ(_chart_max_y_range, max)) { return; }

        _chart_max_y_range = std::max(max, _chart_min_y_range);
        _axisY.setMax(_chart_max_y_range);
        emit chartYRangeMaxChanged(_chart_max_y_range);
    }
}

void SpectrumChart::onSelectedPixelChanged(const QPointF & position)
{
    Q_UNUSED(position)

    updatePlot();
}

void SpectrumChart::onKernelSizeChanged(int kernel)
{
    Q_UNUSED(kernel)

    updatePlot();
}

void SpectrumChart::updateSelectionRectangle()
{
    //remove previous selection rectangle
    if (_wavelength_selection_rectangle != nullptr) {
        scene()->removeItem(_wavelength_selection_rectangle);
        delete _wavelength_selection_rectangle;
        _wavelength_selection_rectangle = nullptr;
    }

    if (!_display_wavelength_selection_rectangle)
    {
        return;
    }

    double pos_x  = _chart.mapToPosition(QPointF(_min_wavelength_selection, _chart_min_y_range)).x();
    double pos_y  = _chart.mapToPosition(QPointF(_chart_min_x_range, _chart_max_y_range)).y();
    double width  = _chart.mapToPosition(QPointF(_max_wavelength_selection, _chart_min_y_range)).x() - _chart.mapToPosition(QPointF(_min_wavelength_selection, _chart_min_y_range)).x();
    double height = _chart.mapToPosition(QPointF(_chart_min_x_range, _chart_min_y_range)).y() - _chart.mapToPosition(QPointF(_chart_min_y_range, _chart_max_y_range)).y();

    _wavelength_selection_rectangle = scene()->addRect(pos_x, pos_y, width, height, _selection_rectangle_pen, _selection_rectangle_brush);
}


void SpectrumChart::updateSpectrumColorBand()
{
    //remove previous selection rectangle
    if (_spectrum_color_rectangle != nullptr) {
        scene()->removeItem(_spectrum_color_rectangle);
        delete _spectrum_color_rectangle;
        _spectrum_color_rectangle = nullptr;
    }

    double start_pos_x = _chart.mapToPosition(QPointF(_chart_min_x_range, _chart_min_y_range)).x();
    double end_pos_x   = _chart.mapToPosition(QPointF(_chart_max_x_range, _chart_min_y_range)).x();
    double pos_y       = _chart.mapToPosition(QPointF(_chart_min_x_range, _chart_min_y_range)).y();
    //float width = _chart_max_x_range - _chart_min_x_range;

    QImage temp_image(int(scene()->width()), int(scene()->height()), QImage::Format_RGBA8888);

    // Sanity check
    if (   temp_image.height() < 1
        || temp_image.height() > 100000
        || temp_image.width() < 1
        || temp_image.width() > 100000) {
        return;
    }

    int transparency = 128;
    //pos_y = pos_y; //- height;

    float height = 0.75F * float(temp_image.height() - pos_y);

    // Sanity check
    if (height < 1 || height > 100000) {
        return;
    }

    temp_image.fill(QColor(0,0,0,0));

    uchar* image_bits = temp_image.bits();

    for (int i = int(start_pos_x); i <= int(end_pos_x); i++)
    {
        uint wavelength = uint(_chart_min_x_range + (double(i) - start_pos_x) / (end_pos_x - start_pos_x) * (_chart_max_x_range - _chart_min_x_range));

        //mrf::color::Color c = Color(0.05f)*_converter.xyzValuesAtWavelengthIndex(uint wavelength_index) const

        auto first_w = _converter.sensCurveFirstWavelength();
        uint id_w = wavelength - first_w;

        mrf::color::Color c(0.F);

        if (id_w > 0 && id_w < _converter.nbWavelengths())
        {
            auto temp = _converter.xyzValuesAtIndex(id_w);
            //normalize the spectrum by Y as for a reflective spectrum conversion
            c = mrf::color::Color(temp.x(), temp.y(), temp.z()) *0.05F/temp.y();

            c = c.XYZtoLinearRGB();
            c = mrf::color::getGammaFunction()(c);
            c *= 255;
            c = c.clamped(0, 255);
        }

        //turn of on black colors
        if (c.r() > 0 || c.g() > 0 || c.b() > 0)
        {
            for (int j = 0; j < height; j++)
            {
                int pixel_x = i;
                int pixel_y = std::min(int(j + pos_y), temp_image.height() - 1);
                int num_pixel = pixel_y * temp_image.width() + pixel_x;

                image_bits[4 * num_pixel    ] = uchar(c.r());
                image_bits[4 * num_pixel + 1] = uchar(c.g());
                image_bits[4 * num_pixel + 2] = uchar(c.b());
                image_bits[4 * num_pixel + 3] = uchar(transparency);
            }
        }
    }

    _spectrum_color_rectangle = scene()->addPixmap(QPixmap::fromImage(temp_image));
}

void SpectrumChart::autoRescaleGraph()
{
    auto point_list = _spectrum_line_series.points();
    if (point_list.empty()) { return;
}

    double min_y = std::numeric_limits<double>::max();
    double max_y = 0.;
    double min_x = std::numeric_limits<double>::max();
    double max_x = 0.;
    for (int i = 0; i < point_list.size(); i++)
    {
        min_y = std::min(min_y, point_list[i].y());
        max_y = std::max(max_y, point_list[i].y());

        min_x = std::min(min_x, point_list[i].x());
        max_x = std::max(max_x, point_list[i].x());
    }

    if (std::abs(max_x - min_x) < 0.0001)
    {
        max_x = min_x + 0.0001;
    }
    if (std::abs(max_y - min_y) < 0.0001)
    {
        max_y = min_y + 0.0001;
    }

    //add small offset over data so min and max values are not on edge of graph
    double offset = (max_y - min_y)*0.05;
    min_y = std::max(0., min_y - offset);
    max_y += offset;

    offset = (max_x - min_x)*0.05;
    min_x = std::max(0., min_x - offset);
    max_x += offset;

    //resize Qt chart to proper spectrum range
    if (_auto_rescale_graph_x)
    {
        _chart_min_x_range = min_x;
        _chart_max_x_range = max_x;
        _axisX.setRange(_chart_min_x_range, _chart_max_x_range);
        emit chartXRangeMinChanged(_chart_min_x_range);
        emit chartXRangeMaxChanged(_chart_max_x_range);
    }
    if (_auto_rescale_graph_y)
    {
        _chart_min_y_range = min_y;
        _chart_max_y_range = max_y;
        _axisY.setRange(_chart_min_y_range, _chart_max_y_range);
        emit chartYRangeMinChanged(_chart_min_y_range);
        emit chartYRangeMaxChanged(_chart_max_y_range);
    }


    updateSelectionRectangle();
    updateSpectrumColorBand();
}

void SpectrumChart::unselectPoints()
{
    //move all selected points to the non selected serie
    const auto selected_points = _spectrum_scatter_selected_series.points();
    for (const QPointF &currentPoint : selected_points)
    {
        _spectrum_scatter_series.append(currentPoint);
        _spectrum_scatter_selected_series.remove(currentPoint);
    }
}



void SpectrumChart::updatePlot() {
    if (!_model || !_model->isLoaded()) { return; }

    if (_model->model() == SpectralViewerModel::sRGBImage) { return; }

    mrf::color::Color xyz(0);
    mrf::color::Color rgb(0.F);
    mrf::color::Color tone_mapped_rgb(0.F);
    mrf::color::Color color(0.F);

    std::vector<float> avg_spectrum_values;
    std::vector<uint> spectrum_wavelengths;
    float avg_spectrum_single_value = 0.F;

    _model->getSelectedPixelInfo(
                xyz, rgb,
                tone_mapped_rgb,
                spectrum_wavelengths,
                avg_spectrum_values,
                avg_spectrum_single_value);

    QVector<QPointF> point_list(int(avg_spectrum_values.size()));
    for (int i = 0; i < point_list.size(); i++) {
        point_list[i] =
                    QPointF(
                        static_cast<qreal>(spectrum_wavelengths[uint(i)]),
                        static_cast<qreal>(avg_spectrum_values[uint(i)]));
    }

    setSpectrumPointList(point_list);
}

