/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QObject>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QtCharts/QChartView>
#include <QtCharts/QChart>
#include <QtCharts/QBarSeries>
#include <QLineSeries>
#include <QValueAxis>

class LuminanceHistogramChart : public QGraphicsView
{
  Q_OBJECT
 public:
  explicit LuminanceHistogramChart( QWidget * parent = nullptr );
  virtual ~LuminanceHistogramChart();
  void setHistogramValues(const int* const values, float const & max_value);

 protected:
  void resizeEvent(QResizeEvent *event) override;
  //void mouseMoveEvent(QMouseEvent* event);

 private:
  QtCharts::QChart _chart;

  QtCharts::QLineSeries _dummy_line_series;
  QtCharts::QValueAxis _axisX, _axisY;

  //stores max value of histogram, usefull for rescaling it
  float _max_x_value;
  float _max_y_value;
  //int _x_margin;
  //int _y_margin;
  //int _legend_width;
  //int _legend_height;
  QVector<float> _histogram_values;
};
