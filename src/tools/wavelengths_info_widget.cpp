#include "wavelengths_info_widget.h"
#include "ui_wavelengths_info_widget.h"

WavelengthsInfoWidget::WavelengthsInfoWidget(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::WavelengthsInfoWidget)
{
    ui->setupUi(this);
    ui->spinBox->setValue(ui->listWavelengths->iconSize().width());
}

WavelengthsInfoWidget::~WavelengthsInfoWidget()
{
    delete ui;
}

bool WavelengthsInfoWidget::isActive(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
const
{
    return mode == SpectralViewerModel::Model::SpectralImage;
}

void WavelengthsInfoWidget::setModel(
        QSharedPointer<SpectralViewerModel> model
        )
{
    AbstractController::setModel(model);
    onWavelengthUpdated();
}

void WavelengthsInfoWidget::onWavelengthUpdated()
{
    ui->listWavelengths->clear();

    for (size_t i = 0 ; i < _model->getSpectrumWavelengths().size(); i++) {
        QListWidgetItem *i0 = new QListWidgetItem(
                   QIcon(QPixmap::fromImage(_model->_wavelengthImages[i])),
                   QString::number((uint)_model->getSpectrumWavelengths()[i]) + " nm");

        ui->listWavelengths->insertItem((int)i, i0);
    }
}

void WavelengthsInfoWidget::on_spinBox_valueChanged(int size)
{
    ui->listWavelengths->setIconSize(QSize(size, size));
}

void WavelengthsInfoWidget::on_iconModeButton_toggled(bool checked)
{
    ui->listWavelengths->setViewMode(checked ? QListView::IconMode : QListView::ListMode);
}
