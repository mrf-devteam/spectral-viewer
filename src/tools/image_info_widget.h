/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QWidget>
#include "abstract_controller.h"

namespace Ui {
class ImageInfoWidget;
}

class ImageInfoWidget : public QWidget, public AbstractController
{
    Q_OBJECT

public:
    explicit ImageInfoWidget(QWidget *parent = nullptr);
    virtual ~ImageInfoWidget();

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const override;

    virtual void setModel(QSharedPointer<SpectralViewerModel> model) override;


private slots:
    void wavelengthInfo();
    void onSpectralModeChanged(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type);
    void onStartLoading();
    void onLoadSuccess();
    void onOpenedFileMetaChanged(QMap<QString, QString> const& meta);

private:
    Ui::ImageInfoWidget *ui;
};

