#ifndef WAVELENGTHS_INFO_WIDGET_H
#define WAVELENGTHS_INFO_WIDGET_H

#include <QDialog>
#include "abstract_controller.h"

namespace Ui {
class WavelengthsInfoWidget;
}

class WavelengthsInfoWidget : public QDialog, public AbstractController
{
    Q_OBJECT

public:
    explicit WavelengthsInfoWidget(QWidget *parent = nullptr);
    virtual ~WavelengthsInfoWidget();

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const override;

    void setModel(QSharedPointer<SpectralViewerModel> model) override;

private slots:
    void onWavelengthUpdated();

    void on_spinBox_valueChanged(int arg1);

    void on_iconModeButton_toggled(bool checked);

private:
    Ui::WavelengthsInfoWidget *ui;
};

#endif // WAVELENGTHS_INFO_WIDGET_H
