/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "image_info_widget.h"
#include "ui_image_info_widget.h"
#include "wavelengths_info_widget.h"

#include <QMap>
#include <QTableWidgetItem>

ImageInfoWidget::ImageInfoWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::ImageInfoWidget)
{
    ui->setupUi(this);

    qRegisterMetaType<QMap<QString, QString>>("QMap<QString, QString>");

}

ImageInfoWidget::~ImageInfoWidget()
{
    delete ui;
}

bool ImageInfoWidget::isActive(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
const
{
    switch (mode) {
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::SpectralImage:
    case SpectralViewerModel::sRGBImage:
        return true;

    case SpectralViewerModel::Spectrum:
    case SpectralViewerModel::NoFile:
        return false;
    }

    return false;
}

void ImageInfoWidget::setModel(QSharedPointer<SpectralViewerModel> model)
{
    AbstractController::setModel(model);

    // Force first update
    ui->labelValueWidth ->setNum(static_cast<int>(_model->width()));
    ui->labelValueHeight->setNum(static_cast<int>(_model->height()));
    ui->labelValueBands ->setNum(static_cast<int>(_model->bands()));

    onOpenedFileMetaChanged(model->openedFileMeta());
    //ui->labelComment    ->setText(_model->openedFileComments());

    onSpectralModeChanged(_model->model(), _model->spectrumType());

    _connections.append({
        connect(_model.data()       , SIGNAL(widthChanged(int)),
                ui->labelValueWidth , SLOT(setNum(int))),

        connect(_model.data()       , SIGNAL(heightChanged(int)),
                ui->labelValueHeight, SLOT(setNum(int))),

        connect(_model.data()       , SIGNAL(bandsChanged(int)),
                ui->labelValueBands , SLOT(setNum(int))),

        connect(_model.data(), SIGNAL(spectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType)),
                this         , SLOT(onSpectralModeChanged(SpectralViewerModel::Model, mrf::radiometry::SpectrumType))),

        connect(_model.data(), SIGNAL(openedFileMetaChanged(QMap<QString, QString> const &)),
                this         , SLOT(onOpenedFileMetaChanged(QMap<QString, QString> const &))),

        // Loading
        connect(_model.data(), SIGNAL(startLoading()),
                this         , SLOT(onStartLoading())),
        connect(_model.data(), SIGNAL(loadSuccess()),
                this         , SLOT(onLoadSuccess())),

        connect(ui->bWavelengthsInfo, SIGNAL(clicked()),
                this                , SLOT(wavelengthInfo())),

    });
}

void ImageInfoWidget::wavelengthInfo()
{
    if (_model->isLoaded()) {
        WavelengthsInfoWidget w;
        w.setModel(_model);
        w.exec();
    }
}

void ImageInfoWidget::onSpectralModeChanged(
        SpectralViewerModel::Model model,
        mrf::radiometry::SpectrumType)
{
    switch (model) {
    case SpectralViewerModel::sRGBImage:
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::Spectrum:
    case SpectralViewerModel::NoFile:
        ui->bWavelengthsInfo->hide();
        break;

    case SpectralViewerModel::SpectralImage:
        ui->bWavelengthsInfo->show();
        break;
    }
}

void ImageInfoWidget::onStartLoading()
{
    ui->tableMeta->clear();
}

void ImageInfoWidget::onLoadSuccess()
{
}

void ImageInfoWidget::onOpenedFileMetaChanged(const QMap<QString, QString> &meta)
{
    ui->tableMeta->setVisible(!meta.empty());
    ui->tableMeta->setColumnCount(2);
    ui->tableMeta->setRowCount(meta.size());

    QMap<QString, QString>::const_iterator i = meta.constBegin();
    int j = 0;
    while (i != meta.constEnd()) {
        QTableWidgetItem* label = new QTableWidgetItem(i.key());
        QTableWidgetItem* value = new QTableWidgetItem(i.value());

        QFont f;
        f.setBold(true);

        label->setFont(f);
        label->setFlags(Qt::ItemIsEnabled);
        value->setFlags(Qt::ItemIsEnabled);

        ui->tableMeta->setItem(j, 0, label);
        ui->tableMeta->setItem(j, 1, value);
        ++i; j++;
    }
}
