/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QWidget>
#include "abstract_controller.h"

namespace Ui {
class ImageTransformWidget;
}

class ImageTransformWidget : public QWidget, public AbstractController
{
    Q_OBJECT

public:
    explicit ImageTransformWidget(QWidget *parent = nullptr);
    virtual ~ImageTransformWidget();

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const override;

    void setModel(QSharedPointer<SpectralViewerModel> model) override;

private:
    Ui::ImageTransformWidget *ui;
};

