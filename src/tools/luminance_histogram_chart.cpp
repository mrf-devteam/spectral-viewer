/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "luminance_histogram_chart.h"
#include <QGraphicsSceneMouseEvent>
#include <iostream>
#include <QBarSet>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QLogValueAxis>
#include <QLineSeries>

using std::cout;
using std::endl;
using namespace QtCharts;

LuminanceHistogramChart::LuminanceHistogramChart( QWidget * parent )
  : QGraphicsView(new QGraphicsScene, parent)
  , _max_x_value(1.F)
  , _max_y_value(1.F)
  // , _x_margin(5)
  // , _y_margin(5)
  // , _legend_width (30)
  // , _legend_height(30)
  , _histogram_values(50, 0.F)
{
  this->scene()->addItem(&_chart);
  _chart.setTitle("Luminance Histogram");
  _chart.createDefaultAxes();
  _chart.legend()->setVisible(true);
  //_chart->legend()->setAlignment(Qt::AlignBottom);

  _dummy_line_series.setName("dummy");
  _dummy_line_series.append(QPoint(0, 0));
  _dummy_line_series.append(QPoint(_max_x_value, 100));
  _dummy_line_series.hide();

  _chart.addSeries(&_dummy_line_series);

  _axisX.setRange(0, _max_x_value);
  _axisX.setTickCount(25);

  _axisY.setRange(0, 100);
  _axisY.setLabelFormat("%d%");
  //axisY->setTickCount(10);

  _chart.addAxis(&_axisX, Qt::AlignBottom);
  _chart.addAxis(&_axisY, Qt::AlignLeft);
}

LuminanceHistogramChart::~LuminanceHistogramChart()
{
  scene()->clear();
}

void LuminanceHistogramChart::setHistogramValues(
        const int* const values,
        float const & max_value)
{
  _max_x_value = max_value;
  _max_y_value = 0;
  int sum = 0;

  for (int i = 0; i < _histogram_values.size(); i++) {
    //std::cout << i << " " << values[i]<<"\n";
    _histogram_values[i] = values[i];
    //_max_value = std::max(_max_value, static_cast<float>(_histogram_values[i]));
    sum += values[i];
  }

  if (sum > 0)
  {
    const float scaling = 100.F / static_cast<float>(sum);

    for(float & hist_value : _histogram_values) {
      hist_value *= scaling;
      _max_y_value = std::max(_max_y_value, static_cast<float>(hist_value));
    }
  }

  //reset axis
  _axisX.setRange(0, _max_x_value);
  _axisY.setRange(0, _max_y_value);
}

void LuminanceHistogramChart::resizeEvent(QResizeEvent *event)
{
  QGraphicsScene* scene = this->scene();
  if (scene != nullptr)
  {
    scene->setSceneRect(QRect(QPoint(0, 0), event->size()));
    _chart.resize(event->size());

    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(QColor(54, 169, 228));

    scene->removeItem(&_chart);
    scene->clear();
    scene->addItem(&_chart);


    /*
    //OLD version without map to position
    //int start_y = _chart->mapToScene(0, 0).y();// scene->height() - _y_margin - _legend_height;
    //int start_y = scene->height() - _y_margin - _legend_height;


    float bar_width = (scene->width() - (2 * (_x_margin+1) + _legend_width) ) / static_cast<float>(nb_bars);
    float max_bar_height = scene->height() - (2 * _y_margin + _legend_height);

    for (int i = 0; i < _NB_BARS; i++)
    {


      float x = _x_margin + _legend_width + static_cast<float>(i)*bar_width;

      if (x + bar_width < scene->width()-_x_margin)
      {

      //float bar_height = (max_bar_height)* _histogram_values[i] / std::max(1.f, _max_value);
      float bar_value = (max_bar_height)* _histogram_values[i] / std::max(1.f, _max_value);
      float bar_height = _chart->mapToPosition(QPointF(0.0, 0.0)).y() - _chart->mapToPosition(QPointF(0.0, bar_value)).y();

      //int y = start_y - bar_height;
      //int y = _chart->mapToScene(0, 0).y();
      //int y = scene->height() - _chart->mapToPosition(QPointF(0, 125)).y();
      int y = _chart->mapToPosition(QPointF(0, bar_value)).y();



      //bar_height = 10*rand()/RAND_MAX;// -abs(_chart->mapToScene(0, 0).y() - _chart->mapToScene(0, 200).y());
      //scene->addRect(x, y, bar_width, bar_height, QPen(), brush);
      scene->addRect(x, y, bar_width, bar_height, QPen(), brush);

      y = _chart->mapToPosition(QPointF(0, 500)).y();
      cout << "y= " << y << " \n";
      y = _chart->mapToPosition(QPointF(0, 0)).y();
      cout << "y= " << y << " \n";
      }
      else
      {
      cout << "too small "<< i<<" \n";
      }

    }
    //draw legend
    //horizontal axis
    scene->addLine(_legend_width+_x_margin, scene->height()-_legend_height,
    scene->width()-_x_margin, scene->height() - _legend_height);

    //vertical axis
    scene->addLine(_legend_width, _y_margin,
    _legend_width, scene->height() - _legend_height - _y_margin);

    */

    double bar_width =
            (_chart.mapToPosition(QPointF(_max_x_value, 0.0)).x() - _chart.mapToPosition(QPointF(0.0, 0.0)).x())
            / double(_histogram_values.size());

    for (int i = 0; i < _histogram_values.size(); i++)
    {
      //float x_value = static_cast<float>(i) / static_cast<float>(_max_x_value*_NB_BARS);
      double x = _chart.mapToPosition(QPointF(0.0, 0.0)).x() + i*bar_width; //_chart->mapToPosition(QPointF(x_value, 0.0)).x();
      float y_value = _histogram_values[i];

      double bar_height = _chart.mapToPosition(QPointF(0.0, 0.0)).y() - _chart.mapToPosition(QPointF(0.0, y_value)).y();
      double y = _chart.mapToPosition(QPointF(0, y_value)).y();

      //cout << i<<" "<<bar_value << " "<<bar_height<<" "<<max_bar_height<<"\n";
      QPen black;
      black.setWidth((int) 1.5);
      scene->addRect(x, y, bar_width, bar_height, black, brush);
    }
  }
  QGraphicsView::resizeEvent(event);
}

/*
void LuminanceHistogramChart::mouseMoveEvent(QMouseEvent* event)
{
  float x = _chart.mapToValue(event->pos()).x();
  float y = _chart.mapToValue(event->pos()).y();
  //std::cout << "x=" << x << "y=" << y << "\n";
  int num_bar = std::max(std::min( static_cast<int>(_NB_BARS * x/ _max_x_value), _NB_BARS-1), 0);
  float bar_value = _histogram_values[num_bar];
  //std::cout << "num=" << num_bar << " val=" << bar_value << "\n";

  QGraphicsView::mouseMoveEvent(event);
}
*/
