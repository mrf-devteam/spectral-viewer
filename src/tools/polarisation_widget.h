#ifndef STOKES_WIDGET_H
#define STOKES_WIDGET_H

#include <QWidget>
#include "abstract_controller.h"

namespace Ui {
class PolarisationWidget;
}

class PolarisationWidget : public QWidget, public AbstractController
{
    Q_OBJECT

public:
    explicit PolarisationWidget(QWidget *parent = nullptr);
    ~PolarisationWidget();

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const override;

    void setModel(QSharedPointer<SpectralViewerModel> model) override;

private slots:
    void on_cbS0_stateChanged(int arg1);

    void on_cbS1_stateChanged(int arg1);

    void on_cbS2_stateChanged(int arg1);

    void on_cbS3_stateChanged(int arg1);

private:
    Ui::PolarisationWidget *ui;
};

#endif // STOKES_WIDGET_H
