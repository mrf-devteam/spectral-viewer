/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QWidget>
#include "abstract_controller.h"

namespace Ui {
class TonemappingWidget;
}

class TonemappingWidget : public QWidget, public AbstractController
{
    Q_OBJECT

public:
    explicit TonemappingWidget(QWidget *parent = nullptr);
    virtual ~TonemappingWidget();

    virtual bool isActive(
            SpectralViewerModel::Model mode,
            mrf::radiometry::SpectrumType type)
    const override;

    void setModel(QSharedPointer<SpectralViewerModel> model) override;

    mrf::color::SensitivityCurveName getCurrentSensitivityCurve() const;
    mrf::radiometry::MRF_ILLUMINANTS getReflectiveIlluminant() const;
    mrf::color::MRF_WHITE_POINT getCurrentWhitePoint() const;
    mrf::color::MRF_COLOR_SPACE getCurrentColorSpace() const;
    SpectralViewerModel::ExposureAdaptation getUIExposureCompensation() const;


private slots:
    void setSensitivityCurve(int index);
    void setReflectiveIlluminant(int index);
    void setColorSpace(int index);
    void setWhitePoint(int index);
    void setExposureAdaptation(const QString &arg1);

    void onSpectralModeChanged(SpectralViewerModel::Model model, mrf::radiometry::SpectrumType type);

    void onSensitivityCurveChanged    (mrf::color::SensitivityCurveName sensitivityCurve);
    void onReflectiveIlluminantChanged(mrf::radiometry::MRF_ILLUMINANTS illuminant);
    void onColorSpaceChanged          (mrf::color::MRF_COLOR_SPACE space);
    void onWhitePointChanged          (mrf::color::MRF_WHITE_POINT whitePoint);

    void downsample();

    void on_pbAutoExposure_clicked();

private:
    Ui::TonemappingWidget *ui;
};
