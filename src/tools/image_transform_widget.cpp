/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include "image_transform_widget.h"
#include "ui_image_transform_widget.h"

ImageTransformWidget::ImageTransformWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::ImageTransformWidget)
{
    ui->setupUi(this);
}

ImageTransformWidget::~ImageTransformWidget()
{
    delete ui;
}

bool ImageTransformWidget::isActive(
        SpectralViewerModel::Model mode,
        mrf::radiometry::SpectrumType)
const
{
    switch (mode) {
    case SpectralViewerModel::HDRImage:
    case SpectralViewerModel::SpectralImage:
    case SpectralViewerModel::sRGBImage:
        return true;

    case SpectralViewerModel::Spectrum:
    case SpectralViewerModel::NoFile:
        return false;
    }

    return false;
}

void ImageTransformWidget::setModel(QSharedPointer<SpectralViewerModel> model)
{
    AbstractController::setModel(model);

    // Force first update
    ui->sbZoom->setValue(_model->zoomLevel());

    _connections.append({
        // Zoom
        connect(_model.data(), SIGNAL(zoomLevelChanged(double)),
                ui->sbZoom   , SLOT(setValue(double))),
        connect(ui->sbZoom   , SIGNAL(valueChanged(double)),
                _model.data(), SLOT(setZoomLevel(double))),

        // Rotation
//        connect(ui->buttonRotateLeft , SIGNAL(clicked()),
//                _model.data()        , SLOT(rotateLeft())),
//        connect(ui->buttonRotateRight, SIGNAL(clicked()),
//                _model.data()        , SLOT(rotateRight()))
    });
}
