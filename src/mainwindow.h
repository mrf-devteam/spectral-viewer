/*
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <QMainWindow>
#include <QLabel>
#include <QSettings>
#include <QShortcut>
#include <QFuture>
#include <QProgressDialog>
#include <QProgressBar>

#include <mrf_core/image/spectral_image.hpp>

#include "spectral_viewer_model.h"

#include "tools/abstract_controller.h"

namespace Ui
{
class MainWindow;
class SpectrumParameters;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    struct Toolbox {
        QDockWidget        * dockWidget;
        AbstractController * controler;
    };

public:
    explicit MainWindow(
            const QSharedPointer<SpectralViewerModel>& model,
            QWidget *parent = nullptr
            );

    ~MainWindow();

    //-------------------------------------------------------------------------
    // Loading functions
    //-------------------------------------------------------------------------

    /**
     * @brief openFile
     * Handle opening a file. It determines the type of file and the relevant
     * model to use
     * @param fileName Name of the file
     */
    void openFile(const QString& fileName);

    void openFolder(
            const QStringList &imageFiles,
            uint start_wavelength,
            uint offset_wavelength
            );


protected:
    void closeEvent(QCloseEvent * event);
    void showEvent(QShowEvent* event);

private:
    /**
     * @brief setModel
     * Set a model to display
     * @param model
     */
    void setModel(const QSharedPointer<SpectralViewerModel>& model);

    void dropEvent(QDropEvent* event);
    void dragEnterEvent(QDragEnterEvent *ev);

    void writeSettings();
    void readSettings();

private slots:
    void openWelcome();

    void onLoadSaveStatus(mrf::image::IMAGE_LOAD_SAVE_FLAGS status);
    void onOpenedFilenameChanged(const QString &filename);
    void onSpectralModeChanged(SpectralViewerModel::Model mode, mrf::radiometry::SpectrumType type);

    void onAfterLoad();
    void onStartLoading();
    void onProcessingStarted();
    void onProcessingEnded();

    void resetViewingParameters();
    void onLoadSuccess();
    void errorOpenImage();

    //-------------------------------------------------------------------------
    // File menu actions
    //-------------------------------------------------------------------------

    void on_actionOpenFolder_triggered();
    void on_actionOpenFile_triggered();

    //-------------------------------------------------------

    //void on_actionSave_as_exr_triggered();
    void on_actionSave_Spectral_Image_triggered();
    void on_actionSave_RGB_image_triggered();
    void on_actionExit_triggered();

    //-------------------------------------------------------------------------
    // View menu actions
    //-------------------------------------------------------------------------

    void on_actionMemory_Occupancy_triggered();

    //-------------------------------------------------------------------------
    // Help menu actions
    //-------------------------------------------------------------------------

    void on_actionHelp_triggered();
    void on_actionAbout_triggered();


    void on_actionRotate_Right_triggered();
    void on_actionRotate_Left_triggered();

    void on_actionRefresh_triggered();

    void on_actionZoomIn_triggered();
    void on_actionZoomOut_triggered();

    void on_actionSplitView_triggered();

    void on_actionGrowView_triggered();

private:
    Ui::MainWindow *ui;

    QProgressDialog* _progressDialog;

    QSharedPointer<SpectralViewerModel> _model;
    QLabel* _status_bar_memory_consumption;
    QProgressBar* _status_bar_progress;

    QFutureWatcher<void> *_imageProcessWatcher;

    std::vector<Toolbox> _toolboxes;

    QString _baseWindowTitle;

    bool _checkForUpdates;
};
