Spectral Viewer
===============

Spectral Viewer is an open source software for opening and
manipulating spectral images.

It supports:
- Spectral formats:
  - ENVI spectral images
  - ART Raw spectral images
  - OpenEXR
- HDR Formats:
  - OpenEXR
  - Ward HDR format
- Common formats:
  - PNG, JPEG, BMP...
  - TIFF (if compiled with `libtiff`)

Please visit the [project
website](http://mrf-devteam.gitlab.io/spectral-viewer/) for more
information.


Binaries
========

Precompiled packages
--------------------

Spectral Viewer supports Windows, Linux and macOS. A list of binaries
are available on [this
page](http://mrf-devteam.gitlab.io/spectral-viewer/download).


Linux Specific packages
-----------------------

We provide Arch Linux and Ubuntu packages:

For Ubuntu, the package is published on Launchpad
```
sudo add-apt-repository ppa:alban-f/spectral-viewer
sudo apt update
sudo apt install spectral-viewer
```

For Arch, the [AUR package](https://aur.archlinux.org/packages/spectral-viewer/)
 can be installed with yay.
```
yay -S spectral-viewer
```

You can also use the snap package for a large flavour of distributions:

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/spectral-viewer)



Compilation
===========

You need CMake to build Spectral Viewer.

## Checking out the sources

Make sure you check out Spectral Viewer and its submodule. Spectral
Viewer depends on MRF:

```
git clone https://gitlab.com/mrf-devteam/spectral-viewer.git
cd spectral-viewer
git submodule init
git submodule update
```

## Compilation

```
mkdir build
cd build
cmake ..
make
```
