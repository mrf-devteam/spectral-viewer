#!/bin/bash

# icons
mkdir -p SpectralViewer.AppDir/usr/share/icons/hicolor/128x128/apps
mkdir -p SpectralViewer.AppDir/usr/share/icons/hicolor/64x64/apps
mkdir -p SpectralViewer.AppDir/usr/share/icons/hicolor/48x48/apps
mkdir -p SpectralViewer.AppDir/usr/share/icons/hicolor/32x32/apps

cp SpectralViewer.png SpectralViewer.AppDir/usr/share/icons/hicolor/512x512/apps/SpectralViewer.png

convert -resize 128 SpectralViewer.png SpectralViewer.AppDir/usr/share/icons/hicolor/128x128/apps/SpectralViewer.png
convert -resize 64 SpectralViewer.png SpectralViewer.AppDir/usr/share/icons/hicolor/64x64/apps/SpectralViewer.png
convert -resize 48 SpectralViewer.png SpectralViewer.AppDir/usr/share/icons/hicolor/48x48/apps/SpectralViewer.png
convert -resize 32 SpectralViewer.png SpectralViewer.AppDir/usr/share/icons/hicolor/32x32/apps/SpectralViewer.png

cp SpectralViewer.png SpectralViewer.AppDir/

mkdir -p SpectralViewer.AppDir/usr/share/applications
cp mrf-devteam.spectralviewer.desktop SpectralViewer.AppDir/usr/share/applications/

mkdir -p SpectralViewer.AppDir/usr/share/metainfo
cp mrf-devteam.spectralviewer.metainfo.xml SpectralViewer.AppDir/usr/share/metainfo/


wget https://github.com/probonopd/linuxdeployqt/releases/download/5/linuxdeployqt-5-x86_64.AppImage
chmod a+x linuxdeployqt-5-x86_64.AppImage 

./linuxdeployqt-5-x86_64.AppImage \
    SpectralViewer.AppDir/usr/share/applications/mrf-devteam.spectralviewer.desktop \
    -appimage \
    -verbose=2 \
    -qmake=/usr/bin/qmake  \
    -no-translations \
    -extra-plugins=iconengines/libqsvgicon.so
