#include <QtTest/QtTest>

#include <mrf/image/image_formats.hpp>
#include <spectral_viewer_model.h>
#include <mainwindow.h>

class TestSpectralImage: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase() {
        const std::string output_path = TEST_DATA;
        const mrf::uint width = 10;
        const mrf::uint height = 5;

        QDir path(QString::fromStdString(output_path + "/envi"));
        if (!path.exists()) {
            path.mkpath(".");
        }

        // Create a empty spectral image
        mrf::image::EnviSpectralImage image_empty(width, height);
        path_emtpy_image = output_path + "/envi/empty.hdr";
        image_empty.save(path_emtpy_image);

        // Create an image with nan
        mrf::image::EnviSpectralImage image_NaN(width, height);
        std::vector<float> imageNaNContent(width * height);

        for (float& v: imageNaNContent) {
            v = std::numeric_limits<float>::quiet_NaN();
        }

        for (const mrf::uint& wavelength: image_NaN.wavelengths()) {
            image_NaN.addValuesFromBuffer(imageNaNContent, width, height, wavelength);
        }

        path_NaN_image = output_path + "/envi/nan.hdr";
        image_NaN.save(path_NaN_image);

        // Create an image with infinity
        mrf::image::EnviSpectralImage image_infty(width, height);
        std::vector<float> imageInftyContent(width * height);
        for (float& v: imageInftyContent) {
            v = std::numeric_limits<float>::infinity();
        }

        for (const mrf::uint& wavelength: image_infty.wavelengths()) {
            image_infty.addValuesFromBuffer(imageInftyContent, width, height, wavelength);
        }

        path_Infty_image = output_path + "/envi/infty.hdr";
        image_infty.save(path_Infty_image);
    }

    void loadEmpty()
    {
        SpectralViewerModel m;
        m.openFile(QString::fromStdString(path_emtpy_image));

        QVERIFY(m.isLoaded());
    }

    void loadNaN()
    {
        SpectralViewerModel m;
        m.openFile(QString::fromStdString(path_NaN_image));

        QVERIFY(m.isLoaded());
    }

    void loadInfty()
    {
        SpectralViewerModel m;
        m.openFile(QString::fromStdString(path_Infty_image));

        QVERIFY(m.isLoaded());
    }


private:

    std::string path_emtpy_image;
    std::string path_NaN_image;
    std::string path_Infty_image;

};


QTEST_MAIN(TestSpectralImage)
#include "test_spectral_image.moc"
