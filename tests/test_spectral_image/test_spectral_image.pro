QT += testlib core gui charts concurrent svg network
CONFIG += c++14 warn_on

TEMPLATE = app
TARGET = tests

!mac {
    QMAKE_CXXFLAGS += -fopenmp
    LIBS += -fopenmp
}

LIBS +=                                        \
    -L../../externals -lMRF                    \
    -L../../src       -lSpectralViewer

INCLUDEPATH +=                 \
    $$PWD/../../externals/MRF/ \
    $$PWD/../../src/

# Input
SOURCES += test_spectral_image.cpp

include($$PWD/../test.pri)
