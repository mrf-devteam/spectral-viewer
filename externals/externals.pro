TARGET = MRF
TEMPLATE = lib
CONFIG += c++14 warn_on
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS MRF_STATIC

!mac {
    QMAKE_CXXFLAGS += -fopenmp
    LIBS += -fopenmp
}

INCLUDEPATH += MRF MRF/mrf

SOURCES +=                                                                     \
    MRF/mrf/radiometry/spectrum.inl                                            \
                                                                               \
    MRF/mrf/image/color_image.cpp                                              \
    MRF/mrf/image/exr_color_image.cpp                                          \
    MRF/mrf/image/hdr_color_image.cpp                                          \
    MRF/mrf/image/pfm_color_image.cpp                                          \
    MRF/mrf/image/png_color_image.cpp                                          \
    MRF/mrf/image/spectral_image.cpp                                           \
    MRF/mrf/image/uniform_spectral_image.cpp                                   \
    MRF/mrf/image/artraw_spectral_image.cpp                                    \
    MRF/mrf/image/envi_spectral_image.cpp                                      \
    MRF/mrf/image/exr_spectral_image.cpp                                       \
                                                                               \
    MRF/mrf/util/cpu_memory.cpp                                                \
    MRF/mrf/util/string_parsing.cpp                                            \
    MRF/mrf/io/exr_io.cpp                                                      \
                                                                               \
    MRF/externals/tinypng/lodepng.cpp                                          \
    MRF/externals/rgbe/rgbe.cpp                                                \
    MRF/externals/tinyexr/tinyexr.cpp

HEADERS  +=                                                                    \
    MRF/mrf/mrf_types.hpp                                                      \
    MRF/mrf/mrf_dll.hpp                                                        \
                                                                               \
    MRF/mrf/radiometry/spectrum.hpp                                            \
                                                                               \
    MRF/mrf/color/color.hpp                                                    \
    MRF/mrf/color/color_data.hpp                                               \
    MRF/mrf/color/spectrum_converter.hpp                                       \
                                                                               \
    MRF/mrf/image/image.hpp                                                    \
    MRF/mrf/image/color_image.hpp                                              \
    MRF/mrf/image/exr_color_image.hpp                                          \
    MRF/mrf/image/hdr_color_image.hpp                                          \
    MRF/mrf/image/pfm_color_image.hpp                                          \
    MRF/mrf/image/spectral_image.hpp                                           \
    MRF/mrf/image/uniform_spectral_image.hpp                                   \
    MRF/mrf/image/artraw_spectral_image.hpp                                    \
    MRF/mrf/image/envi_spectral_image.hpp                                      \
    MRF/mrf/image/exr_spectral_image.hpp                                       \
    MRF/mrf/image/image_formats.hpp                                            \
                                                                               \
    MRF/mrf/data_struct/array2d.hpp                                            \
    MRF/mrf/data_struct/array2d.hpp                                            \
    MRF/mrf/data_struct/eigen_array1d.hpp                                      \
    MRF/mrf/data_struct/mrf_array1d.hpp                                        \
    MRF/mrf/data_struct/three_dim_map.hpp                                      \
    MRF/mrf/data_struct/array3D.hpp                                            \
    MRF/mrf/data_struct/eigen_array2d.hpp                                      \
    MRF/mrf/data_struct/mrf_array2d.hpp                                        \
    MRF/mrf/data_struct/two_dim_map.hpp                                        \
    MRF/mrf/data_struct/array.hpp                                              \
    MRF/mrf/data_struct/four_dim_map.hpp                                       \
    MRF/mrf/data_struct/one_dim_map.hpp
