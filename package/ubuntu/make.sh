#!/bin/sh
# Inspired from RetroShare packaging script

MAJOR_VERSION=3
MINOR_VERSION=3.0
PKG_NAME="spectral-viewer"
dist="bionic focal hirsute"
ppa_addr="ppa:alban-f/spectral-viewer"

version_number="${MAJOR_VERSION}"'.'"${MINOR_VERSION}"
package_dir="${PKG_NAME}"'-'"${version_number}"

echo "Building package for "${package_dir}

if test -d "${package}" ; then
    rm -rf ${package_dir}
fi

date=`git log --pretty=format:"%ai" | head -1 | cut -d\  -f1 | sed -e s/-//g`
time=`git log --pretty=format:"%aD" | head -1 | cut -d\  -f5 | sed -e s/://g`
hhsh=`git log --pretty=format:"%H"  | head -1 | cut -c1-8`
rev=${date}.${hhsh}

change=`git log --pretty="%h %an %aD %s"`

mkdir ${package_dir}
cd ${package_dir}

# Creating source tarball
git clone git@gitlab.com:mrf-devteam/spectral-viewer.git

# Cleaning up useless files for tarball
cd spectral-viewer
git submodule init
git submodule update
cd externals/MRF
rm -rf apps
rm -rf doc
rm -rf mdl
rm -rf unit_tests
rm -rf .git
cd -
rm -rf .git
cd ..

tar cvzf "${PKG_NAME}_${version_number}.orig.tar.gz" spectral-viewer 

# For each distribution, generating a changelog
for i in ${dist}; do
	cd spectral-viewer
    rm -r debian
    cp -r ../../debian .
    sed -e s/XXXXXX/"${rev}"/g -e s/YYYYYY/"${i}"/g -e s/ZZZZZZ/"${version_number}"/g ../../debian/changelog > debian/changelog
    debuild -S
    cd ..
    dput ${ppa_addr} "${PKG_NAME}_${version_number}-1.${rev}~${i}_source.changes"
done
